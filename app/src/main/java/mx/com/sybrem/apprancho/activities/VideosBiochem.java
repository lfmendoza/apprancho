package mx.com.sybrem.apprancho.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import mx.com.sybrem.apprancho.R;
import mx.com.sybrem.apprancho.clases.RanchoDB;
import mx.com.sybrem.apprancho.clases.VideoObject;
import mx.com.sybrem.apprancho.clases.adapters.AdapterVideoBiochem;
import mx.com.sybrem.apprancho.clases.general.DialogsBiochem;
import mx.com.sybrem.apprancho.clases.general.ObjectSerializer;

public class VideosBiochem extends AppCompatActivity {

    ArrayList<String> linksDescarga;
    ProgressDialog pd;
    private int totalVideos = 0;
    ListView lstVideos;
    ArrayList<VideoObject> arrVideos;
    AdapterVideoBiochem adapterVideoBiochem;
    LinearLayout LinearVideos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos_biochem);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String descargad = (String) extras.get("descargad");

        LinearVideos = (LinearLayout)findViewById(R.id.LinearVideos);
        LinearVideos.setVisibility(View.GONE);

        linksDescarga = new ArrayList<>();
        File file = new File(Environment.getExternalStorageDirectory() + "/download/BiochemFiles/vidios/1.mp4");
        if (file.exists()) {
            verificaVideos();
        }else{
            DialogsBiochem.muestraDialogoWarningCustom(VideosBiochem.this, "Se descargarán los videos Biochem, esto pude durar unos minutos.", new DialogsBiochem.CustomAccion() {
                @Override
                public void customAccion() {
                    new ObtieneArchivosVideo().execute();
                }
            }, 1);
        }
    }

    public void verificaVideos(){
        Log.e("verifVideo", "entra a verifica videos");
        SharedPreferences prefs = getSharedPreferences("vidiosNames", Context.MODE_PRIVATE);
        try {
            linksDescarga = (ArrayList<String>) ObjectSerializer.deserialize(prefs.getString("vidiosLink", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (IOException e) {
            Log.e("ERR", "ERROR[1]:"+e);
        } catch (ClassNotFoundException e) {
            Log.e("ERR", "ERROR[2]:"+e);
        }
        int totals = 0;
        if(linksDescarga.size() > 0){
            for(String video : linksDescarga){
                File file = new File(Environment.getExternalStorageDirectory() + "/download/BiochemFiles/vidios/" + video);
                if (file.exists()) {
                    Log.e("EXISTS", "EL video "+ video+" ya existe.");
                    totals++;
                }else{
                    Log.e("NON EXISTS", "EL video "+ video+" no existe.");
                    Intent explicit_intent = new Intent(VideosBiochem.this, DescargaVideos.class);
                    explicit_intent.putExtra("linkDescarga", video);
                    startActivity(explicit_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                    finish();
                }
            }
        }else{
            linksDescarga.add("1.mp4");
            linksDescarga.add("2.mp4");
            linksDescarga.add("3.mp4");
            linksDescarga.add("4.mp4");
            linksDescarga.add("5.mp4");
            linksDescarga.add("6.mp4");
            totals = linksDescarga.size();
        }
        if(totals == linksDescarga.size()){
            iniciaVista();
        }
    }

    public void iniciaVista(){
        arrVideos = new ArrayList<>();
        LinearVideos.setVisibility(View.VISIBLE);

        for(String video : linksDescarga){
            arrVideos.add(new VideoObject(video));
        }
        lstVideos = (ListView)findViewById(R.id.lstVideos);
        adapterVideoBiochem = new AdapterVideoBiochem(VideosBiochem.this, arrVideos);
        lstVideos.setAdapter(adapterVideoBiochem);
        lstVideos.setDividerHeight(0);
        lstVideos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                VideoObject vid = (VideoObject) adapterVideoBiochem.getItem(position);
                File file = new File(Environment.getExternalStorageDirectory() + "/download/BiochemFiles/vidios/"+vid.getVideoName());
                if(file.exists()){
                    Uri uri = Uri.fromFile(file);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(uri, "video/*");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        });
    }
    @Override
    public void onBackPressed() {
        finActivity();
    }


    public void finActivity(){
        RanchoDB dbHandler = new RanchoDB(VideosBiochem.this, null, null, 1);
        dbHandler.checkDBStatus();
        String usuario = dbHandler.ultimoUsuarioRegistrado();
        finish();
        Intent explicit_intent;
        explicit_intent = new Intent(VideosBiochem.this, NavDrawerActivity.class);
        explicit_intent.putExtra("usuario", usuario);
        explicit_intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        explicit_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(explicit_intent);
        overridePendingTransition(R.anim.lista_dearriba_pabajo, R.anim.fab_close);
    }

    private class ObtieneArchivosVideo extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

            pd = new ProgressDialog(VideosBiochem.this);
            pd.setTitle("Conectando a servidor Biochem...");
            pd.setMessage("Obteniendo videos, espere...");
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            String JsonObtieneVideos = "";
            String urlGetTemas = "http://sybrem.com.mx/adsnet/syncmovil/java/videosMaiceros/archivos.php";

            try {
                URL url = new URL(urlGetTemas);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                JsonObtieneVideos = buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return JsonObtieneVideos;
        }

        @Override
        protected void onPostExecute(String respuesta_servidor) {
            super.onPostExecute(respuesta_servidor);

            if (pd.isShowing()) {
                pd.dismiss();
            }
            Log.e("RESPUESTA VID", respuesta_servidor);
            try {
                JSONArray respuesta = new JSONArray(respuesta_servidor);
                for(int i = 0; i < respuesta.length(); i++){
                    JSONObject obj = respuesta.getJSONObject(i);
                    String archive = obj.getString("archivo");
                    totalVideos++;
                    linksDescarga.add(archive);
                }
                SharedPreferences prefs = getSharedPreferences("vidiosNames", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                try {
                    editor.putString("vidiosLink", ObjectSerializer.serialize(linksDescarga));
                } catch (IOException e) {
                    Log.e("ERROR", "Error al serializar los nombres de los videos maiceros: "+ e);
                }
                editor.commit();
                verificaVideos();
            }catch (JSONException e) {
                DialogsBiochem.muestraDialogoError(VideosBiochem.this, "ERROR:\n"+e);
            }
        }
    }
}
