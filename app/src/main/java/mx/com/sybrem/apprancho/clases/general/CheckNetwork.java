package mx.com.sybrem.apprancho.clases.general;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class CheckNetwork {

    private static final String TAG = CheckNetwork.class.getSimpleName();

    public static boolean isInternetAvailable(Context context) {
        NetworkInfo info = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        if (info == null) {
            Log.d(TAG, "Sin conexión a internet.");
            return false;
        } else {
            if (info.isConnected()) {
                Log.d(TAG, "conexión a internet establecida.");
                return true;
            } else {
                Log.d(TAG, "Hay disponible conexión a internet.");
                return true;
            }
        }
    }
}
