package mx.com.sybrem.apprancho.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import mx.com.sybrem.apprancho.BuildConfig;
import mx.com.sybrem.apprancho.R;
import mx.com.sybrem.apprancho.clases.RanchoDB;
import mx.com.sybrem.apprancho.clases.general.Autoupdater;
import mx.com.sybrem.apprancho.clases.general.CheckNetwork;
import mx.com.sybrem.apprancho.clases.general.DialogsBiochem;
import mx.com.sybrem.apprancho.clases.general.ToastBiochem;

public class Login extends AppCompatActivity {

    private static final int GET_UNKNOWN_APP_SOURCES = 0;
    private static final int INSTALL_PACKAGES_REQUESTCODE = 0;

    private Context context;
    private Autoupdater updater; //Uso de la clase que usa la clase de la autoActualización

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "ranchoTablet.db";

    String currentVersionName = "";

    Button borra_datos, btn;
    String usuario = "", password = "";
    AlertDialog.Builder pd;
    AlertDialog dialog;
    View view;
    private long downloadID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        final RanchoDB ranchoDB = new RanchoDB(Login.this, DATABASE_NAME, null, DATABASE_VERSION);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usuario = ((EditText) findViewById(R.id.txtUsuario)).getText().toString();
        password = ((EditText) findViewById(R.id.txtPass)).getText().toString();

        currentVersionName = BuildConfig.VERSION_NAME;

        TextView tviu = (TextView) findViewById(R.id.versionApp);
        tviu.setText("V" + currentVersionName);

        borra_datos = (Button) findViewById(R.id.btnClearData);
        borra_datos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogsBiochem.muestraDialogoWarningCustom(Login.this, "Se borrarán los datos de la aplicación.\nDeseas continuar?", new DialogsBiochem.CustomAccion() {
                    @Override
                    public void customAccion() {
                        DialogsBiochem.muestraDialogoWarningCustom(Login.this, "La aplicación se cerrará al terminar de borrar los datos", new DialogsBiochem.CustomAccion() {
                            @Override
                            public void customAccion() {
                                try {
                                    // borra los datos de la aplicacion
                                    String packageName = getApplicationContext().getPackageName();
                                    Runtime runtime = Runtime.getRuntime();
                                    runtime.exec("pm clear " + packageName + " HERE");

                                } catch (Exception e) {
                                    DialogsBiochem.muestraDialogoError(Login.this, "Ocurrió un error al borrar los datos de la aplicación.\n"+e);
                                }
                            }
                        }, 1);
                    }
                }, 2);
            }
        });

        btn = (Button) findViewById(R.id.btnLogin);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pd = new AlertDialog.Builder(Login.this);
                pd.setCancelable(false);
                //LayoutInflater inflater = (LayoutInflater) CapturaPedidosFin.this.getSystemService( CapturaPedidosFin.this.LAYOUT_INFLATER_SERVICE );
                View view = getLayoutInflater().inflate(R.layout.dialog_transparente, null);
                ImageView imgCarga = (ImageView) view.findViewById(R.id.imgGif);
                Glide.with(Login.this)
                        .load(R.drawable.broilanimated)
                        .into(imgCarga);

                pd.setView(view);
                dialog = pd.create();
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


                usuario = ((EditText) findViewById(R.id.txtUsuario)).getText().toString();
                password = ((EditText) findViewById(R.id.txtPass)).getText().toString();

                final Boolean[] valida = {false};

                if (usuario.length() <= 0 || password.length() <= 0) {
                    ToastBiochem.toastSimple(Login.this, "Proporcione su usuario y password", 0);
                    return;
                }
                Boolean info = ranchoDB.checkAccesos();
                Log.e("@@checkAccesos", "vale => " + info);
                if (info == false) {
                    if (CheckNetwork.isInternetAvailable(Login.this)) //returns true if internet available
                    {
                        btn.setVisibility(View.GONE);
                        ranchoDB.resetGlAccesos();
                        new JsonTaskAccess(ranchoDB).execute();
                    } else {
                        ToastBiochem.toastSimple(Login.this, "Sin conexion a internet", 0);
                        return;
                    }
                } else {
                    dialog.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            valida[0] = ranchoDB.validacion(usuario, password);
                            Log.e("Valida user", "(valida[0])la validación es: "+valida[0]);
                            if (valida[0] == true) {

                                String tipo_usuario = ranchoDB.tipoUsuario(usuario);

                                if (tipo_usuario.equals("A")) {
                                    Intent explicit_intent;
                                    explicit_intent = new Intent(Login.this, NavDrawerActivity.class);
                                    explicit_intent.putExtra("usuario", usuario);
                                    siguienteActivity(explicit_intent, 1);
                                    //return;
                                } else {

                                    String ruta = ranchoDB.rutaSeleccionada();

                                    if (ruta.length() > 0) {
                                        Intent explicit_intent;
                                        explicit_intent = new Intent(Login.this, NavDrawerActivity.class);
                                        explicit_intent.putExtra("usuario", usuario);
                                        siguienteActivity(explicit_intent, 1);
                                        //return;
                                    } else {
                                        Intent explicit_intent;
                                        explicit_intent = new Intent(Login.this, EligeRuta.class);
                                        explicit_intent.putExtra("usuario", usuario);
                                        siguienteActivity(explicit_intent, 1);
                                        //return;
                                    }
                                }
                            } else {
                                ToastBiochem.toastSimple(Login.this, "El usuario no esta registrado o los datos son incorrectos", 0);
                                //return;
                            }
                            dialog.dismiss();
                        }
                    }, 1850);
                    return;
                }
            }
        });

        checkIsAndroidO();
    }

    //Este asynctask descarga los datos para poder autenticarse dentro de la aplicacion con las credenciales del sistema sybrem.
    private class JsonTaskAccess extends AsyncTask<String, String, String> {
        String url5 = "http://sybrem.com.mx/adsnet/syncmovil/java/catalogos.php?tabla=users&ruta=0";//http://sybrem.com.mx/adsnet/syncmovil/SyncMovil.php?ruta=0&tabla=axesos";
        String url4 = "http://sybrem.com.mx/adsnet/syncmovil/java/catalogos.php?tabla=rutas&ruta=0";//"http://sybrem.com.mx/adsnet/syncmovil/SyncMovil.php?ruta=0&tabla=rutas";
        //String urlTblOfertas = "http://sybrem.com.mx/adsnet/syncmovil/img_ofertas/img_existentes.php";

        // Bloque para poder usar los metodos de la clase MyDBHandler en la clase JsonTask
        private RanchoDB dbHandler;

        public JsonTaskAccess(RanchoDB dbHandler) {
            this.dbHandler = dbHandler;
        }
        // Termina bloque de inclusion de los metodos de MyDBHandler (Se uso esta practica debido a que ya esta usado extends en la clase

        protected void onPreExecute() {
            super.onPreExecute();

            pd = new AlertDialog.Builder(Login.this);
            pd.setCancelable(false);
            view = getLayoutInflater().inflate(R.layout.dialog_transparente, null);

            TextView tvClasif = (TextView) view.findViewById(R.id.txt);
            tvClasif.setText("Obteniendo datos de servidor, espere...");
            ImageView imagen = (ImageView) view.findViewById(R.id.imgGif);
            Glide.with(Login.this)
                    .load(R.drawable.tractorzaso)
                    .into(imagen);
            pd.setView(view);
            dialog = pd.create();
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimacionArribaAbajo;
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }

        protected String doInBackground(String... params) {

            /*************************************************************
             * Bloque para el catalogo de Accesos:                       *
             * ***********************************************************/
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            String JsonUrl5 = "";

            try {
                URL url = new URL(url5);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    // Log.d("Salida: ", "> " + line);   //Se hace una salida por monitoreo en la consola. ELIMINAR / COMENTAR
                }
                JsonUrl5 = buffer.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // Una vez obtenida la cadena JSon del segundo catalogo se lee linea por lina para insertar en la tabla
            try {
                // Convierte el string con la informaciÑn de los clientes en un array JSON
                JSONArray jsonArr_accesos = new JSONArray(JsonUrl5);

                for (int i = 0; i < jsonArr_accesos.length(); i++) {
                    JSONObject jsonObj5 = jsonArr_accesos.getJSONObject(i);
                    dbHandler.insertaGlAccesos(jsonObj5);
                }
            } // Fin del Try
            catch (JSONException e) {
                e.printStackTrace();
            } // fin del catch

            /*************************************************************
             * Bloque para el catalogo de Rutas:                         *
             * ***********************************************************/
            connection = null;
            reader = null;
            String JsonUrl4 = "";
            String tipoUser = dbHandler.tipoUsuario(usuario);
            Log.e("tipouser login", "tipoUser: "+tipoUser);
            if (tipoUser.equals("A")) {
                String ag_params = dbHandler.idAgentesParametros(usuario);
                url4 = "http://sybrem.com.mx/adsnet/syncmovil/java/catalogos.php?tabla=rutas&ruta=0&agtparams=" + ag_params;
            }
            try {
                URL url = new URL(url4);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                JsonUrl4 = buffer.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // Una vez obtenida la cadena JSon del segundo catalogo se lee linea por lina para insertar en la tabla
            try {
                // Convierte el string con la informacion de los clientes en un array JSON
                JSONArray jsonArr_rutas = new JSONArray(JsonUrl4);

                for (int i = 0; i < jsonArr_rutas.length(); i++) {
                    JSONObject jsonObj4 = jsonArr_rutas.getJSONObject(i);
                    dbHandler.insertaVnCatRutas(jsonObj4);
                }
            } // Fin del Try
            catch (JSONException e) {
                e.printStackTrace();
            } // fin del catch

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            pd = new AlertDialog.Builder(Login.this);
            pd.setCancelable(false);
            View view = getLayoutInflater().inflate(R.layout.dialog_transparente, null);
            ImageView imgCarga = (ImageView) view.findViewById(R.id.imgGif);
            Glide.with(Login.this)
                    .load(R.drawable.broilanimated)
                    .into(imgCarga);

            pd.setView(view);
            dialog = pd.create();
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Boolean valida = dbHandler.validacion(usuario, password);
                    Log.e("Valida user", "la validación es: "+valida);
                    if (valida == true) {
                        String tipo_usuario = dbHandler.tipoUsuario(usuario);

                        if (tipo_usuario.equals("A")) {
                            Intent explicit_intent;
                            explicit_intent = new Intent(Login.this, NavDrawerActivity.class);
                            explicit_intent.putExtra("usuario", usuario);
                            siguienteActivity(explicit_intent, 1);
                            //return;
                        } else {

                            String ruta = dbHandler.rutaSeleccionada();

                            if (ruta.length() > 0) {
                                Intent explicit_intent;
                                explicit_intent = new Intent(Login.this, NavDrawerActivity.class);
                                explicit_intent.putExtra("usuario", usuario);
                                siguienteActivity(explicit_intent, 1);
                                //return;
                            } else {
                                Intent explicit_intent;
                                explicit_intent = new Intent(Login.this, EligeRuta.class);
                                explicit_intent.putExtra("usuario", usuario);
                                siguienteActivity(explicit_intent, 1);
                                //return;
                            }
                        }
                    } else {
                        ToastBiochem.toastSimple(Login.this, "El usuario no esta registrado o los datos son incorrectos", 0);
                        //return;
                    }
                    dialog.dismiss();
                    btn.setVisibility(View.VISIBLE);
                }
            }, 1750);
        }
    } // --------------------------------------------------Fin de la clase JsonTaskAccess-------------------------------------------------

    public void siguienteActivity(Intent explicit_intent, int clear) {
        explicit_intent.putExtra("usuario", usuario);
        btn.setVisibility(View.VISIBLE);
        if (clear == 0) {
            startActivity(explicit_intent);
            overridePendingTransition(R.anim.fab_open, R.anim.fab_close);
        } else {
            startActivity(explicit_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            overridePendingTransition(R.anim.fab_open, R.anim.fab_close);
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }

    private void checkIsAndroidO() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            boolean result = getPackageManager().canRequestPackageInstalls();
            if (result) {
                comenzarActualizar();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.REQUEST_INSTALL_PACKAGES}, INSTALL_PACKAGES_REQUESTCODE);
            }
        } else {
            comenzarActualizar();
        }
    }

    private void comenzarActualizar() {
        //Para tener el contexto mas a mano.
        context = this;
        //Creamos el Autoupdater.
        updater = new Autoupdater(Login.this);
        //Ponemos a correr el ProgressBar.
        //Ejecutamos el primer metodo del Autoupdater.
        updater.DownloadData(finishBackgroundDownload);
    }

    private Runnable finishBackgroundDownload = new Runnable() {
        @Override
        public void run() {
            //Volvemos el ProgressBar a invisible.
            //Comprueba que halla nueva versión.
            if (updater.isNewVersionAvailable()) {
                //muestra una notificacion de nueva version
                NotificationCompat.Builder mBuilder;
                NotificationManager mNotifyMgr = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
                int icono = R.drawable.ic_carrito;
                Intent i = new Intent(Login.this, Login.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        getApplicationContext(),
                        0,
                        new Intent(), // add this
                        PendingIntent.FLAG_UPDATE_CURRENT);
                mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(getApplicationContext())
                        .setContentIntent(pendingIntent)
                        .setSmallIcon(icono)
                        .setContentTitle("Nueva versión " + updater.getLatestVersionName())
                        .setContentText("Descarga la nueva versión de la aplicación Biochem")
                        .setAutoCancel(true);
                mNotifyMgr.notify(1, mBuilder.build());
                //mNotifyMgr.createNotificationChannel();

                //Crea mensaje con datos de versión.
                String msj = "¡Nueva versión disponible!";
                msj += "\n" + updater.getMejoras();
                msj += "\nVersión actual: " + updater.getCurrentVersionName();
                msj += "\n\nÚltima versión: " + updater.getLatestVersionName();
                msj += "\n\nSe actualizará a la nueva versión";

                DialogsBiochem.muestraDialogoWarningCustom(Login.this, msj, new DialogsBiochem.CustomAccion() {
                    @Override
                    public void customAccion() {
                        updater.InstallNewVersion(null);
                    }
                },2);
            } else {
                Log.e("", "No Hay actualizaciones de la appBiochem");
            }
        }
    };
}
