package mx.com.sybrem.apprancho.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Button;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import mx.com.sybrem.apprancho.R;

public class DescargaVideos extends AppCompatActivity {

    String linkDescarga = "";
    private static double SPACE_KB = 1024;
    private static double SPACE_MB = 1024 * SPACE_KB;
    private static double SPACE_GB = 1024 * SPACE_MB;
    private static double SPACE_TB = 1024 * SPACE_GB;

    public int cantidadVidios = 0;
    ProgressDialog pd;
    int bandera = 0;

    ArrayList<String> linksDescarga;
    String BASE_URL = "http://sybrem.com.mx/adsnet/syncmovil/java/videosMaiceros/";
    String linkCompleto = "";

    ProgressDialog pdDescarga;
    private Button button;
    private long downloadID;
    private BroadcastReceiver onDownloadComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            if (downloadID == id) {
                Intent explicit_intent = new Intent(DescargaVideos.this, VideosBiochem.class);
                explicit_intent.putExtra("descargad", "1");
                startActivity(explicit_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                finish();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descarga_videos);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        linkDescarga = (String) extras.get("linkDescarga");

        registerReceiver(onDownloadComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        beginDownload();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onDownloadComplete);
    }

    private void beginDownload() {

        File file = new File(Environment.getExternalStorageDirectory() + "/download/BiochemFiles/vidios/" + linkDescarga);
        if(file.exists())file.delete();
        /*
        Create a DownloadManager.Request with all the information necessary to start the download
         */
        linkCompleto = BASE_URL + linkDescarga;
        DownloadManager.Request request = null;// Set if download is allowed on roaming network
        request = new DownloadManager.Request(Uri.parse(linkCompleto))
                .setTitle("Descarga de videos Biochem")// Title of the Download Notification
                .setDescription("Descargando...")// Description of the Download Notification
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)// Visibility of the download Notification
                .setDestinationUri(Uri.fromFile(file))// Uri of the destination file
                //.setRequiresCharging(false)// Set if charging is required to begin the download
                .setAllowedOverMetered(true)// Set if download is allowed on Mobile network
                .setAllowedOverRoaming(true);
        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        downloadID = downloadManager.enqueue(request);// enqueue puts the download request in the queue.


        final ProgressDialog progressBarDialog = new ProgressDialog(this);
        progressBarDialog.setTitle("Descargando video " + linkDescarga);
        progressBarDialog.setCancelable(false);

        progressBarDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressBarDialog.setProgress(0);

        new Thread(new Runnable() {

            @Override
            public void run() {
                boolean downloading = true;

                DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                while (downloading) {

                    DownloadManager.Query q = new DownloadManager.Query();
                    q.setFilterById(downloadID); //filter by id which you have receieved when reqesting download from download manager
                    Cursor cursor = manager.query(q);
                    cursor.moveToFirst();
                    int bytes_downloaded = cursor.getInt(cursor
                            .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));

                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                        downloading = false;
                    }

                    final int dl_progress = (int) ((bytes_downloaded * 100l) / bytes_total);

                    final int descargao = bytes_downloaded;
                    final int descargax = bytes_total;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBarDialog.setProgressNumberFormat((bytes2String(descargao)) + "/" + (bytes2String(descargax)));
                            progressBarDialog.setProgress(dl_progress);
                        }
                    });
                    cursor.close();
                }
            }
        }).start();
        progressBarDialog.show();
    }

    public static String bytes2String(long sizeInBytes) {

        NumberFormat nf = new DecimalFormat();
        nf.setMaximumFractionDigits(2);

        try {
            if ( sizeInBytes < SPACE_KB ) {
                return nf.format(sizeInBytes) + " Byte(s)";
            } else if ( sizeInBytes < SPACE_MB ) {
                return nf.format(sizeInBytes/SPACE_KB) + " KB";
            } else if ( sizeInBytes < SPACE_GB ) {
                return nf.format(sizeInBytes/SPACE_MB) + " MB";
            } else if ( sizeInBytes < SPACE_TB ) {
                return nf.format(sizeInBytes/SPACE_GB) + " GB";
            } else {
                return nf.format(sizeInBytes/SPACE_TB) + " TB";
            }
        } catch (Exception e) {
            return sizeInBytes + " Byte(s)";
        }
    }
}
