package mx.com.sybrem.apprancho.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mx.com.sybrem.apprancho.R;
import mx.com.sybrem.apprancho.clases.Agente;
import mx.com.sybrem.apprancho.clases.RanchoDB;
import mx.com.sybrem.apprancho.clases.adapters.AdapterAgente;
import mx.com.sybrem.apprancho.clases.general.CheckNetwork;
import mx.com.sybrem.apprancho.clases.general.ToastBiochem;

public class EligeRuta extends AppCompatActivity implements SearchView.OnQueryTextListener{

    RanchoDB dbHandler;
    TextView txtUsuario;
    ListView listadoAgentes;
    private String agentes;

    private  static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "bladeTablet.db";

    AlertDialog.Builder pd;
    AlertDialog dialog;
    ProgressDialog pdSender; // Mensaje de progreso en sincronización para JSonTaskSender.
    ProgressDialog pdSendPayment; // Mensaje de progreso en sincronizacion.
    ProgressDialog pdSendVisitas; // Mensaje de progreso en sincronizacion Visitas.
    ProgressDialog pdSendProspectos;// Mensaje de progreso en sincronizacion Prospectos.
    ProgressDialog pdSendComprobaciones; // Mensaje de progreso en sincronizacion Visitas.
    AdapterAgente adapter_agente;
    ArrayList<Agente> arrListAgente;
    ListView LstAgentes;
    View view;

    SearchView buscaAgente;
    Filter filtroAgente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elige_ruta);

        dbHandler = new RanchoDB(this, null, null, DATABASE_VERSION);
        dbHandler.checkDBStatus(); // Tiene como fin forzar la creacion de la base de datos.

        //Se pasan los datos del usuario que se logeo
        String usuarioLogeado = "";
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        usuarioLogeado = (String) extras.get("usuario");

        //se asigna la variable al objeto textvie
        txtUsuario = (TextView)findViewById(R.id.textViewUsuarioLog);
        txtUsuario.setText("Usuario: " + usuarioLogeado);

        //Actuaizamos en la bitacora el usuario logeado
        dbHandler.registraBitacora(usuarioLogeado);

        //Obtenemos el listado de los agentes de la base de datos
        agentes = dbHandler.getListadoAgentes();
        arrListAgente = new ArrayList<Agente>();
        if (!agentes.equals("[]")){
            try {
                JSONArray jsonReporte = new JSONArray(agentes);
                for (int i3 = 0; i3 < jsonReporte.length(); i3++) {
                    JSONObject obj = jsonReporte.getJSONObject(i3);
                    String num_ruta = obj.getString("num_ruta").toString();
                    String nombre_agente = obj.getString("nombre_agente").toString();
                    arrListAgente.add(new Agente(num_ruta, nombre_agente));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        buscaAgente = (SearchView)findViewById(R.id.buscaAgente);
        LstAgentes = (ListView)findViewById(R.id.LstAgentes);
        adapter_agente = new AdapterAgente(EligeRuta.this, arrListAgente);
        LstAgentes.setAdapter(adapter_agente);
        LstAgentes.setDivider(null);
        LstAgentes.setTextFilterEnabled(true);
        filtroAgente = adapter_agente.getFilter();
        setupSearchView();

        LstAgentes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Agente agenteListView = (Agente) adapter_agente.getItem(position);
                String num_rutax = agenteListView.getNum_ruta();
                Log.e("RUTA ELEGIDA", "Ruta elegida: "+ num_rutax + " " + agenteListView.getNombre_agente());
                Toast.makeText(EligeRuta.this, "Ruta elegida: "+ num_rutax + " " + agenteListView.getNombre_agente(), Toast.LENGTH_SHORT).show();
                dbHandler.registraRutaSeleccionada(num_rutax);

                if(CheckNetwork.isInternetAvailable(EligeRuta.this)){
                    // Se tienen que volver a descargar los catalogos del sistema.
                    dbHandler.resetCatalogs(); // Elimina la información de los catálogos existentes
                    new ObtieneDatos(dbHandler).execute(); // Descarga de la base de datos los catalogos.
                }else{
                    ToastBiochem.toastSimple(EligeRuta.this, "Sin conexion a Internet",0);
                }
            }
        });
    }

    private void setupSearchView() {
        buscaAgente.setIconifiedByDefault(false);
        buscaAgente.setOnQueryTextListener(this);
        buscaAgente.setSubmitButtonEnabled(true);
        buscaAgente.setQueryHint("Buscar un agente");
    }
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            filtroAgente.filter("");
        } else {
            filtroAgente.filter(newText);
        }
        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    //Metodo para refrescar la pantalla cuando se guarda el pago
    public void reload(){
        finish();
        RanchoDB dbHandler = new RanchoDB(EligeRuta.this, null, null, 1);
        dbHandler.checkDBStatus(); // Tiene como fin forzar la creacion de la base de datos.
        String usuario = dbHandler.ultimoUsuarioRegistrado();
        Intent explicit_intent;
        explicit_intent = new Intent(EligeRuta.this, NavDrawerActivity.class);
        explicit_intent.putExtra("usuario", usuario);
        startActivity(explicit_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
        overridePendingTransition(R.anim.fab_open,R.anim.fab_close);
    }

    private class ObtieneDatos extends AsyncTask<String, String, String>
    {
        // Bloque para poder usar los metodos de la clase MyDBHandler en la clase JsonTask
        public RanchoDB dbHandler;

        public ObtieneDatos(RanchoDB dbHandler)
        {
            this.dbHandler = dbHandler;
        }
        // Termina bloque de inclusion de los metodos de MyDBHandler (Se uso esta practica debido a que ya esta usado extends en la clase

        protected void onPreExecute()
        {
            super.onPreExecute();
            pd = new AlertDialog.Builder(EligeRuta.this);
            pd.setCancelable(false);
            view = getLayoutInflater().inflate(R.layout.dialog_transparente, null);

            TextView tvClasif = (TextView) view.findViewById(R.id.txt);
            tvClasif.setText("Obteniendo datos de servidor, espere...");
            ImageView imagen = (ImageView) view.findViewById(R.id.imgGif);
            Glide.with(EligeRuta.this)
                    .load(R.drawable.tractorzaso)
                    .into(imagen);
            pd.setView(view);
            dialog = pd.create();
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimacionArribaAbajo;
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }

        protected String doInBackground(String... params)
        {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND + Process.THREAD_PRIORITY_MORE_FAVORABLE);
            String usuarioLog = dbHandler.ultimoUsuarioRegistrado();
            String TipoUsuario = dbHandler.tipoUsuario(usuarioLog);
            String ruta = "";

            if(TipoUsuario.equals("A")) {
                ruta = dbHandler.num_ruta(usuarioLog);
            }
            else{
                ruta = dbHandler.rutaSeleccionada();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);

            if (dialog.isShowing())
            {
                dialog.dismiss();
                reload();
            }

        }
    }
}
