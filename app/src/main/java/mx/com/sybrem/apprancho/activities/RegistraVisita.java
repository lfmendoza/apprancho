package mx.com.sybrem.apprancho.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.barteksc.pdfviewer.util.ArrayUtils;
import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.github.johnpersano.supertoasts.library.utils.PaletteUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.stream.Stream;

import mx.com.sybrem.apprancho.BuildConfig;
import mx.com.sybrem.apprancho.R;
import mx.com.sybrem.apprancho.clases.FotoVisita;
import mx.com.sybrem.apprancho.clases.RanchoDB;
import mx.com.sybrem.apprancho.clases.general.CheckNetwork;
import mx.com.sybrem.apprancho.clases.general.DialogsBiochem;
import mx.com.sybrem.apprancho.clases.general.FiltroConEspacioAdapter;
import mx.com.sybrem.apprancho.clases.general.GlobalsBioshem;
import mx.com.sybrem.apprancho.clases.general.MiInterpoladorBounce;

import static android.view.View.GONE;

public class RegistraVisita extends AppCompatActivity implements DialogsBiochem.CustomAccion, View.OnClickListener {

    RanchoDB dbHandler;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "ranchoTablet.db";

    String usuario = "", ruta = "";
    AlertDialog.Builder pd2;
    AlertDialog dialog;

    LinearLayout partOne, partTwo, partTres, layPhotos;
    AutoCompleteTextView nameRancho;
    Button btnFechaVisita, btnSigue, btnFechaProxima, btnAnterior, btnSigue2, btnAnterior2, btnTermina, btnAgregProd;
    EditText txtCultivo, txtSiembra, txtProblema, txtSolucion, txtObservaciones;
    TextView txtFechaMostrar, txtFechaProxima;
    TableLayout tblProductos, tblFotos;

    ImageButton btnCamaga;

    String cultivo = "", siembra = "", problema = "", solucion = "", observaciones = "";

    int idImg = 20000, idRowPhoto = 21000, idBtnFoto = 23000, idNamePhoto = 22000;
    int idTableRow = 3000, idBotonDelete = 4000, idProds = 5000, idProdCantidad = 6000, /*idProdLote = 7000,*/
            idNameProds = 8000, basurax;

    int dia = 0, mes = 0, año = 0;
    String fechaVisita = "", fechaProximaVisita = "", cliente_aux = "";

    String[] ranchos, ranchosSync, ranchosNoSync;
    FiltroConEspacioAdapter<String> adapterRanchos;

    Animation entra, sale;
    Animation myAnim;
    MiInterpoladorBounce interpolator;

    private final static int RESULT_IFE = 0;
    int flag_chingona = 0, idTaked = 0;
    String bArray_IFE = "";

    ArrayList<FotoVisita> fotosTomadas, fotoAux;
    Typeface classico;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registra_visita);

        dbHandler = new RanchoDB(RegistraVisita.this, DATABASE_NAME, null, DATABASE_VERSION);
        dbHandler.checkDBStatus();

        usuario = dbHandler.ultimoUsuarioRegistrado();
        classico = ResourcesCompat.getFont(RegistraVisita.this, R.font.classico);

        String TipoUsuario = dbHandler.tipoUsuario(usuario);

        if (TipoUsuario.equals("A")) {
            ruta = dbHandler.num_ruta(usuario);
        } else {
            ruta = dbHandler.rutaSeleccionada();
        }
        ranchosSync = dbHandler.getRanchos("1");
        ranchosNoSync = dbHandler.getRanchos("0");
        ranchos = new String[ranchosSync.length + ranchosNoSync.length];

        System.arraycopy(ranchosSync, 0, ranchos, 0, ranchosSync.length);
        System.arraycopy(ranchosNoSync, 0, ranchos, ranchosSync.length, ranchosNoSync.length);

        entra = AnimationUtils.loadAnimation(RegistraVisita.this, R.anim.fade_in);
        sale = AnimationUtils.loadAnimation(RegistraVisita.this, R.anim.fade_out);

        myAnim = AnimationUtils.loadAnimation(RegistraVisita.this, R.anim.bounce);
        // Use bounce interpolator with amplitude 0.2 and frequency 20
        interpolator = new MiInterpoladorBounce(0.2, 20);

        fotosTomadas = new ArrayList<>();

        basurax = R.drawable.wrong;

        partOne = (LinearLayout) findViewById(R.id.partOne);
        partTwo = (LinearLayout) findViewById(R.id.partTwo);
        partTres = (LinearLayout) findViewById(R.id.partTres);
        layPhotos = (LinearLayout) findViewById(R.id.layPhotos);

        nameRancho = (AutoCompleteTextView) findViewById(R.id.nameRancho);

        btnFechaVisita = (Button) findViewById(R.id.btnFechaVisita);
        btnSigue = (Button) findViewById(R.id.btnSigue);
        btnFechaProxima = (Button) findViewById(R.id.btnFechaProxima);
        btnAnterior = (Button) findViewById(R.id.btnAnterior);
        btnSigue2 = (Button) findViewById(R.id.btnSigue2);
        btnAnterior2 = (Button) findViewById(R.id.btnAnterior2);
        btnTermina = (Button) findViewById(R.id.btnTermina);
        btnAgregProd = (Button) findViewById(R.id.btnAgregProd);

        txtCultivo = (EditText) findViewById(R.id.txtCultivo);
        txtSiembra = (EditText) findViewById(R.id.txtSiembra);
        txtProblema = (EditText) findViewById(R.id.txtProblema);
        txtSolucion = (EditText) findViewById(R.id.txtSolucion);
        txtObservaciones = (EditText) findViewById(R.id.txtObservaciones);

        txtFechaMostrar = (TextView) findViewById(R.id.txtFechaMostrar);
        txtFechaProxima = (TextView) findViewById(R.id.txtFechaProxima);

        tblProductos = (TableLayout) findViewById(R.id.tblProductos);
        tblFotos = (TableLayout) findViewById(R.id.tblFotos);

        btnCamaga = (ImageButton) findViewById(R.id.btnCamaga);

        adapterRanchos = new FiltroConEspacioAdapter<String>(RegistraVisita.this, R.layout.autocomplete_letra_grande, ranchos);
        nameRancho.setAdapter(adapterRanchos);

        nameRancho.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                cliente_aux = adapterRanchos.getItem(pos);
            }
        });

        btnFechaVisita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionaFecha(view);
            }
        });
        btnFechaProxima.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionaFecha(view);
            }
        });

        btnSigue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cultivo = txtCultivo.getText().toString();
                siembra = txtSiembra.getText().toString();
                problema = txtProblema.getText().toString();
                solucion = txtSolucion.getText().toString();

                int errores = 0;
                String msje = "";
                if (cliente_aux.length() < 1) {
                    errores++;
                    msje += "* Debe proporcionar el nombre del rancho.\n";
                }
                if (cultivo.length() < 5) {
                    errores++;
                    msje += "* Debe proporcionar el cultivo.\n";
                }
                if (siembra.length() < 1) {
                    errores++;
                    msje += "* Debe proporcionar la extenseión de la siembra.\n";
                } else {
                    int has = Integer.parseInt(siembra);
                    if (has < 1) {
                        errores++;
                        msje += "* La extensión de la siembra debe ser mayor a cero.\n";
                    }
                }
                if (problema.length() < 6) {
                    errores++;
                    msje += "* Debe proporcionar el problema.\n";
                }
                if (solucion.length() < 6) {
                    errores++;
                    msje += "* Debe proporcionar la solución al problema.\n";
                }
                if (fechaVisita.length() < 1) {
                    errores++;
                    msje += "* Debe proporcionar la fecha de la visita.\n";
                }

                if (errores > 0) {
                    DialogsBiochem.muestraDialogoError(RegistraVisita.this, msje);
                } else {
                    partOne.startAnimation(sale);
                    partOne.setVisibility(View.GONE);
                    partTwo.setVisibility(View.VISIBLE);
                    partTwo.startAnimation(entra);
                }
            }
        });

        btnAnterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                partTwo.startAnimation(sale);
                partTwo.setVisibility(View.GONE);
                partOne.setVisibility(View.VISIBLE);
                partOne.startAnimation(entra);
            }
        });

        btnAgregProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder dialog_agrega = new AlertDialog.Builder(RegistraVisita.this);
                dialog_agrega.setCancelable(false);
                View dialogAgrega_producto = getLayoutInflater().inflate(R.layout.dialog_agrega_producto2, null);

                TextView multiplosProducto = (TextView) dialogAgrega_producto.findViewById(R.id.multiplosProducto);
                TextView precioProduct = (TextView) dialogAgrega_producto.findViewById(R.id.precioProduct);
                TextView precioProduct2 = (TextView) dialogAgrega_producto.findViewById(R.id.precioProduct2);
                TextView existenciaAgente = (TextView) dialogAgrega_producto.findViewById(R.id.existenciaAgente);

                String[] productos = dbHandler.getAllInvCatProductos();

                AutoCompleteTextView autoCompleteProductoPopup = (AutoCompleteTextView) dialogAgrega_producto.findViewById(R.id.autoCompleteProductoPopup);
                FiltroConEspacioAdapter<String> adapterProductos;
                adapterProductos = new FiltroConEspacioAdapter<String>(RegistraVisita.this, R.layout.autocomplete_letra_grande, productos);
                autoCompleteProductoPopup.setAdapter(adapterProductos);//*/

                LinearLayout selOrigen = (LinearLayout) dialogAgrega_producto.findViewById(R.id.selOrigen);
                selOrigen.setVisibility(GONE);

                EditText etCantProduct = (EditText) dialogAgrega_producto.findViewById(R.id.etCantProduct);
                //EditText etLoteProduct = (EditText) dialogAgrega_producto.findViewById(R.id.etLoteProduct);
                final String[] producto_seleccionao = {""}, extendedx = {""};

                //String is_almax = (id_tipo_venta.equals("3") && spExtendid.getSelectedItem().toString().equals("Almacen")) ? "Pasa" : "";
                autoCompleteProductoPopup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
                        String seleccionado = adapterProductos.getItem(pos).toString();
                        String[] split_seleccionao = seleccionado.split("-");
                        String cve_cat_prodicto = split_seleccionao[0];
                        multiplosProducto.setText("Ingrese con múltiplos de: " + dbHandler.pzasPaquetex(cve_cat_prodicto));
                        multiplosProducto.setVisibility(View.GONE);
                        producto_seleccionao[0] = adapterProductos.getItem(pos);
                    }
                });

                TextWatcher tw = new TextWatcher() {

                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        double precio_realtime = 0;
                        String f = "" + charSequence;
                        if (f.length() > 0) {
                            if (producto_seleccionao[0].length() > 10) {
                                String[] split_seleccionao = producto_seleccionao[0].split("-");
                                String cve_cat_prodicto = split_seleccionao[0];
                                double precio = dbHandler.precioProducto(cve_cat_prodicto);
                                double canti_prod = Double.parseDouble(f);
                                precio_realtime = precio * canti_prod;
                                precioProduct.setText("Precio: $" + String.format("%.2f", precio));
                                precioProduct2.setText("Total de lo ingresado: $" + String.format("%.2f", precio_realtime));
                                precioProduct.setVisibility(View.VISIBLE);
                                precioProduct2.setVisibility(View.VISIBLE);
                            } else {
                                precioProduct.setVisibility(GONE);
                                precioProduct2.setVisibility(GONE);
                            }
                        } else {
                            if (producto_seleccionao[0].length() > 10) {
                                String[] split_seleccionao = producto_seleccionao[0].split("-");
                                String cve_cat_prodicto = split_seleccionao[0];
                                double precio = dbHandler.precioProducto(cve_cat_prodicto);
                                precioProduct.setText("Precio: $" + String.format("%.2f", precio));
                                precioProduct2.setText("Total de lo ingresado: $" + String.format("%.2f", precio_realtime));
                                precioProduct.setVisibility(View.VISIBLE);
                                precioProduct2.setVisibility(View.VISIBLE);
                            } else {
                                precioProduct.setVisibility(GONE);
                                precioProduct2.setVisibility(GONE);
                            }
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                };
                etCantProduct.addTextChangedListener(tw);

                Button agrega = (Button) dialogAgrega_producto.findViewById(R.id.btnAgrega);
                Button cerrar = (Button) dialogAgrega_producto.findViewById(R.id.btnCierra);


                dialog_agrega.setView(dialogAgrega_producto);
                final AlertDialog dialog = dialog_agrega.create();
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                //Si se presiona el boton Aceptar del Dialog
                agrega.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String msje_errores = "";
                        int bandera_errores = 0;
                        double existenciaAgente = 0;

                        String nom_product = autoCompleteProductoPopup.getText().toString();
                        String cantidad_producto = etCantProduct.getText().toString();
                        //String lote_product = etLoteProduct.getText().toString();

                        String cve_cliente = "";

                        if (nom_product.length() < 10) {
                            bandera_errores++;
                            msje_errores += "Debe seleccionar un producto.\n";
                        } else {
                            if (producto_seleccionao[0].length() < 1) {
                                bandera_errores++;
                                msje_errores += "Debe seleccionar un producto.\n";
                            }
                        }
                        if (cantidad_producto.length() < 1) {
                            bandera_errores++;
                            msje_errores += "Debe escribir la cantidad del producto.\n";
                        } else {
                            double cant_prod_double = Double.parseDouble(cantidad_producto);
                            if (cant_prod_double <= 0) {
                                bandera_errores++;
                                msje_errores += "Debe escribir la cantidad del producto.\n";
                            }
                        }
                        /*if (lote_product.length() < 4) {
                            bandera_errores++;
                            msje_errores += "Debe escribir el lote del producto.\n";
                        }*/
                        // Si existen errores al guardar
                        if (bandera_errores > 0) {
                            String text = msje_errores;
                            //dialog.dismiss();
                            DialogsBiochem.muestraDialogoError(RegistraVisita.this, text);
                        }
                        // Si NO existen errores al guardar
                        else {
                            double cant_prod_double = Double.parseDouble(cantidad_producto);
                            String[] separated = nom_product.split("-");
                            String cve_cat_producto = separated[0];
                            //si el pedido es de almacen y no es oferta rancho
                            //boolean pzas_validas = dbHandler.piezasPaquete(cve_cat_producto, cant_prod_double);
                            /*if (!pzas_validas) {
                                String msje_pzas = dbHandler.pzasPaquete(cve_cat_producto) + "\nLa cantidad ingresada es de " + cantidad_producto;
                                String text = "Error al guardar!!\n" + msje_pzas;
                                dialog.dismiss();
                                DialogsBiochem.muestraDialogoError(Devoluciones.this, text);
                                return;
                            } else {*/
                            dialog.dismiss();
                            agregaProducto(nom_product, cve_cat_producto, (int) cant_prod_double + "");
                        }
                    }
                });

                //Si se presiona el boton Cancelar del Dialog
                cerrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        multiplosProducto.setVisibility(GONE);
                        precioProduct.setVisibility(GONE);
                        precioProduct2.setVisibility(GONE);
                        existenciaAgente.setVisibility(GONE);
                        dialog.dismiss();
                        return;
                    }
                });
                return;
            }
        });

        btnSigue2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                observaciones = txtObservaciones.getText().toString();
                int bandera_errores = 0;
                String msje_errores = "";

                if (observaciones.length() < 6) {
                    bandera_errores++;
                    msje_errores += "* Debe proporcionar algunas observaciones.\n";
                }

                if (fechaProximaVisita.length() < 1) {
                    bandera_errores++;
                    msje_errores += "* Debe proporcionar la fecha de la próxima visita.\n";
                }

                boolean hayProds = false;
                for (int i = 3001; i <= idTableRow; i++) {
                    int idCveCatProdx = i + 2000;

                    TextView tvCveCatProd = (TextView) findViewById(idCveCatProdx);
                    if (tvCveCatProd != null) {
                        hayProds = true;
                        break;
                    }
                }
                if (hayProds == false) {
                    bandera_errores++;
                    msje_errores += "* Debe ingresar los productos de muestra.\n";
                }

                if (bandera_errores > 0) {
                    DialogsBiochem.muestraDialogoError(RegistraVisita.this, msje_errores);
                } else {
                    partTwo.startAnimation(sale);
                    partTwo.setVisibility(View.GONE);
                    partTres.setVisibility(View.VISIBLE);
                    partTres.startAnimation(entra);
                }
            }
        });

        btnAnterior2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                partTres.startAnimation(sale);
                partTres.setVisibility(View.GONE);
                partTwo.setVisibility(View.VISIBLE);
                partTwo.startAnimation(entra);
            }
        });

        btnTermina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int errores = 0;
                String msje = "";

                if(fotosTomadas.size() < 1){
                    errores++;
                    msje += "Debe tomar algunas fotos para comprobar su visita.\n";
                }

                if(errores > 0){
                    DialogsBiochem.muestraDialogoError(RegistraVisita.this, msje);
                }else{
                    String ransho_movil = "0", ransho = "0", visita_mamalona = "";
                    String siguienteVisita = dbHandler.getSiguienteVisita();
                    if(cliente_aux.contains("]")){
                        String[] split_ransho = cliente_aux.split("\\]");
                        String[] split_mamalon = split_ransho[0].split("\\[");
                        ransho_movil = split_mamalon[1];
                        visita_mamalona = siguienteVisita + "-"+ransho_movil;
                    }else{
                        String[] split_perron = cliente_aux.split("-");
                        ransho = split_perron[0];
                        visita_mamalona = siguienteVisita + "-"+ransho;
                    }
                    String muestrasString = "";
                    for (int i = 3001; i <= idTableRow; i++) {
                        int idCveCatProdx = i + 2000;
                        int idNamedProds = i + 5000;
                        int idCanti = i + 3000;

                        TextView tvCveCatProd = (TextView) findViewById(idCveCatProdx);
                        if (tvCveCatProd != null) {
                            TextView tvNomProd = (TextView)findViewById(idNamedProds);
                            String nameProd = tvNomProd.getText().toString();
                            TextView tvCantidadProd = (TextView)findViewById(idCanti);
                            String cantidadx = tvCantidadProd.getText().toString();
                            muestrasString += nameProd + " " + cantidadx +"\n";
                        }
                    }
                    ContentValues visita = new ContentValues();
                    visita.put(dbHandler.COL_VNTRANCHOVISITA_ID_VISITA, "0");
                    visita.put(dbHandler.COL_VNTRANCHOVISITA_ID_VISITA_MOVIL, visita_mamalona);
                    visita.put(dbHandler.COL_VNTRANCHOVISITA_ID_RANCHO, ransho);
                    visita.put(dbHandler.COL_VNTRANCHOVISITA_ID_RANCHO_MOVIL, ransho_movil);
                    visita.put(dbHandler.COL_VNTRANCHOVISITA_FECHA_VISITA, fechaVisita);
                    visita.put(dbHandler.COL_VNTRANCHOVISITA_CULTIVO, cultivo);
                    visita.put(dbHandler.COL_VNTRANCHOVISITA_EXTENSION_SIEMBRA, siembra);
                    visita.put(dbHandler.COL_VNTRANCHOVISITA_PROBLEMA, problema);
                    visita.put(dbHandler.COL_VNTRANCHOVISITA_SOLUCION_BIOCHEM, solucion);
                    visita.put(dbHandler.COL_VNTRANCHOVISITA_MUESTRAS, muestrasString);
                    visita.put(dbHandler.COL_VNTRANCHOVISITA_PROXIMA_VISITA, fechaProximaVisita);
                    visita.put(dbHandler.COL_VNTRANCHOVISITA_OBSERVACIONES, observaciones);
                    visita.put(dbHandler.COL_VNTRANCHOVISITA_SINCRONIZADO, "0");
                    //Insertamos primera la visita
                    boolean insertaVisita = dbHandler.realizaInsert(visita, dbHandler.TABLE_VNT_RANCHO_VISITA);
                    if(insertaVisita == false){
                        DialogsBiochem.muestraDialogoError(RegistraVisita.this, "Ocurrió un error al insertar la visita en la tabla \""
                                + dbHandler.TABLE_VNT_RANCHO_VISITA + "\" del dispositivo " + "intente más tarde");
                    }else{
                        int errors = 0;
                        for (int i = 3001; i <= idTableRow; i++) {
                            int idCveCatProdx = i + 2000;
                            int idNamedProds = i + 5000;
                            int idCanti = i + 3000;

                            TextView tvCveCatProd = (TextView) findViewById(idCveCatProdx);
                            if (tvCveCatProd != null) {
                                String cveCatProdx = tvCveCatProd.getText().toString();
                                TextView tvCantidadProd = (TextView)findViewById(idCanti);
                                String cantidadx = tvCantidadProd.getText().toString();
                                ContentValues muestra = new ContentValues();
                                String siguienteMuestra = dbHandler.getSiguienteMuestra();

                                muestra.put(dbHandler.COL_VNTRANCHOMUESTRAS_ID_MUESTRA, "0");
                                muestra.put(dbHandler.COL_VNTRANCHOMUESTRAS_ID_MUESTRA_MOVIL, siguienteMuestra + "-"+visita_mamalona);
                                muestra.put(dbHandler.COL_VNTRANCHOMUESTRAS_ID_VISITA, "0");
                                muestra.put(dbHandler.COL_VNTRANCHOMUESTRAS_ID_VISITA_MOVIL, visita_mamalona);
                                muestra.put(dbHandler.COL_VNTRANCHOMUESTRAS_CVE_CAT_PRODUCTO, cveCatProdx);
                                muestra.put(dbHandler.COL_VNTRANCHOMUESTRAS_CANTIDAD, cantidadx);
                                muestra.put(dbHandler.COL_VNTRANCHOMUESTRAS_SINCRONIZADO, "0");

                                boolean insertaMuestra = dbHandler.realizaInsert(muestra, dbHandler.TABLE_VNT_RANCHO_MUESTRAS);
                                if(insertaMuestra == false){
                                    errors++;
                                    if(dbHandler.realizaDelete(dbHandler.TABLE_VNT_RANCHO_VISITA, dbHandler.COL_VNTRANCHOVISITA_ID, siguienteVisita) == false){
                                        DialogsBiochem.muestraDialogoError(RegistraVisita.this, "Ocurrió un error al borrar el registro de la tabla \"" +
                                                dbHandler.TABLE_VNT_RANCHO_VISITA + "\" ya que no se pudieron insertar las muestras de la visita");
                                    }
                                    break;
                                }
                            }
                        }
                        if(errors == 0){
                            int erroresFotos = 0;
                            for(FotoVisita foto: fotosTomadas){
                                String siguienteFoto = dbHandler.getSiguienteFoto();
                                ContentValues fotox = new ContentValues();

                                fotox.put(dbHandler.COL_VNTFOTOSVISITA_ID_FOTO, "0");
                                fotox.put(dbHandler.COL_VNTFOTOSVISITA_ID_FOTO_MOVIL, siguienteFoto+"-"+siguienteVisita);
                                fotox.put(dbHandler.COL_VNTFOTOSVISITA_ID_VISITA, "0");
                                fotox.put(dbHandler.COL_VNTFOTOSVISITA_ID_VISITA_MOVIL, visita_mamalona);
                                fotox.put(dbHandler.COL_VNTFOTOSVISITA_FOTO, foto.getBase64Img());
                                fotox.put(dbHandler.COL_VNTFOTOSVISITA_SINCRONIZADO, "0");

                                boolean insertaFoto = dbHandler.realizaInsert(fotox, dbHandler.TABLE_VNT_FOTOS_VISITA);
                                if(insertaFoto == false){
                                    erroresFotos++;
                                    if(dbHandler.realizaDelete(dbHandler.TABLE_VNT_FOTOS_VISITA, dbHandler.COL_VNTRANCHOMUESTRAS_ID_VISITA_MOVIL, visita_mamalona)){
                                        DialogsBiochem.muestraDialogoError(RegistraVisita.this, "Ocurrió un error al borrar el registro de la tabla \"" +
                                                dbHandler.TABLE_VNT_RANCHO_MUESTRAS + "\" ya que no se pudieron insertar las fotos de la visita");
                                    }
                                    break;
                                }
                            }
                            if(erroresFotos == 0){
                                if(CheckNetwork.isInternetAvailable(RegistraVisita.this)){
                                    new EnviaVisitas(dbHandler).execute();
                                }else{
                                    DialogsBiochem.muestraDialogoSuccessCustom(RegistraVisita.this, "Se guardó la visita en el dispositivo, conéctese a internet para enviar la información al servidor.", RegistraVisita.this::customAccion);
                                }
                            }
                        }
                    }
                }
            }
        });

        btnCamaga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creamos el Intent para llamar a la Camara
                Intent cameraIntent = new Intent(
                        android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                //Creamos una carpeta en la memeria del terminal
                File imagesFolder = new File(
                        Environment.getExternalStorageDirectory(), "Biochem_Files");
                imagesFolder.mkdirs();
                //añadimos el nombre de la imagen
                File image = new File(imagesFolder, "rancho.jpg");
                if (image.exists()) image.delete();
                Uri uriSavedImage;
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                    uriSavedImage = Uri.fromFile(image);
                } else {
                    uriSavedImage = FileProvider.getUriForFile(RegistraVisita.this,
                            BuildConfig.APPLICATION_ID + ".provider",
                            image);
                }
                //Le decimos al Intent que queremos grabar la imagen
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
                //Lanzamos la aplicacion de la camara con retorno (forResult)
                startActivityForResult(cameraIntent, RESULT_IFE);
            }
        });
    }

    public void agregaProducto(String nomProducto, String cveCatProducto, String cantidadProd) {

        TableRow tr = new TableRow(RegistraVisita.this);
        tr.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));
        idTableRow++;
        tr.setId(idTableRow);

        idProds++;
        TextView tvCveCatProd = new TextView(RegistraVisita.this);
        tvCveCatProd.setText(cveCatProducto);
        tvCveCatProd.setVisibility(View.GONE);
        tvCveCatProd.setId(idProds);
        tr.addView(tvCveCatProd);

        idBotonDelete++;
        Button deletePartida = new Button(RegistraVisita.this);
        deletePartida.setBackgroundResource(basurax);
        float scale = RegistraVisita.this.getResources().getDisplayMetrics().density;
        int pixels = (int) (40 * scale + 0.5f);
        deletePartida.setLayoutParams(new TableRow.LayoutParams(pixels, pixels));
        deletePartida.setId(idBotonDelete);
        deletePartida.setPadding(15, 15, 15, 15);
        deletePartida.setOnClickListener(RegistraVisita.this::onClick);
        tr.addView(deletePartida);

        idNameProds++;
        TextView tvNomProd = new TextView(RegistraVisita.this);
        tvNomProd.setText(nomProducto);
        tvNomProd.setPadding(5, 5, 5, 5);
        tvNomProd.setGravity(Gravity.LEFT);
        tvNomProd.setId(idNameProds);
        tvNomProd.setTypeface(classico, Typeface.BOLD);
        tr.addView(tvNomProd);

        idProdCantidad++;
        TextView tvCantProd = new TextView(RegistraVisita.this);
        tvCantProd.setText(cantidadProd);
        tvCantProd.setId(idProdCantidad);
        tvCantProd.setPadding(5, 5, 5, 5);
        tvCantProd.setGravity(Gravity.CENTER);
        tvCantProd.setTypeface(classico, Typeface.BOLD);
        tr.addView(tvCantProd);

        //idProdLote++;
        /*TextView tvLoteProd = new TextView(RegistraVisita.this);
        tvLoteProd.setText(loteProd);
        tvLoteProd.setId(idProdLote);
        tvLoteProd.setPadding(5, 5, 5, 5);
        tvLoteProd.setGravity(Gravity.CENTER);
        tr.addView(tvLoteProd);*/

        tblProductos.addView(tr);
        myAnim.setInterpolator(interpolator);
        tr.startAnimation(myAnim);
    }

    @Override
    public void onClick(View v) {
        int botonPulsao = v.getId();

        int idProdx = botonPulsao + 1000;
        int idNameProdx = botonPulsao + 4000;
        int idProdCantidadx = botonPulsao + 2000;
        //int idProdLotex = botonPulsao + 3000;

        TextView tvCveCatProd = (TextView) findViewById(idProdx);
        TextView tvNameProd = (TextView) findViewById(idNameProdx);
        TextView tvProdCantidad = (TextView) findViewById(idProdCantidadx);
        //TextView tvProdLote = (TextView) findViewById(idProdLotex);

        int idTableRowx = botonPulsao - 1000;
        TableRow tr = (TableRow) findViewById(idTableRowx);
        if (tr != null) {
            tblProductos.removeView(tr);
        }
        ///*
        SuperActivityToast.OnButtonClickListener onButtonClickListener = new SuperActivityToast.OnButtonClickListener() {
            @Override
            public void onClick(View view, Parcelable token) {
                agregaProducto(tvNameProd.getText().toString(), tvCveCatProd.getText().toString(), tvProdCantidad.getText().toString());
            }
        };

        SuperActivityToast.create(RegistraVisita.this, new Style(), Style.TYPE_BUTTON)
                .setButtonText("DESHACER")
                .setButtonIconResource(R.drawable.ic_deshacer)
                .setButtonDividerColor(PaletteUtils.getSolidColor(PaletteUtils.WHITE))
                .setOnButtonClickListener("", null, onButtonClickListener)
                .setText("El producto se eliminó de la lista correctamente")
                .setTextSize(Style.TEXTSIZE_LARGE)
                .setDuration(Style.DURATION_LONG)
                .setFrame(Style.FRAME_LOLLIPOP)
                .setColor(PaletteUtils.getSolidColor(PaletteUtils.MATERIAL_GREEN))
                .setAnimations(Style.ANIMATIONS_POP)
                .show();
        //*/
    }

    @Override
    public void customAccion() {
        finActivity();
    }

    public void seleccionaFecha(View v) {

        final Calendar c = Calendar.getInstance();
        dia = c.get(Calendar.DAY_OF_MONTH);
        mes = c.get(Calendar.MONTH);
        año = c.get(Calendar.YEAR);

        if (v == btnFechaVisita) {
            DatePickerDialog dpd = new DatePickerDialog(RegistraVisita.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int año, int mes, int dia) {
                    int mes_mamalon = mes + 1;
                    String mesAux = "" + mes_mamalon;
                    String mesString = (mesAux.length() == 1) ? "0" + mesAux : mesAux;
                    String dia_aux = "" + dia;
                    String dia_string = (dia_aux.length() == 1) ? "0" + dia_aux : dia_aux;
                    fechaVisita = año + "-" + mesString + "-" + dia_string;
                    String fecha_mostrar_vista = dia + " de " + getNombreMes(mes_mamalon) + " del " + año;
                    txtFechaMostrar.setText(fecha_mostrar_vista);
                    btnFechaVisita.setVisibility(View.GONE);
                    txtFechaMostrar.setVisibility(View.VISIBLE);
                }
            }, año, mes, dia);
            dpd.getDatePicker().setMaxDate(System.currentTimeMillis() - 2000);
            dpd.show();
        }

        if (v == btnFechaProxima) {
            DatePickerDialog dpd = new DatePickerDialog(RegistraVisita.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int año, int mes, int dia) {
                    int mes_mamalon = mes + 1;
                    String mesAux = "" + mes_mamalon;
                    String mesString = (mesAux.length() == 1) ? "0" + mesAux : mesAux;
                    String dia_aux = "" + dia;
                    String dia_string = (dia_aux.length() == 1) ? "0" + dia_aux : dia_aux;
                    fechaProximaVisita = año + "-" + mesString + "-" + dia_string;
                    String fecha_proxima_visita = dia + " de " + getNombreMes(mes_mamalon) + " del " + año;
                    txtFechaProxima.setText(fecha_proxima_visita);
                    btnFechaProxima.setVisibility(View.GONE);
                    txtFechaProxima.setVisibility(View.VISIBLE);
                }
            }, año, mes, dia);
            dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 2000);
            dpd.show();
        }
    }

    public String getNombreMes(int mes) {
        String respuesta = "";
        switch (mes) {
            case 1:
                respuesta = "Enero";
                break;
            case 2:
                respuesta = "Febrero";
                break;
            case 3:
                respuesta = "Marzo";
                break;
            case 4:
                respuesta = "Abril";
                break;
            case 5:
                respuesta = "Mayo";
                break;
            case 6:
                respuesta = "Junio";
                break;
            case 7:
                respuesta = "Julio";
                break;
            case 8:
                respuesta = "Agosto";
                break;
            case 9:
                respuesta = "Septiembre";
                break;
            case 10:
                respuesta = "Octubre";
                break;
            case 11:
                respuesta = "Noviembre";
                break;
            case 12:
                respuesta = "Diciembre";
                break;
        }
        return respuesta;
    }

    @Override
    public void onBackPressed() {
        finActivity();
    }

    public void finActivity() {
        dbHandler = new RanchoDB(RegistraVisita.this, DATABASE_NAME, null, DATABASE_VERSION);
        dbHandler.checkDBStatus();
        usuario = dbHandler.ultimoUsuarioRegistrado();
        finish();
        Intent explicit_intent;
        explicit_intent = new Intent(RegistraVisita.this, NavDrawerActivity.class);
        explicit_intent.putExtra("usuario", usuario);
        explicit_intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        explicit_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(explicit_intent);
        overridePendingTransition(R.anim.lista_deabajo_parriba, R.anim.fab_close);
        return;
    }

    public void cargaFotosTomadas() {
        layPhotos.removeAllViews();
        tblFotos.removeAllViews();
        idImg = 20000;
        idRowPhoto = 21000;
        idNamePhoto = 22000;
        idBtnFoto = 23000;
        Log.e("TAMAÑO", "El tamaño de fotosTomadas es "+ fotosTomadas.size());
        if(fotosTomadas.size() > 0){
            for (FotoVisita f : fotosTomadas) {
                idImg++;
                ImageView img = new ImageView(RegistraVisita.this);
                img.setLayoutParams(new LinearLayout.LayoutParams(tamanoEnDP(100), tamanoEnDP(100)));
                byte[] base64Imagen = Base64.decode(f.getBase64Img(), Base64.DEFAULT);
                Bitmap bitmapMamalon = BitmapFactory.decodeByteArray(base64Imagen, 0, base64Imagen.length);
                img.setImageBitmap(bitmapMamalon);
                img.setId(idImg);
                img.setPadding(tamanoEnDP(5),0, tamanoEnDP(5),0);
                img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        muestraFoto(f.getIdImagen(), bitmapMamalon);
                    }
                });
                layPhotos.addView(img);

                //Table Row que muestra el nombre de las fotos
                idRowPhoto++;
                TableRow tr = new TableRow(RegistraVisita.this);
                tr.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));

                Button deletePartida = new Button(RegistraVisita.this);
                deletePartida.setBackgroundResource(basurax);
                float scale = RegistraVisita.this.getResources().getDisplayMetrics().density;
                int pixels = (int) (40 * scale + 0.5f);
                deletePartida.setLayoutParams(new TableRow.LayoutParams(pixels, pixels));
                deletePartida.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        eliminaPhoto(f.getIdImagen(), f.getBase64Img());
                    }
                });
                deletePartida.setPadding(15, 15, 15, 15);
                tr.addView(deletePartida);

                TextView nameFoto = new TextView(RegistraVisita.this);
                nameFoto.setText("Foto " + f.getIdImagen());
                nameFoto.setTextColor(Color.BLACK);
                nameFoto.setPadding(5, 5, 5, 5);
                nameFoto.setTypeface(classico);
                nameFoto.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
                tr.addView(nameFoto);

                tblFotos.addView(tr);
            }
        }
    }

    public void eliminaPhoto(int idFoto, String base64Img){
        Log.e("eliminaPhoto", "El tamaño de fotosTomadas es "+ fotosTomadas.size());
        DialogsBiochem.muestraDialogoWarningCustom(RegistraVisita.this, "Está seguro de eliminar la foto " + idFoto, new DialogsBiochem.CustomAccion() {
            @Override
            public void customAccion() {
                fotoAux = new ArrayList<>();
                fotoAux.add(new FotoVisita(idFoto, base64Img));
                fotosTomadas.removeAll(fotoAux);
                Log.e("eliminaPhoto", "2nd step ==> El tamaño de fotosTomadas es "+ fotosTomadas.size());
                cargaFotosTomadas();
            }
        },2);
    }

    public int tamanoEnDP(int dp){

        int dps = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                getResources().getDimension(R.dimen.unidad),
                getResources().getDisplayMetrics()
        );
        int tamanoFinal = dp * dps;
        return tamanoFinal;
    }

    public void muestraFoto(int idFoto, Bitmap bitmap){
        //Cuando se de clic en la imagen
        pd2 = new AlertDialog.Builder(RegistraVisita.this);
        pd2.setCancelable(false);
        //LayoutInflater inflater = (LayoutInflater) CapturaPedidosFin.this.getSystemService( CapturaPedidosFin.this.LAYOUT_INFLATER_SERVICE );
        View view = getLayoutInflater().inflate(R.layout.dialog_img_visita, null);
        ImageView cierraDialog = (ImageView)view.findViewById(R.id.cierraDialog);
        ImageView imgVisita = (ImageView)view.findViewById(R.id.imgVisita);
        TextView txtFoto = (TextView)view.findViewById(R.id.txtFoto);

        txtFoto.setText("Foto " + idFoto);

        imgVisita.setImageBitmap(bitmap);

        cierraDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        pd2.setView(view);
        dialog = pd2.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //Comprobamos que la foto se a realizado
            if (resultCode == RESULT_OK) {
                //Creamos un bitmap con la imagen recientemente
                //almacenada en la memoria
                switch (requestCode) {
                    case RESULT_IFE:
                        idTaked++;
                        Bitmap bMap = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + "/Biochem_Files/rancho.jpg");
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        bMap.compress(Bitmap.CompressFormat.JPEG, 80, bos);
                        Bitmap useThisBitmap = Bitmap.createScaledBitmap(bMap,
                                (int) (bMap.getWidth() * 0.3), (int) (bMap.getHeight() * 0.3), true);
                        //photoTaked.setImageBitmap(useThisBitmap);
                        bos = new ByteArrayOutputStream();
                        useThisBitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);
                        bArray_IFE = Base64.encodeToString(bos.toByteArray(), Base64.DEFAULT);
                        fotosTomadas.add(new FotoVisita(idTaked, bArray_IFE));
                        cargaFotosTomadas();
                        break;
                }
                if (flag_chingona >= 1) {
                    //   btnSiguiente.setVisibility(View.VISIBLE);
                }
            } else {
                /*
                takePhoto.setVisibility(View.VISIBLE);
                photoTaked.setVisibility(View.GONE);
                */
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
            Log.d("###RESULT CODE", "==>" + resultCode);
            try {
                if (resultCode == RESULT_OK) {
                    switch (requestCode) {
                        case RESULT_IFE:
                            idTaked++;
                            File imagesFolder = new File(
                                    Environment.getExternalStorageDirectory(), "Biochem_Files");
                            imagesFolder.mkdirs();
                            //añadimos el nombre de la imagen
                            File image = new File(imagesFolder, "ife.jpg");
                            Uri uriSavedImage = Uri.fromFile(image);
                            getContentResolver().notifyChange(uriSavedImage, null);
                            ContentResolver cr = getContentResolver();
                            try {
                                Bitmap mPhoto = MediaStore.Images.Media
                                        .getBitmap(cr, uriSavedImage);

                                Bitmap useThisBitmap = Bitmap.createScaledBitmap(mPhoto,
                                        (int) (mPhoto.getWidth() * 0.3), (int) (mPhoto.getHeight() * 0.3), true);
                                //Bitmap fotoFinal
                                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                                useThisBitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);
                                bArray_IFE = Base64.encodeToString(bos.toByteArray(), Base64.DEFAULT);
                                fotosTomadas.add(new FotoVisita(idTaked, bArray_IFE));
                                cargaFotosTomadas();
                                //photoTaked.setImageBitmap(useThisBitmap);

                            } catch (Exception e) {
                                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT)
                                        .show();
                            }
                            break;
                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public class EnviaVisitas extends AsyncTask<String, String, String> {
        // Bloque para poder usar los metodos de la clase MyDBHandler en la clase JsonTask
        private RanchoDB dbHandler;

        public EnviaVisitas(RanchoDB dbHandler) {
            this.dbHandler = dbHandler;
        }
        // Termina bloque de inclusion de los metodos de MyDBHandler (Se uso esta practica debido a que ya esta usado extends en la clase

        protected void onPreExecute() {
            super.onPreExecute();

            pd2 = new AlertDialog.Builder(RegistraVisita.this);
            pd2.setCancelable(false);
            //LayoutInflater inflater = (LayoutInflater) CapturaPedidosFin.this.getSystemService( CapturaPedidosFin.this.LAYOUT_INFLATER_SERVICE );
            View view = getLayoutInflater().inflate(R.layout.dialog_transparente, null);

            TextView tvClasif = (TextView) view.findViewById(R.id.txt);
            tvClasif.setText("Enviando datos al servidor, espere...");
            ImageView imgEncabezado = (ImageView)view.findViewById(R.id.imgGif);
            Glide.with(RegistraVisita.this)
                    .load(R.drawable.broilanimated)
                    .into(imgEncabezado);

            pd2.setView(view);
            dialog = pd2.create();
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }

        protected String doInBackground(String... params) {

            /*************************************************************
             * Bloque para envio de los pedidos :                        *
             * ***********************************************************/
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            String respuesta_servidor = "";
            String urlSendPedidito = ((GlobalsBioshem)RegistraVisita.this.getApplication()).getUrlSendVisitas() + URLEncoder.encode(dbHandler.transmiteVisitas());// + InfoEnviar.enviaInfo();

            try {

                URL url = new URL(urlSendPedidito);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                respuesta_servidor = buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return respuesta_servidor;
        } // FIn del doInBackground

        @Override
        protected void onPostExecute(String respuesta_servidor) {
            super.onPostExecute(respuesta_servidor);

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            String codigo_respuesta = "";
            String mensaje_respuesta = "";
            String errs = "", success = "";
            try {
                JSONArray respuesta = new JSONArray(respuesta_servidor);
                for(int i = 0; i < respuesta.length(); i++){
                    JSONObject obj = respuesta.getJSONObject(i);
                    codigo_respuesta = obj.getString("codigo_respuesta");
                    mensaje_respuesta = obj.getString("mensaje_respuesta");
                    errs = obj.getString("errores");
                    success = obj.getString("success");
                }
                if(codigo_respuesta.equals("201")){
                    JSONArray errores_visitas = new JSONArray(errs);
                    String msje_malos = "";
                    for(int i = 0; i < errores_visitas.length(); i++){
                        JSONObject wrong = errores_visitas.getJSONObject(i);
                        String id_visita_movil = wrong.getString("id_visita_movil");
                        String id_rancho = wrong.getString("id_rancho");
                        String ransho = dbHandler.dameNombreRanchoFinal(id_rancho);
                        msje_malos += "*La visita al rancho " + ransho + " no se pudo sincronizar con el servidor, el error fue: "+wrong.getString("err")+".\n\n";
                    }
                    JSONArray success_visitas = new JSONArray(success);
                    if(success_visitas.length() > 0){
                        for(int i = 0; i < success_visitas.length(); i++){
                            JSONObject good = success_visitas.getJSONObject(i);
                            String id_visita = good.getString("id_visita");
                            String id_visita_movil = good.getString("id_visita_movil");
                            dbHandler.visitaSincronizadaFinal(id_visita, id_visita_movil);

                            boolean existeMuestra = dbHandler.checkInformacionTabla(dbHandler.TABLE_VNT_RANCHO_MUESTRAS, dbHandler.COL_VNTRANCHOMUESTRAS_ID,
                                    dbHandler.COL_VNTRANCHOMUESTRAS_ID_VISITA_MOVIL, id_visita_movil);
                            if(existeMuestra == true){
                                dbHandler.muestraSincronizadaFromVisitas(id_visita, id_visita_movil);
                            }

                            boolean existeFoto = dbHandler.checkInformacionTabla(dbHandler.TABLE_VNT_FOTOS_VISITA, dbHandler.COL_VNTFOTOSVISITA_ID,
                                    dbHandler.COL_VNTFOTOSVISITA_ID_VISITA_MOVIL, id_visita_movil);
                            if(existeFoto == true){
                                dbHandler.fotoSincronizadaFromVisitas(id_visita, id_visita_movil);
                            }
                        }
                    }
                    mensaje_respuesta += "\n"+msje_malos;
                    DialogsBiochem.muestraDialogoWarningCustom(RegistraVisita.this, mensaje_respuesta, new DialogsBiochem.CustomAccion() {
                        @Override
                        public void customAccion() {
                            if(success_visitas.length() > 0){
                                new EnviaMuestras(dbHandler).execute();
                            }else{
                                finActivity();
                            }
                        }
                    }, 1);
                }
                if(codigo_respuesta.equals("200")){
                    JSONArray success_ranchos = new JSONArray(success);
                    if(success_ranchos.length() > 0){
                        for(int i = 0; i < success_ranchos.length(); i++){
                            JSONObject good = success_ranchos.getJSONObject(i);
                            String id_visita = good.getString("id_visita");
                            String id_visita_movil = good.getString("id_visita_movil");
                            dbHandler.visitaSincronizadaFinal(id_visita, id_visita_movil);

                            boolean existeVisita = dbHandler.checkInformacionTabla(dbHandler.TABLE_VNT_RANCHO_MUESTRAS, dbHandler.COL_VNTRANCHOMUESTRAS_ID,
                                    dbHandler.COL_VNTRANCHOMUESTRAS_ID_VISITA_MOVIL, id_visita_movil);
                            if(existeVisita == true){
                                dbHandler.muestraSincronizadaFromVisitas(id_visita, id_visita_movil);
                            }
                            boolean existeFoto = dbHandler.checkInformacionTabla(dbHandler.TABLE_VNT_FOTOS_VISITA, dbHandler.COL_VNTFOTOSVISITA_ID,
                                    dbHandler.COL_VNTFOTOSVISITA_ID_VISITA_MOVIL, id_visita_movil);
                            if(existeFoto == true){
                                dbHandler.fotoSincronizadaFromVisitas(id_visita, id_visita_movil);
                            }
                        }
                    }
                    new EnviaMuestras(dbHandler).execute();
                }else{
                    DialogsBiochem.muestraDialogoErrorCustom(RegistraVisita.this, "CODIGO DE ERROR: " + codigo_respuesta + ".\n" + mensaje_respuesta, RegistraVisita.this::customAccion);
                }
            } catch (JSONException e) {
                DialogsBiochem.muestraDialogoErrorCustom(RegistraVisita.this, "Ocurrio un error al convertir la respuesta del servidor:"+respuesta_servidor+"\n" + e, RegistraVisita.this::customAccion);
            }
        } // Fin del onPostExecute
    }// //_-------------------------------------------------- fin de envia visitas

    public class EnviaMuestras extends AsyncTask<String, String, String> {
        // Bloque para poder usar los metodos de la clase MyDBHandler en la clase JsonTask
        private RanchoDB dbHandler;

        public EnviaMuestras(RanchoDB dbHandler) {
            this.dbHandler = dbHandler;
        }
        // Termina bloque de inclusion de los metodos de MyDBHandler (Se uso esta practica debido a que ya esta usado extends en la clase

        protected void onPreExecute() {
            super.onPreExecute();

            pd2 = new AlertDialog.Builder(RegistraVisita.this);
            pd2.setCancelable(false);
            //LayoutInflater inflater = (LayoutInflater) CapturaPedidosFin.this.getSystemService( CapturaPedidosFin.this.LAYOUT_INFLATER_SERVICE );
            View view = getLayoutInflater().inflate(R.layout.dialog_transparente, null);

            TextView tvClasif = (TextView) view.findViewById(R.id.txt);
            tvClasif.setText("Enviando datos al servidor, espere...");
            ImageView imgEncabezado = (ImageView)view.findViewById(R.id.imgGif);
            Glide.with(RegistraVisita.this)
                    .load(R.drawable.broilanimated)
                    .into(imgEncabezado);

            pd2.setView(view);
            dialog = pd2.create();
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }

        protected String doInBackground(String... params) {

            /*************************************************************
             * Bloque para envio de los pedidos :                        *
             * ***********************************************************/
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            String respuesta_servidor = "";
            String urlSendPedidito = ((GlobalsBioshem)RegistraVisita.this.getApplication()).getUrlSendMuestras() + URLEncoder.encode(dbHandler.transmiteMuestras());// + InfoEnviar.enviaInfo();

            try {

                URL url = new URL(urlSendPedidito);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                respuesta_servidor = buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return respuesta_servidor;
        } // FIn del doInBackground

        @Override
        protected void onPostExecute(String respuesta_servidor) {
            super.onPostExecute(respuesta_servidor);

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            String codigo_respuesta = "";
            String mensaje_respuesta = "";
            String errs = "", success = "";
            try {
                JSONArray respuesta = new JSONArray(respuesta_servidor);
                for(int i = 0; i < respuesta.length(); i++){
                    JSONObject obj = respuesta.getJSONObject(i);
                    codigo_respuesta = obj.getString("codigo_respuesta");
                    mensaje_respuesta = obj.getString("mensaje_respuesta");
                    errs = obj.getString("errores");
                    success = obj.getString("success");
                }
                if(codigo_respuesta.equals("201")){
                    JSONArray errores_muestras = new JSONArray(errs);
                    String msje_malos = "";
                    for(int i = 0; i < errores_muestras.length(); i++){
                        JSONObject wrong = errores_muestras.getJSONObject(i);
                        String id_muestra_movil = wrong.getString("id_muestra_movil");
                        String id_visita = wrong.getString("id_visita");
                        String ransho = dbHandler.dameRanchoFromVisita(id_visita);
                        msje_malos += "*Una muestra para el rancho " + ransho + " no se pudo sincronizar con el servidor, el error fue: "+wrong.getString("err")+".\n\n";
                    }
                    JSONArray success_muestras = new JSONArray(success);
                    if(success_muestras.length() > 0){
                        for(int i = 0; i < success_muestras.length(); i++){
                            JSONObject good = success_muestras.getJSONObject(i);
                            String id_muestra = good.getString("id_muestra");
                            String id_muestra_movil = good.getString("id_muestra_movil");
                            dbHandler.muestraSincronizadaFinal(id_muestra, id_muestra_movil);
                        }
                    }
                    mensaje_respuesta += "\n"+msje_malos;
                    DialogsBiochem.muestraDialogoWarningCustom(RegistraVisita.this, mensaje_respuesta, new DialogsBiochem.CustomAccion() {
                        @Override
                        public void customAccion() {
                            if(success_muestras.length() > 0){
                                new EnviaFotos(dbHandler).execute();
                            }else{
                                finActivity();
                            }
                        }
                    }, 1);
                }
                if(codigo_respuesta.equals("200")){
                    JSONArray success_ranchos = new JSONArray(success);
                    if(success_ranchos.length() > 0){
                        for(int i = 0; i < success_ranchos.length(); i++){
                            JSONObject good = success_ranchos.getJSONObject(i);
                            String id_muestra = good.getString("id_muestra");
                            String id_muestra_movil = good.getString("id_muestra_movil");
                            dbHandler.muestraSincronizadaFinal(id_muestra, id_muestra_movil);
                        }
                    }
                    new EnviaFotos(dbHandler).execute();
                }else{
                    DialogsBiochem.muestraDialogoErrorCustom(RegistraVisita.this, "CODIGO DE ERROR: " + codigo_respuesta + ".\n" + mensaje_respuesta, RegistraVisita.this::customAccion);
                }
            } catch (JSONException e) {
                DialogsBiochem.muestraDialogoErrorCustom(RegistraVisita.this, "Ocurrio un error al convertir la respuesta del servidor:"+respuesta_servidor+"\n" + e, RegistraVisita.this::customAccion);
            }
        } // Fin del onPostExecute
    }// Fin de envia muestras

    public class EnviaFotos extends AsyncTask<String, String, String> {
        // Bloque para poder usar los metodos de la clase MyDBHandler en la clase JsonTask
        private RanchoDB dbHandler;

        public EnviaFotos(RanchoDB dbHandler) {
            this.dbHandler = dbHandler;
        }
        // Termina bloque de inclusion de los metodos de MyDBHandler (Se uso esta practica debido a que ya esta usado extends en la clase

        protected void onPreExecute() {
            super.onPreExecute();

            pd2 = new AlertDialog.Builder(RegistraVisita.this);
            pd2.setCancelable(false);
            //LayoutInflater inflater = (LayoutInflater) CapturaPedidosFin.this.getSystemService( CapturaPedidosFin.this.LAYOUT_INFLATER_SERVICE );
            View view = getLayoutInflater().inflate(R.layout.dialog_transparente, null);

            TextView tvClasif = (TextView) view.findViewById(R.id.txt);
            tvClasif.setText("Enviando datos al servidor, espere...");
            ImageView imgEncabezado = (ImageView)view.findViewById(R.id.imgGif);
            Glide.with(RegistraVisita.this)
                    .load(R.drawable.broilanimated)
                    .into(imgEncabezado);

            pd2.setView(view);
            dialog = pd2.create();
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }

        protected String doInBackground(String... params) {

            /*************************************************************
             * Bloque para envio de los pedidos :                        *
             * ***********************************************************/
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            String respuesta_servidor = "";
            String urlSendPedidito = ((GlobalsBioshem)RegistraVisita.this.getApplication()).getUrlSendFotos() + URLEncoder.encode(dbHandler.transmiteFotos());// + InfoEnviar.enviaInfo();

            try {

                URL url = new URL(urlSendPedidito);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                respuesta_servidor = buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return respuesta_servidor;
        } // FIn del doInBackground

        @Override
        protected void onPostExecute(String respuesta_servidor) {
            super.onPostExecute(respuesta_servidor);

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            String codigo_respuesta = "";
            String mensaje_respuesta = "";
            String errs = "", success = "";
            try {
                JSONArray respuesta = new JSONArray(respuesta_servidor);
                for(int i = 0; i < respuesta.length(); i++){
                    JSONObject obj = respuesta.getJSONObject(i);
                    codigo_respuesta = obj.getString("codigo_respuesta");
                    mensaje_respuesta = obj.getString("mensaje_respuesta");
                    errs = obj.getString("errores");
                    success = obj.getString("success");
                }
                if(codigo_respuesta.equals("201")){
                    JSONArray errores_fotos = new JSONArray(errs);
                    String msje_malos = "";
                    for(int i = 0; i < errores_fotos.length(); i++){
                        JSONObject wrong = errores_fotos.getJSONObject(i);
                        String id_foto_movil = wrong.getString("id_foto_movil");
                        String id_visita = wrong.getString("id_visita");
                        String ransho = dbHandler.dameRanchoFromVisita(id_visita);
                        msje_malos += "*Una foto de la visita al rancho " + ransho + " no se pudo sincronizar con el servidor, el error fue: "+wrong.getString("err")+".\n\n";
                    }
                    JSONArray success_fotos = new JSONArray(success);
                    if(success_fotos.length() > 0){
                        for(int i = 0; i < success_fotos.length(); i++){
                            JSONObject good = success_fotos.getJSONObject(i);
                            String id_foto = good.getString("id_foto");
                            String id_foto_movil = good.getString("id_foto_movil");
                            dbHandler.fotoSincronizadaFinal(id_foto, id_foto_movil);
                        }
                    }
                    mensaje_respuesta += "\n"+msje_malos;
                    DialogsBiochem.muestraDialogoWarningCustom(RegistraVisita.this, mensaje_respuesta, RegistraVisita.this::customAccion,1);
                }
                if(codigo_respuesta.equals("200")){
                    JSONArray success_ranchos = new JSONArray(success);
                    if(success_ranchos.length() > 0){
                        for(int i = 0; i < success_ranchos.length(); i++){
                            JSONObject good = success_ranchos.getJSONObject(i);
                            String id_foto = good.getString("id_foto");
                            String id_foto_movil = good.getString("id_foto_movil");
                            dbHandler.fotoSincronizadaFinal(id_foto, id_foto_movil);
                        }
                    }
                    DialogsBiochem.muestraDialogoSuccessCustom(RegistraVisita.this, mensaje_respuesta, RegistraVisita.this::customAccion);
                }else{
                    DialogsBiochem.muestraDialogoErrorCustom(RegistraVisita.this, "CODIGO DE ERROR: " + codigo_respuesta + ".\n" + mensaje_respuesta, RegistraVisita.this::customAccion);
                }
            } catch (JSONException e) {
                DialogsBiochem.muestraDialogoErrorCustom(RegistraVisita.this, "Ocurrio un error al convertir la respuesta del servidor:"+respuesta_servidor+"\n" + e, RegistraVisita.this::customAccion);
            }
        } // Fin del onPostExecute
    }// Fin de envia muestras
}
