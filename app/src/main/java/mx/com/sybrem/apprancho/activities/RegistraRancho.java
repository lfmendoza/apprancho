package mx.com.sybrem.apprancho.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import mx.com.sybrem.apprancho.R;
import mx.com.sybrem.apprancho.clases.RanchoDB;
import mx.com.sybrem.apprancho.clases.general.CheckNetwork;
import mx.com.sybrem.apprancho.clases.general.DialogsBiochem;
import mx.com.sybrem.apprancho.clases.general.GlobalsBioshem;
import mx.com.sybrem.apprancho.clases.general.ToastBiochem;

public class RegistraRancho extends AppCompatActivity implements DialogsBiochem.CustomAccion {

    EditText txtPoblacion, txtMunicipio, txtCodigoPostal, txtEstado, txtCliente, txtRancho, txtTelefono, txtEmail, txtReferencias;
    Button btnCancela, btnGuarda, btnLoca;
    RanchoDB dbHandler;

    String usuario = "";

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "ranchoTablet.db";

    String poblacion = "", municipio = "", cp = "", estado = "", cliente = "", rancho = "" , telefono = "", email = "", referencias = "";
    String ruta = "";

    AlertDialog.Builder pd2;
    AlertDialog dialog;

    LocationManager locationManager;
    double longitudeGPS = 0, latitudeGPS = 0;
    String ubicacion = "0.0 0.0";
    TextView tvUbicacion;
    LinearLayout layoutLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registra_rancho);

        dbHandler = new RanchoDB(RegistraRancho.this, DATABASE_NAME, null, DATABASE_VERSION);
        dbHandler.checkDBStatus();

        usuario = dbHandler.ultimoUsuarioRegistrado();

        String TipoUsuario = dbHandler.tipoUsuario(usuario);


        if (TipoUsuario.equals("A")) {
            ruta = dbHandler.num_ruta(usuario);
        } else {
            ruta = dbHandler.rutaSeleccionada();
        }

        txtPoblacion = (EditText)findViewById(R.id.txtPoblacion);
        txtMunicipio = (EditText)findViewById(R.id.txtMunicipio);
        txtCodigoPostal = (EditText)findViewById(R.id.txtCodigoPostal);
        txtEstado = (EditText)findViewById(R.id.txtEstado);
        txtCliente = (EditText)findViewById(R.id.txtCliente);
        txtRancho = (EditText)findViewById(R.id.txtRancho);
        txtTelefono = (EditText)findViewById(R.id.txtTelefono);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        txtReferencias = (EditText)findViewById(R.id.txtReferencias);

        layoutLocation = (LinearLayout) findViewById(R.id.layoutLocation);
        tvUbicacion = (TextView) findViewById(R.id.tvUbicacion);
        btnLoca = (Button) findViewById(R.id.btnLoca);
        btnLoca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleGPSUpdates();
            }
        });

        btnCancela = (Button)findViewById(R.id.btnCancela);
        btnGuarda = (Button)findViewById(R.id.btnGuarda);

        btnCancela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finActivity();
            }
        });

        btnGuarda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int bandera_errores = 0;
                String msje = "";
                poblacion = txtPoblacion.getText().toString();
                municipio = txtMunicipio.getText().toString();
                cp = txtCodigoPostal.getText().toString();
                estado = txtEstado.getText().toString();
                cliente = txtCliente.getText().toString();
                rancho = txtRancho.getText().toString();
                telefono = txtTelefono.getText().toString();
                email = txtEmail.getText().toString();
                referencias = txtReferencias.getText().toString();

                if(poblacion.length() < 3){
                    bandera_errores++;
                    msje += "* Debe escribir la población.\n";
                }
                if(municipio.length() < 3){
                    bandera_errores++;
                    msje += "* Debe escribir el municipio.\n";
                }
                if(cp.length() < 5){
                    bandera_errores++;
                    msje += "* Debe escribir el código postal.\n";
                }
                if(estado.length() < 5){
                    bandera_errores++;
                    msje += "* Debe escribir el estado.\n";
                }
                /*if(cliente.length() < 10){
                    bandera_errores++;
                    msje += "* Debe escribir el nombre del cliente.\n";
                }*/
                if(rancho.length() < 5){
                    bandera_errores++;
                    msje += "* Debe escribir el nombre del rancho.\n";
                }
                if(telefono.length() < 10){
                    bandera_errores++;
                    msje += "* Debe escribir el número de teléfono.\n";
                }
                if(email.length() < 6){
                    bandera_errores++;
                    msje += "* Debe escribir el email.\n";
                }
                if (referencias.length() < 10){
                    bandera_errores++;
                    msje += "* Debe escribir algunas referencias del rancho.\n";
                }

                if(bandera_errores > 0){
                    DialogsBiochem.muestraDialogoError(RegistraRancho.this, msje);
                }else{
                    //Se almacena en la base de datos local y se sincroniza
                    String cve_agente = "";
                    String tipoUsuario = dbHandler.tipoUsuario(usuario);
                    if (tipoUsuario.equals("A")) {
                        cve_agente = usuario;
                    } else {
                        cve_agente = dbHandler.agenteSeleccionado();
                    }
                    String siguienteRanchito = dbHandler.getSiguienteRancho();
                    String id_agentes_parametros = dbHandler.getIdAgentesParametros(cve_agente);
                    String[] ubiqueichon = ubicacion.split(" ");
                    String latitudex = ubiqueichon[0];
                    String longitudex = ubiqueichon[1];

                    ContentValues catRancho = new ContentValues();
                    catRancho.put(dbHandler.COL_VNTCATRANCHO_ID_RANCHO, "0");
                    catRancho.put(dbHandler.COL_VNTCATRANCHO_ID_RANCHO_MOVIL, siguienteRanchito+"-"+ruta+"-"+rancho);
                    catRancho.put(dbHandler.COL_VNTCATRANCHO_ID_CAT_RUTAS, ruta);
                    catRancho.put(dbHandler.COL_VNTCATRANCHO_ESTADO, estado);
                    catRancho.put(dbHandler.COL_VNTCATRANCHO_MUNICIPIO, municipio);
                    catRancho.put(dbHandler.COL_VNTCATRANCHO_POBLACION, poblacion);
                    catRancho.put(dbHandler.COL_VNTCATRANCHO_CODIGO_POSTAL, cp);
                    catRancho.put(dbHandler.COL_VNTCATRANCHO_NOMBRE_RANCHO, rancho);
                    catRancho.put(dbHandler.COL_VNTCATRANCHO_TELEFONO_CONTACTO, telefono);
                    catRancho.put(dbHandler.COL_VNTCATRANCHO_CORREO_ELECTRONICO, email);
                    catRancho.put(dbHandler.COL_VNTCATRANCHO_REFERENCIA, referencias);
                    catRancho.put(dbHandler.COL_VNTCATRANCHO_LATITUDE, latitudex);
                    catRancho.put(dbHandler.COL_VNTCATRANCHO_LONGITUDE, longitudex);
                    catRancho.put(dbHandler.COL_VNTCATRANCHO_SINCRONIZADO, "0");

                    boolean insertaRancho = dbHandler.insertaCatRancho(catRancho);
                    if(insertaRancho == false){
                        DialogsBiochem.muestraDialogoError(RegistraRancho.this,"Ocurrió un error al insertar el registro del rancho en la base de datos de este dispositivo, intentelo de nuevo más tarde.");
                    }else{
                        if(CheckNetwork.isInternetAvailable(RegistraRancho.this)){
                            new EnviaRanshos(dbHandler).execute();
                        }else{
                            DialogsBiochem.muestraDialogoSuccessCustom(RegistraRancho.this, "El registro del rancho se guardó correctamente en el dispositivo, conéctese a internet para poder almacenarlo en la base de datos de la empresa.", RegistraRancho.this::customAccion);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed(){
        finActivity();
    }

    public void finActivity() {
        dbHandler = new RanchoDB(RegistraRancho.this, DATABASE_NAME, null, DATABASE_VERSION);
        dbHandler.checkDBStatus();
        usuario = dbHandler.ultimoUsuarioRegistrado();
        finish();
        Intent explicit_intent;
        explicit_intent = new Intent(RegistraRancho.this, NavDrawerActivity.class);
        explicit_intent.putExtra("usuario", usuario);
        explicit_intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        explicit_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(explicit_intent);
        overridePendingTransition(R.anim.lista_deabajo_parriba, R.anim.fab_close);
        return;
    }

    public class EnviaRanshos extends AsyncTask<String, String, String> {
        // Bloque para poder usar los metodos de la clase MyDBHandler en la clase JsonTask
        private RanchoDB dbHandler;

        public EnviaRanshos(RanchoDB dbHandler) {
            this.dbHandler = dbHandler;
        }
        // Termina bloque de inclusion de los metodos de MyDBHandler (Se uso esta practica debido a que ya esta usado extends en la clase

        protected void onPreExecute() {
            super.onPreExecute();

            pd2 = new AlertDialog.Builder(RegistraRancho.this);
            pd2.setCancelable(false);
            //LayoutInflater inflater = (LayoutInflater) CapturaPedidosFin.this.getSystemService( CapturaPedidosFin.this.LAYOUT_INFLATER_SERVICE );
            View view = getLayoutInflater().inflate(R.layout.dialog_transparente, null);

            TextView tvClasif = (TextView) view.findViewById(R.id.txt);
            tvClasif.setText("Enviando datos al servidor, espere...");
            ImageView imgEncabezado = (ImageView)view.findViewById(R.id.imgGif);
            Glide.with(RegistraRancho.this)
                    .load(R.drawable.broilanimated)
                    .into(imgEncabezado);

            pd2.setView(view);
            dialog = pd2.create();
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }

        protected String doInBackground(String... params) {

            /*************************************************************
             * Bloque para envio de los pedidos :                        *
             * ***********************************************************/
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            String respuesta_servidor = "";
            String urlSendPedidito = ((GlobalsBioshem)RegistraRancho.this.getApplication()).getUrlSendRanchos() + URLEncoder.encode(dbHandler.transmiteRanshos());// + InfoEnviar.enviaInfo();

            try {

                URL url = new URL(urlSendPedidito);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                respuesta_servidor = buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return respuesta_servidor;
        } // FIn del doInBackground

        @Override
        protected void onPostExecute(String respuesta_servidor) {
            super.onPostExecute(respuesta_servidor);

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            String codigo_respuesta = "";
            String mensaje_respuesta = "";
            String errs = "", success = "";
            try {
                JSONArray respuesta = new JSONArray(respuesta_servidor);
                for(int i = 0; i < respuesta.length(); i++){
                    JSONObject obj = respuesta.getJSONObject(i);
                    codigo_respuesta = obj.getString("codigo_respuesta");
                    mensaje_respuesta = obj.getString("mensaje_respuesta");
                    errs = obj.getString("errores");
                    success = obj.getString("success");
                }
                if(codigo_respuesta.equals("201")){
                    JSONArray errores_ranchos = new JSONArray(errs);
                    String msje_malos = "";
                    for(int i = 0; i < errores_ranchos.length(); i++){
                        JSONObject wrong = errores_ranchos.getJSONObject(i);
                        String id_rancho_movil = wrong.getString("id_rancho_movil");
                        String ransho = dbHandler.dameNombreRancho(id_rancho_movil);
                        msje_malos += "*El rancho " + ransho + " no se pudo sincronizar con el servidor, el error fue: "+wrong.getString("err")+".\n\n";
                    }
                    JSONArray success_ranchos = new JSONArray(success);
                    if(success_ranchos.length() > 0){
                        for(int i = 0; i < success_ranchos.length(); i++){
                            JSONObject good = success_ranchos.getJSONObject(i);
                            String id_rancho = good.getString("id_rancho");
                            String id_rancho_movil = good.getString("id_rancho_movil");
                            dbHandler.ranchoSincronizado(id_rancho, id_rancho_movil);
                            boolean existeVisita = dbHandler.checkInformacionTabla(dbHandler.TABLE_VNT_RANCHO_VISITA, dbHandler.COL_VNTRANCHOVISITA_ID_VISITA_MOVIL,
                                    dbHandler.COL_VNTRANCHOVISITA_ID_RANCHO_MOVIL, id_rancho_movil);
                            if(existeVisita == true){
                                dbHandler.visitaSincronizada(id_rancho, id_rancho_movil);
                            }
                        }
                    }
                    mensaje_respuesta += "\n"+msje_malos;
                    DialogsBiochem.muestraDialogoWarningCustom(RegistraRancho.this, mensaje_respuesta, RegistraRancho.this::customAccion,1);
                }
                if(codigo_respuesta.equals("200")){
                    JSONArray success_ranchos = new JSONArray(success);
                    if(success_ranchos.length() > 0){
                        for(int i = 0; i < success_ranchos.length(); i++){
                            JSONObject good = success_ranchos.getJSONObject(i);
                            String id_rancho = good.getString("id_rancho");
                            String id_rancho_movil = good.getString("id_rancho_movil");
                            dbHandler.ranchoSincronizado(id_rancho, id_rancho_movil);
                            boolean existeVisita = dbHandler.checkInformacionTabla(dbHandler.TABLE_VNT_RANCHO_VISITA, dbHandler.COL_VNTRANCHOVISITA_ID_VISITA_MOVIL,
                                    dbHandler.COL_VNTRANCHOVISITA_ID_RANCHO_MOVIL, id_rancho_movil);
                            if(existeVisita == true){
                                dbHandler.visitaSincronizada(id_rancho, id_rancho_movil);
                            }
                        }
                    }
                    DialogsBiochem.muestraDialogoSuccessCustom(RegistraRancho.this, mensaje_respuesta, RegistraRancho.this::customAccion);
                }else{
                    DialogsBiochem.muestraDialogoErrorCustom(RegistraRancho.this, "CODIGO DE ERROR: " + codigo_respuesta + ".\n" + mensaje_respuesta, RegistraRancho.this::customAccion);
                }
            } catch (JSONException e) {
                DialogsBiochem.muestraDialogoErrorCustom(RegistraRancho.this, "Ocurrio un error al convertir la respuesta del servidor:"+respuesta_servidor+"\n" + e, RegistraRancho.this::customAccion);
            }
        } // Fin del onPostExecute
    } // Fin de la clase EnviaRanshos

    @Override
    public void customAccion() {
        finActivity();
    }

    /*###################################################################################################################################################################*/
    /*############################################################################################################*/
    /*#########################################                 ##################################################*/
    /*#########################################   UBICACION     ##################################################*/
    /*#########################################                 ##################################################*/
    /*############################################################################################################*/
    /*###################################################################################################################################################################*/
    /* Aqui empieza la Clase Localizacion */

    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        /*final androidx.appcompat.app.AlertDialog.Builder dialog = new androidx.appcompat.app.AlertDialog.Builder(this);
        dialog.setTitle("Activar GPS")
                .setMessage("Necesitas activar el GPS para poder continuar.")
                .setPositiveButton("Activar ubicación", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();*/
    }

    private boolean isLocationEnabled() {
        locationManager = (LocationManager) RegistraRancho.this.getSystemService(Context.LOCATION_SERVICE);
        if (!CheckNetwork.isInternetAvailable(RegistraRancho.this)) { //returns true if internet available
            if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER)) {
                return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } else {
                if (locationManager.getAllProviders().contains(LocationManager.PASSIVE_PROVIDER)) {
                    return locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
                } else {
                    return false;
                }
            }
        } else {
            if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            } else if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER)) {
                return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } else if (locationManager.getAllProviders().contains(LocationManager.PASSIVE_PROVIDER)) {
                return locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
            } else {
                return false;
            }
        }
    }

    public void toggleGPSUpdates() {
        if (!checkLocation())
            return;
        locationManager.removeUpdates(locationListenerGPS);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ToastBiochem.toastSimple(RegistraRancho.this, "El permiso para obetener la ubicación no fue concedido, favor de revisar.", 0);
        }
        locationManager = (LocationManager) RegistraRancho.this.getSystemService(Context.LOCATION_SERVICE);
        if (!CheckNetwork.isInternetAvailable(RegistraRancho.this)) { //returns true if internet available
            if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, locationListenerGPS);
            }
            if (locationManager.getAllProviders().contains(LocationManager.PASSIVE_PROVIDER) && ubicacion.equals("0.0 0.0")) {
                locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 1000, 10, locationListenerGPS);
            }
        } else {
            if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 10, locationListenerGPS);
                /*Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                updateWithNewLocation(location);*/
            }
            if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER) && ubicacion.equals("0.0 0.0")) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, locationListenerGPS);
            }
            if (locationManager.getAllProviders().contains(LocationManager.PASSIVE_PROVIDER) && ubicacion.equals("0.0 0.0")) {
                locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 1000, 10, locationListenerGPS);
            }
        }
    }

    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(android.location.Location location) {
            if (location != null) {
                longitudeGPS = location.getLongitude();
                latitudeGPS = location.getLatitude();
                ubicacion = "" + latitudeGPS + " " + longitudeGPS;
                tvUbicacion.setText(ubicacion);
                if (!ubicacion.equals("0.0 0.0")) {
                    layoutLocation.setVisibility(View.GONE);
                    ToastBiochem.toastSimple(RegistraRancho.this, "Localización correcta", 1);
                } else {
                    layoutLocation.setVisibility(View.VISIBLE);
                    ToastBiochem.toastSimple(RegistraRancho.this, "Localización incorrecta, falla del dispositivo", 0);
                }
            } else {
                ToastBiochem.toastSimple(RegistraRancho.this, "No se pudo obtener la ubicación, problema de hardware.", 0);
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
            //ToastBiochem.toastSimple(CapturaPedidosFin.this, "onStatusChanged:\n"+s+"\n"+i+"\n"+bundle, 1);
        }

        @Override
        public void onProviderEnabled(String s) {
            //ToastBiochem.toastSimple(CapturaPedidosFin.this, "onProviderEnabled:\n"+s, 1);
        }

        @Override
        public void onProviderDisabled(String s) {
            //ToastBiochem.toastSimple(CapturaPedidosFin.this, "onProviderDisabled:\n"+s, 1);
        }
    };
    /* Fin de la clase localizacion */
}
