package mx.com.sybrem.apprancho.clases.general;

import java.net.URLEncoder;

import mx.com.sybrem.apprancho.BuildConfig;

public class InfoEnviar {

    public static String enviaInfo(){
        String nombre_dispositivo = android.os.Build.MODEL;
        String brand = android.os.Build.BRAND;
        String OsVersion = android.os.Build.VERSION.RELEASE;
        String versionApp = BuildConfig.VERSION_NAME;
        int verchonCode = BuildConfig.VERSION_CODE;

        String url_get = "nombreDispositivo=" + URLEncoder.encode(nombre_dispositivo )+
                "&brand=" + URLEncoder.encode(brand) + "&osVersion=" + URLEncoder.encode(OsVersion) + "&versionApp=" + versionApp +
                "&versionCode=" + verchonCode;
        return url_get;
    }
}
