package mx.com.sybrem.apprancho.clases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteConstraintException;
import android.util.Log;

import androidx.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class RanchoDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "ranchoTablet.db";

    public static final String TABLE_GL_ACCESOS = "gl_accesos";
    public static final String TABLE_GL_SYNC = "gl_sync";
    public static final String TABLE_GL_BITACORA_ACCESOS = "gl_bitacora_accesos";
    public static final String TABLE_GL_RUTA_SELECCIONADA = "gl_ruta_seleccionada";
    public static final String TABLE_VN_CAT_RUTAS = "vn_cat_rutas";
    public static final String TABLE_VNT_CAT_RANCHO = "vnt_cat_rancho";
    public static final String TABLE_VNT_RANCHO_VISITA = "vnt_rancho_visita";
    public static final String TABLE_VNT_RANCHO_MUESTRAS = "vnt_rancho_muestras";
    public static final String TABLE_IN_CAT_PRODUCTOS = "in_cat_productos";
    public static final String TABLE_VNT_FOTOS_VISITA = "vnt_fotos_visita";

    public static final String COL_GLBITACORAACCESOS_ID = "_id";
    public static final String COL_GLBITACORAACCESOS_CVEUSUARIO = "cve_usuario";
    public static final String COL_GLBITACORAACCESOS_FECHAREGISTRO = "fecha_registro";

    // gl_accesos:
    public static final String COL_GLACCESOS_ID = "_id";
    public static final String COL_GLACCESOS_CVEUSUARIO = "cve_usuario";
    public static final String COL_GLACCESOS_PASSWORD = "password";
    public static final String COL_GLACCESOS_NOMBRE = "nombre";
    public static final String COL_GLACCESOS_TIPOUSUARIO = "tipo_usuario";
    public static final String COL_GLACCESOS_ESTATUS = "estatus";
    public static final String COL_GLACCESOS_ID_AGENTESPARAMETROS = "id_agentesparametros";

    public static final String COL_GLSYNC_ID = "_id";
    public static final String COL_GLSYNC_CVEUSUARIO = "cve_usuario";
    public static final String COL_GLSYNC_FECHASYNC = "fecha_sync";
    public static final String COL_GLSYNC_EXITOSYNC = "exito_sync";

    // vn_cat_rutas:
    public static final String COL_VNCATRUTAS_ID = "_id";
    public static final String COL_VNCATRUTAS_ID_CAT_RUTAS = "id_cat_rutas";
    public static final String COL_VNCATRUTAS_ID_COMPANIA = "id_compania";
    public static final String COL_VNCATRUTAS_ID_AGENTESPARAMETROS = "id_agentesparametros";
    public static final String COL_VNCATRUTAS_NOMBRERUTA = "nombre_ruta";
    public static final String COL_VNCATRUTAS_DIASVENCIMIENTOCARTERA = "dias_vencimiento_cartera";
    public static final String COL_VNCATRUTAS_CUOTAVENTA = "cuota_venta";
    public static final String COL_VNCATRUTAS_CUOTACOBRANZA = "cuota_cobranza";
    public static final String COL_VNCATRUTAS_CUOTAMANO = "cuota_mano";
    public static final String COL_VNCATRUTAS_CUOTAPEDIDOS = "cuota_pedidos";
    public static final String COL_VNCATRUTAS_DIASINVENTARIO = "dias_inventario";
    public static final String COL_VNCATRUTAS_ID_ALMACEN = "id_almacen";

    // Tabla gl_ruta_seleccionada ///////////////////////////////////////////////////////////////////
    public static final String COL_GLRUTASELECCIONADA_ID = "_id";
    public static final String COL_GLRUTASELECCIONADA_NUMRUTA = "num_ruta";
    public static final String COL_GLRUTASELECCIONADA_CVEAGENTE = "cve_agente";

    public static final String COL_VNTCATRANCHO_ID = "_id";
    public static final String COL_VNTCATRANCHO_ID_RANCHO = "id_rancho";
    public static final String COL_VNTCATRANCHO_ID_RANCHO_MOVIL = "id_rancho_movil";
    public static final String COL_VNTCATRANCHO_ID_CAT_RUTAS = "id_cat_rutas";
    public static final String COL_VNTCATRANCHO_ESTADO = "estado";
    public static final String COL_VNTCATRANCHO_MUNICIPIO = "municipio";
    public static final String COL_VNTCATRANCHO_POBLACION = "poblacion";
    public static final String COL_VNTCATRANCHO_CODIGO_POSTAL = "codigo_postal";
    public static final String COL_VNTCATRANCHO_NOMBRE_RANCHO = "nombre_rancho";
    public static final String COL_VNTCATRANCHO_TELEFONO_CONTACTO = "telefono_contacto";
    public static final String COL_VNTCATRANCHO_CORREO_ELECTRONICO = "correo_electronico";
    public static final String COL_VNTCATRANCHO_REFERENCIA = "referencia";
    public static final String COL_VNTCATRANCHO_LATITUDE = "latitude";
    public static final String COL_VNTCATRANCHO_LONGITUDE = "longitude";
    public static final String COL_VNTCATRANCHO_SINCRONIZADO = "sincronizado";

    public static final String COL_VNTRANCHOVISITA_ID = "_id";
    public static final String COL_VNTRANCHOVISITA_ID_VISITA = "id_visita";
    public static final String COL_VNTRANCHOVISITA_ID_VISITA_MOVIL = "id_visita_movil";
    public static final String COL_VNTRANCHOVISITA_ID_RANCHO = "id_rancho";
    public static final String COL_VNTRANCHOVISITA_ID_RANCHO_MOVIL = "id_rancho_movil";
    public static final String COL_VNTRANCHOVISITA_FECHA_VISITA = "fecha_visita";
    public static final String COL_VNTRANCHOVISITA_CULTIVO = "cultivo";
    public static final String COL_VNTRANCHOVISITA_EXTENSION_SIEMBRA = "extension_siembra";
    public static final String COL_VNTRANCHOVISITA_PROBLEMA = "problema";
    public static final String COL_VNTRANCHOVISITA_SOLUCION_BIOCHEM = "solucion_biochem";
    public static final String COL_VNTRANCHOVISITA_MUESTRAS = "muestras";
    public static final String COL_VNTRANCHOVISITA_PROXIMA_VISITA = "proxima_visita";
    public static final String COL_VNTRANCHOVISITA_OBSERVACIONES = "observaciones";
    public static final String COL_VNTRANCHOVISITA_SINCRONIZADO = "sincronizado";

    public static final String COL_VNTRANCHOMUESTRAS_ID = "_id";
    public static final String COL_VNTRANCHOMUESTRAS_ID_MUESTRA = "id_muestra";
    public static final String COL_VNTRANCHOMUESTRAS_ID_MUESTRA_MOVIL = "id_muestra_movil";
    public static final String COL_VNTRANCHOMUESTRAS_ID_VISITA = "id_visita";
    public static final String COL_VNTRANCHOMUESTRAS_ID_VISITA_MOVIL = "id_visita_movil";
    public static final String COL_VNTRANCHOMUESTRAS_CVE_CAT_PRODUCTO = "cve_cat_producto";
    public static final String COL_VNTRANCHOMUESTRAS_CANTIDAD = "cantidad";
    public static final String COL_VNTRANCHOMUESTRAS_SINCRONIZADO = "sincronizado";

    public static final String COL_VNTFOTOSVISITA_ID = "_id";
    public static final String COL_VNTFOTOSVISITA_ID_VISITA = "id_visita";
    public static final String COL_VNTFOTOSVISITA_ID_VISITA_MOVIL = "id_visita_movil";
    public static final String COL_VNTFOTOSVISITA_ID_FOTO = "id_foto";
    public static final String COL_VNTFOTOSVISITA_ID_FOTO_MOVIL = "id_foto_movil";
    public static final String COL_VNTFOTOSVISITA_FOTO = "foto";
    public static final String COL_VNTFOTOSVISITA_SINCRONIZADO = "sincronizado";

    // in_cat_productos:
    public static final String COL_INCATPRODUCTOS_ID = "_id";
    public static final String COL_INCATPRODUCTOS_CVECATPRODUCTO = "cve_cat_producto";
    public static final String COL_INCATPRODUCTOS_ID_COMPANIA = "id_compania";
    public static final String COL_INCATPRODUCTOS_CVEPRODUCTO = "cve_producto";
    public static final String COL_INCATPRODUCTOS_NOMPRODUCTO = "nom_producto";
    public static final String COL_INCATPRODUCTOS_CVE_CLASIFICACION = "cve_clasificacion";
    public static final String COL_INCATPRODUCTOS_CVEUNIDADMEDIDA = "cve_unidad_medida";
    public static final String COL_INCATPRODUCTOS_PRECIOUNITARIO = "precio_unitario";
    public static final String COL_INCATPRODUCTOS_PIEZASPORPAQUETE = "piezas_por_paquete";
    public static final String COL_INCATPRODUCTOS_ESTATUS = "estatus";
    public static final String COL_INCATPRODUCTOS_COMENTARIOS = "comentarios";
    public static final String COL_INCATPRODUCTOS_IMAGEN = "imagen";
    public static final String COL_INCATPRODUCTOS_IEPS = "ieps";
    public static final String COL_INCATPRODUCTOS_ID_ORGANIZADOR = "id_organizador";
    public static final String COL_INCATPRODUCTOS_OFERTAS_INTELIGENTES = "ofertas_inteligentes";
    public static final String COL_INCATPRODUCTOS_BONIFICACIONES_INTELIGENTES = "bonificaciones_inteligentes";

    public RanchoDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // Bloque para la tabla local GL_SYNC
        String CREATE_GL_SYNC_TABLE = "CREATE TABLE  IF NOT EXISTS " + TABLE_GL_SYNC + "(" +
                COL_GLSYNC_ID + " INTEGER PRIMARY KEY, " +
                COL_GLSYNC_CVEUSUARIO + " TEXT, " +
                COL_GLSYNC_FECHASYNC + " TEXT, " +
                COL_GLSYNC_EXITOSYNC + " INTEGER " +
                ")";
        db.execSQL(CREATE_GL_SYNC_TABLE);
        // Bloque: TABLE_GL_ACCESOS = "gl_accesos"
        String CREATE_GL_ACCESOS_TABLE = "CREATE TABLE  IF NOT EXISTS " + TABLE_GL_ACCESOS + "(" +
                COL_GLACCESOS_ID + " INTEGER PRIMARY KEY, " +
                COL_GLACCESOS_CVEUSUARIO + " TEXT, " +
                COL_GLACCESOS_PASSWORD + " BLOB, " +
                COL_GLACCESOS_NOMBRE + " TEXT, " +
                COL_GLACCESOS_TIPOUSUARIO + " TEXT, " +
                COL_GLACCESOS_ESTATUS + " INTEGER, " +
                COL_GLACCESOS_ID_AGENTESPARAMETROS + " INTEGER " +
                ")";
        db.execSQL(CREATE_GL_ACCESOS_TABLE);

        // Bloque para la tabla local de vn_cat_rutas:
        String CREATE_VN_CAT_RUTAS_TABLE = "CREATE TABLE  IF NOT EXISTS " + TABLE_VN_CAT_RUTAS + "(" +
                COL_VNCATRUTAS_ID + " INTEGER PRIMARY KEY, " +
                COL_VNCATRUTAS_ID_CAT_RUTAS + " TEXT, " +
                COL_VNCATRUTAS_ID_COMPANIA + " TEXT, " +
                COL_VNCATRUTAS_ID_AGENTESPARAMETROS + " TEXT, " +
                COL_VNCATRUTAS_NOMBRERUTA + " TEXT, " +
                COL_VNCATRUTAS_DIASVENCIMIENTOCARTERA + " INTEGER, " +
                COL_VNCATRUTAS_CUOTAVENTA + " REAL, " +
                COL_VNCATRUTAS_CUOTACOBRANZA + " REAL, " +
                COL_VNCATRUTAS_CUOTAMANO + " REAL, " +
                COL_VNCATRUTAS_CUOTAPEDIDOS + " INTEGER, " +
                COL_VNCATRUTAS_DIASINVENTARIO + " INTEGER, " +
                COL_VNCATRUTAS_ID_ALMACEN + " INTEGER " +
                ")";
        db.execSQL(CREATE_VN_CAT_RUTAS_TABLE);

        String CREATE_GL_BITACORA_ACCESOS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_GL_BITACORA_ACCESOS + "(" +
                COL_GLBITACORAACCESOS_ID + " INTEGER PRIMARY KEY, " +
                COL_GLBITACORAACCESOS_CVEUSUARIO + " TEXT, " +
                COL_GLBITACORAACCESOS_FECHAREGISTRO + " TEXT " +
                ")";
        db.execSQL(CREATE_GL_BITACORA_ACCESOS_TABLE);

        // Tabla gl_ruta_seleccionada ///////////////////////////////////////////////////////////////////
        String CREATE_GL_RUTA_SELECCIONADA = "CREATE TABLE IF NOT EXISTS " + TABLE_GL_RUTA_SELECCIONADA + "(" +
                COL_GLRUTASELECCIONADA_ID + " INTEGER PRIMARY KEY, " +
                COL_GLRUTASELECCIONADA_NUMRUTA + " TEXT, " +
                COL_GLRUTASELECCIONADA_CVEAGENTE + " TEXT " +
                ")";
        db.execSQL(CREATE_GL_RUTA_SELECCIONADA);

        String CREATE_VNT_CAT_RANCHO = "CREATE TABLE IF NOT EXISTS " + TABLE_VNT_CAT_RANCHO + "(" +
                COL_VNTCATRANCHO_ID + " INTEGER PRIMARY KEY, " +
                COL_VNTCATRANCHO_ID_RANCHO + " TEXT, " +
                COL_VNTCATRANCHO_ID_RANCHO_MOVIL + " TEXT, " +
                COL_VNTCATRANCHO_ID_CAT_RUTAS + " TEXT, " +
                COL_VNTCATRANCHO_ESTADO + " TEXT, " +
                COL_VNTCATRANCHO_MUNICIPIO + " TEXT, " +
                COL_VNTCATRANCHO_POBLACION + " TEXT, " +
                COL_VNTCATRANCHO_CODIGO_POSTAL + " TEXT, " +
                COL_VNTCATRANCHO_NOMBRE_RANCHO + " TEXT, " +
                COL_VNTCATRANCHO_TELEFONO_CONTACTO + " TEXT, " +
                COL_VNTCATRANCHO_CORREO_ELECTRONICO + " TEXT, " +
                COL_VNTCATRANCHO_REFERENCIA + " TEXT, " +
                COL_VNTCATRANCHO_LATITUDE + " TEXT, " +
                COL_VNTCATRANCHO_LONGITUDE + " TEXT, " +
                COL_VNTCATRANCHO_SINCRONIZADO + " TEXT " +
                ")";
        db.execSQL(CREATE_VNT_CAT_RANCHO);

        String CREATE_VNT_RANCHO_VISITA = "CREATE TABLE IF NOT EXISTS " + TABLE_VNT_RANCHO_VISITA + "(" +
                COL_VNTRANCHOVISITA_ID + " INTEGER PRIMARY KEY, " +
                COL_VNTRANCHOVISITA_ID_VISITA + " TEXT, " +
                COL_VNTRANCHOVISITA_ID_VISITA_MOVIL + " TEXT, " +
                COL_VNTRANCHOVISITA_ID_RANCHO + " TEXT, " +
                COL_VNTRANCHOVISITA_ID_RANCHO_MOVIL + " TEXT, " +
                COL_VNTRANCHOVISITA_FECHA_VISITA + " TEXT, " +
                COL_VNTRANCHOVISITA_CULTIVO + " TEXT, " +
                COL_VNTRANCHOVISITA_EXTENSION_SIEMBRA + " TEXT, " +
                COL_VNTRANCHOVISITA_PROBLEMA + " TEXT, " +
                COL_VNTRANCHOVISITA_SOLUCION_BIOCHEM + " TEXT, " +
                COL_VNTRANCHOVISITA_MUESTRAS + " TEXT, " +
                COL_VNTRANCHOVISITA_PROXIMA_VISITA + " TEXT, " +
                COL_VNTRANCHOVISITA_OBSERVACIONES + " TEXT, " +
                COL_VNTRANCHOVISITA_SINCRONIZADO + " TEXT " +
                ")";
        db.execSQL(CREATE_VNT_RANCHO_VISITA);

        String CREATE_VNT_RANCHO_MUESTRAS = "CREATE TABLE IF NOT EXISTS " + TABLE_VNT_RANCHO_MUESTRAS + "(" +
                COL_VNTRANCHOMUESTRAS_ID + " INTEGER PRIMARY KEY, " +
                COL_VNTRANCHOMUESTRAS_ID_MUESTRA + " TEXT, " +
                COL_VNTRANCHOMUESTRAS_ID_MUESTRA_MOVIL + " TEXT, " +
                COL_VNTRANCHOMUESTRAS_ID_VISITA + " TEXT, " +
                COL_VNTRANCHOMUESTRAS_ID_VISITA_MOVIL + " TEXT, " +
                COL_VNTRANCHOMUESTRAS_CVE_CAT_PRODUCTO + " TEXT, " +
                COL_VNTRANCHOMUESTRAS_CANTIDAD + " TEXT, " +
                COL_VNTRANCHOMUESTRAS_SINCRONIZADO + " TEXT " +
                ")";
        db.execSQL(CREATE_VNT_RANCHO_MUESTRAS);


        // Bloque TABLE_IN_CAT_PRODUCTOS = "in_cat_productos"
        String CREATE_IN_CAT_PRODUCTOS_TABLE = "CREATE TABLE  IF NOT EXISTS " + TABLE_IN_CAT_PRODUCTOS + "(" +
                COL_INCATPRODUCTOS_ID + " INTEGER PRIMARY KEY, " +
                COL_INCATPRODUCTOS_CVECATPRODUCTO + " INTEGER, " +
                COL_INCATPRODUCTOS_ID_COMPANIA + " TEXT, " +
                COL_INCATPRODUCTOS_CVEPRODUCTO + " TEXT, " +
                COL_INCATPRODUCTOS_NOMPRODUCTO + " TEXT, " +
                COL_INCATPRODUCTOS_CVE_CLASIFICACION + " TEXT, " +
                COL_INCATPRODUCTOS_CVEUNIDADMEDIDA + " TEXT, " +
                COL_INCATPRODUCTOS_PRECIOUNITARIO + " REAL, " +
                COL_INCATPRODUCTOS_PIEZASPORPAQUETE + " INTEGER, " +
                COL_INCATPRODUCTOS_ESTATUS + " TEXT, " +
                COL_INCATPRODUCTOS_COMENTARIOS + " TEXT, " +
                COL_INCATPRODUCTOS_IMAGEN + " TEXT, " +
                COL_INCATPRODUCTOS_IEPS + " TEXT, " +
                COL_INCATPRODUCTOS_ID_ORGANIZADOR + " TEXT, " +
                COL_INCATPRODUCTOS_OFERTAS_INTELIGENTES + " TEXT, " +
                COL_INCATPRODUCTOS_BONIFICACIONES_INTELIGENTES + " TEXT " +
                ")";
        db.execSQL(CREATE_IN_CAT_PRODUCTOS_TABLE);

        String CREATE_VNT_FOTOS_VISITA_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_VNT_FOTOS_VISITA + "(" +
                COL_VNTFOTOSVISITA_ID + " INTEGER PRIMARY KEY, " +
                COL_VNTFOTOSVISITA_ID_VISITA + " TEXT, " +
                COL_VNTFOTOSVISITA_ID_VISITA_MOVIL + " TEXT, " +
                COL_VNTFOTOSVISITA_ID_FOTO + " TEXT, " +
                COL_VNTFOTOSVISITA_ID_FOTO_MOVIL + " TEXT, " +
                COL_VNTFOTOSVISITA_FOTO + " TEXT, " +
                COL_VNTFOTOSVISITA_SINCRONIZADO + " TEXT " +
                ")";
        db.execSQL(CREATE_VNT_FOTOS_VISITA_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GL_SYNC);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GL_ACCESOS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VN_CAT_RUTAS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GL_RUTA_SELECCIONADA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VNT_CAT_RANCHO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VNT_RANCHO_VISITA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IN_CAT_PRODUCTOS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VNT_FOTOS_VISITA);
        onCreate(db);
    }

    public void resetCatalogs() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM  " + TABLE_VNT_CAT_RANCHO + " WHERE " + COL_VNTCATRANCHO_SINCRONIZADO + " = '1'");
        db.execSQL("DELETE FROM  " + TABLE_VNT_FOTOS_VISITA + " WHERE " + COL_VNTCATRANCHO_SINCRONIZADO + " = '1'");
        db.execSQL("DELETE FROM  " + TABLE_VNT_RANCHO_VISITA);
        db.execSQL("DELETE FROM  " + TABLE_IN_CAT_PRODUCTOS);
        db.close();
    }

    public void insertaGlAccesos(JSONObject jsonObj) {
        ContentValues values = new ContentValues();
        try {
            values.put(COL_GLACCESOS_CVEUSUARIO, jsonObj.getString(COL_GLACCESOS_CVEUSUARIO));
            values.put(COL_GLACCESOS_PASSWORD, jsonObj.getString(COL_GLACCESOS_PASSWORD));
            values.put(COL_GLACCESOS_NOMBRE, jsonObj.getString(COL_GLACCESOS_NOMBRE));
            values.put(COL_GLACCESOS_TIPOUSUARIO, jsonObj.getString(COL_GLACCESOS_TIPOUSUARIO));
            values.put(COL_GLACCESOS_ESTATUS, jsonObj.getString(COL_GLACCESOS_ESTATUS));
            values.put(COL_GLACCESOS_ID_AGENTESPARAMETROS, jsonObj.getString(COL_GLACCESOS_ID_AGENTESPARAMETROS));

        } catch (JSONException e) {
            e.printStackTrace();
        } // fin del catch

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_GL_ACCESOS, null, values);
        db.close();

    } // FIN DE insertaGlAccesos

    // Inserta un registro individual en la tabla movil  vn_cat_rutas. Recibe un objeto de tipo JSON con estructura del catalogo de rutas
    public void insertaVnCatRutas(JSONObject jsonObj) {
        ContentValues values = new ContentValues();
        try {
            values.put(COL_VNCATRUTAS_ID_CAT_RUTAS, jsonObj.getString(COL_VNCATRUTAS_ID_CAT_RUTAS));
            values.put(COL_VNCATRUTAS_ID_COMPANIA, jsonObj.getString(COL_VNCATRUTAS_ID_COMPANIA));
            values.put(COL_VNCATRUTAS_ID_AGENTESPARAMETROS, jsonObj.getString(COL_VNCATRUTAS_ID_AGENTESPARAMETROS));
            values.put(COL_VNCATRUTAS_NOMBRERUTA, jsonObj.getString(COL_VNCATRUTAS_NOMBRERUTA));
            values.put(COL_VNCATRUTAS_DIASVENCIMIENTOCARTERA, jsonObj.getString(COL_VNCATRUTAS_DIASVENCIMIENTOCARTERA));
            values.put(COL_VNCATRUTAS_CUOTAVENTA, jsonObj.getString(COL_VNCATRUTAS_CUOTAVENTA));
            values.put(COL_VNCATRUTAS_CUOTACOBRANZA, jsonObj.getString(COL_VNCATRUTAS_CUOTACOBRANZA));
            values.put(COL_VNCATRUTAS_CUOTAMANO, jsonObj.getString(COL_VNCATRUTAS_CUOTAMANO));
            values.put(COL_VNCATRUTAS_CUOTAPEDIDOS, jsonObj.getString(COL_VNCATRUTAS_CUOTAPEDIDOS));
            values.put(COL_VNCATRUTAS_DIASINVENTARIO, jsonObj.getString(COL_VNCATRUTAS_DIASINVENTARIO));
            values.put(COL_VNCATRUTAS_ID_ALMACEN, jsonObj.getString(COL_VNCATRUTAS_ID_ALMACEN));

        } catch (JSONException e) {
            e.printStackTrace();
        } // fin del catch

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_VN_CAT_RUTAS, null, values);
        db.close();
    } // FIN DE insertaVnCatRutas


    // Metodo para validar el usuario y contrasena en el acceso
    public boolean validacion(String campoUsuario, String campoPassword) {

        String usuario = campoUsuario;
        String password = campoPassword;
        Boolean valida = false;

        Cursor cursor = getReadableDatabase().rawQuery("SELECT " + COL_GLACCESOS_CVEUSUARIO + ", " + COL_GLACCESOS_PASSWORD + " FROM " + TABLE_GL_ACCESOS + " WHERE " + COL_GLACCESOS_CVEUSUARIO + " = '" + usuario + "' AND " + COL_GLACCESOS_PASSWORD + " = '" + password + "' AND " + COL_GLACCESOS_ESTATUS + " = '1'", null);
        cursor.moveToFirst();
        String usuarioCheck = new String();
        String passwordCheck = new String();
        while (!cursor.isAfterLast()) {
            usuarioCheck = cursor.getString(cursor.getColumnIndex(COL_GLACCESOS_CVEUSUARIO));
            passwordCheck = cursor.getString(cursor.getColumnIndex(COL_GLACCESOS_PASSWORD));
            cursor.moveToNext();
        }
        cursor.close();

        if (usuario.equalsIgnoreCase(usuarioCheck) && password.equalsIgnoreCase(passwordCheck)) {
            valida = true;
        }
        return valida;
    }

    //Metodo para obtener el tipo de usuario que se logea, esto para ver si es agente, coordinador o usuario normal
    public String tipoUsuario(String cve) {
        String cve_usuario = cve;
        String tipo_usuario = "";

        Cursor cursor = getReadableDatabase().rawQuery("SELECT " + COL_GLACCESOS_TIPOUSUARIO + " FROM " + TABLE_GL_ACCESOS + " WHERE " + COL_GLACCESOS_CVEUSUARIO + " = '" + cve_usuario + "';", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            tipo_usuario = cursor.getString(cursor.getColumnIndex(COL_GLACCESOS_TIPOUSUARIO));
            cursor.moveToNext();
        }
        cursor.close();

        return tipo_usuario;
    }

    public String idAgentesParametros(String cve) {
        String cve_usuario = cve;
        String id_agentes_parametros = "";

        Cursor cursor = getReadableDatabase().rawQuery("SELECT " + COL_GLACCESOS_ID_AGENTESPARAMETROS + " FROM " + TABLE_GL_ACCESOS + " WHERE " + COL_GLACCESOS_CVEUSUARIO + " = '" + cve_usuario + "';", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            id_agentes_parametros = cursor.getString(cursor.getColumnIndex(COL_GLACCESOS_ID_AGENTESPARAMETROS));
            cursor.moveToNext();
        }
        cursor.close();

        return id_agentes_parametros;
    }

    // Metodo para obtener la ruta que se selecciono cuando no eres agente
    public String rutaSeleccionada() {
        String num_ruta = "";
        String Query = "SELECT " + COL_GLRUTASELECCIONADA_NUMRUTA + " FROM " + TABLE_GL_RUTA_SELECCIONADA + ";";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);

        while (cursor.moveToNext()) {
            num_ruta = cursor.getString(cursor.getColumnIndex(COL_GLRUTASELECCIONADA_NUMRUTA));
        }

        return num_ruta;
    }

    // Vacia la tabla de accesos si esta contiene informacion. Esta es por la primera ves que se crea la BD / Acceso
    public void resetGlAccesos() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM  " + TABLE_GL_ACCESOS);
        db.execSQL("DELETE FROM  " + TABLE_VN_CAT_RUTAS);
        //db.execSQL("DELETE FROM  " + TABLE_VN_CAT_AGENTES);
        db.close();
    }

    // Metodo para validar el usuario y contrasena en el acceso
    public boolean checkAccesos() {

        Boolean info = false;

        Cursor cursor = getReadableDatabase().rawQuery("SELECT " + COL_GLACCESOS_CVEUSUARIO + " FROM " + TABLE_GL_ACCESOS + " ORDER BY " + COL_GLACCESOS_CVEUSUARIO + " LIMIT 1", null);
        cursor.moveToFirst();
        String usuarioCheck = new String();
        while (!cursor.isAfterLast()) {
            usuarioCheck = cursor.getString(cursor.getColumnIndex(COL_GLACCESOS_CVEUSUARIO));
            cursor.moveToNext();
        }
        cursor.close();

        if (usuarioCheck.length() >= 1) {
            info = true;
        }

        return info;

    }

    // Al colocarse al inicio del c—digo provoca y forza la creaci—n f’sica de la BD as’ como sus tablas.
    public void checkDBStatus() {
        String query = "SELECT * FROM " + TABLE_GL_SYNC;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        cursor.close();
        db.close();
    }

    // Inserta el usuario entrante en la bitacora interna del sistema
    public void registraBitacora(String cveUsuario) {
        ContentValues values = new ContentValues();

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fechaRegistro = sdf.format(cal.getTime());
        String Query = "";

        values.put(COL_GLBITACORAACCESOS_CVEUSUARIO, cveUsuario);
        values.put(COL_GLBITACORAACCESOS_FECHAREGISTRO, fechaRegistro);
        SQLiteDatabase db = this.getWritableDatabase();

        // Medida de depuración: antes de registrar el usuario se borran los accesos que tengan mas de dos meses de la bitacora
        Query = "DELETE FROM  " + TABLE_GL_BITACORA_ACCESOS + " WHERE datetime(fecha_registro) < datetime(fecha_registro, '-2 month')";
        db.execSQL(Query);
        db.insert(TABLE_GL_BITACORA_ACCESOS, null, values);
        db.close();
    }

    //Metodo para sacar el listado de los agentes
    public String getListadoAgentes() {
        String JSonString = "[";
        Cursor cursor = getReadableDatabase().rawQuery("SELECT " + COL_VNCATRUTAS_ID_CAT_RUTAS + ", " + COL_GLACCESOS_NOMBRE + " FROM " + TABLE_GL_ACCESOS + " ga " +
                " INNER JOIN " + TABLE_VN_CAT_RUTAS + " vcr ON " +
                "ga." + COL_GLACCESOS_ID_AGENTESPARAMETROS + " = vcr." + COL_VNCATRUTAS_ID_AGENTESPARAMETROS +
                " GROUP BY vcr." + COL_VNCATRUTAS_ID_CAT_RUTAS +
                " ORDER BY ga." + COL_GLACCESOS_NOMBRE, null);
        cursor.moveToFirst();
        ArrayList<String> agentes = new ArrayList<String>();
        while (!cursor.isAfterLast()) {
            JSonString += "{" + "\"" + "num_ruta" + "\":\"" + cursor.getString(0) + "\"," +
                    "\"" + "nombre_agente" + "\":\"" + cursor.getString(1) + "\"},\n";
            agentes.add(cursor.getString(cursor.getColumnIndex("nombre")));
            cursor.moveToNext();
        }
        if (JSonString.length() > 1) {
            JSonString = JSonString.substring(0, JSonString.length() - 2);
        }
        JSonString += "]";
        cursor.close();
        return JSonString;
    }

    // Metodo que regresa la clave del ultimo usuario que fue registrado en la bitácora interna del sistema móvil en este dispositivo
    public String ultimoUsuarioRegistrado() {
        String cveUsuario = "";
        String Query = "SELECT " + COL_GLBITACORAACCESOS_CVEUSUARIO + " FROM " + TABLE_GL_BITACORA_ACCESOS + " ORDER BY datetime(fecha_registro) DESC LIMIT 1";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);

        while (cursor.moveToNext()) {
            cveUsuario = cursor.getString(0);
        }

        return cveUsuario;
    }

    public String num_ruta(String agente) {
        String Agente = agente;

        //Cursor cursor = getReadableDatabase().rawQuery("select num_ruta from vn_cat_rutas where cve_compania = '019' and cve_agente = '" + Agente + "';", null);
        Cursor cursor = getReadableDatabase().rawQuery("SELECT " + COL_VNCATRUTAS_ID_CAT_RUTAS + " FROM " + TABLE_VN_CAT_RUTAS + " vcr " +
                "INNER JOIN " + TABLE_GL_ACCESOS + " ga ON " +
                "vcr." + COL_VNCATRUTAS_ID_AGENTESPARAMETROS + " = ga." + COL_GLACCESOS_ID_AGENTESPARAMETROS + " " +
                "WHERE vcr." + COL_VNCATRUTAS_ID_COMPANIA + " = '19' AND ga." + COL_GLACCESOS_CVEUSUARIO + " = '" + Agente + "'", null);
        cursor.moveToFirst();
        String num_ruta = "";
        while (!cursor.isAfterLast()) {
            num_ruta = cursor.getString(cursor.getColumnIndex(COL_VNCATRUTAS_ID_CAT_RUTAS));
            cursor.moveToNext();
        }
        cursor.close();
        if (num_ruta.length() <= 0) {
            num_ruta = "0";
        }

        return num_ruta;
    }

    public String dameLogueado(String num_rutx) {
        String numRuta = num_rutx;

        //Cursor cursor = getReadableDatabase().rawQuery("select vca.nombre as nombrex from vn_cat_agentes vca inner join vn_cat_rutas vcr on vca.cve_usuario = vcr.cve_agente where vcr.cve_compania = '019' and vcr.num_ruta = '" + numRuta + "';", null);
        Cursor cursor = getReadableDatabase().rawQuery("SELECT " + COL_GLACCESOS_NOMBRE + " FROM " + TABLE_GL_ACCESOS + " ga " +
                "INNER JOIN " + TABLE_VN_CAT_RUTAS + " vcr ON " +
                "ga." + COL_GLACCESOS_ID_AGENTESPARAMETROS + " = vcr." + COL_VNCATRUTAS_ID_AGENTESPARAMETROS + " " +
                "WHERE vcr." + COL_VNCATRUTAS_ID_CAT_RUTAS + " = " + num_rutx, null);
        cursor.moveToFirst();
        String num_ruta = "";
        while (!cursor.isAfterLast()) {
            num_ruta = cursor.getString(cursor.getColumnIndex(COL_GLACCESOS_NOMBRE));
            cursor.moveToNext();
        }
        cursor.close();


        if (num_ruta.length() <= 0) {
            num_ruta = "0";
        }
        return num_ruta;
    }

    public boolean checkInformacionAgente() {

        Boolean info = false;
        String dato_cliente = "";


        Cursor cursor = getReadableDatabase().rawQuery("SELECT " + COL_GLACCESOS_CVEUSUARIO + " FROM " + TABLE_GL_ACCESOS + " WHERE " + COL_GLACCESOS_CVEUSUARIO + " != '' LIMIT 1", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            dato_cliente = cursor.getString(cursor.getColumnIndex(COL_GLACCESOS_CVEUSUARIO));
            cursor.moveToNext();
        }
        cursor.close();

        if (dato_cliente.length() == 0) {
            info = true;
        }

        return info;
    }

    // Inserta en la tabla gl_ruta_seleccionada cuando no es agente
    public void registraRutaSeleccionada(String numRuta) {
        ContentValues values = new ContentValues();

        Cursor cursor = getReadableDatabase().rawQuery("SELECT " + COL_GLACCESOS_CVEUSUARIO + " FROM " + TABLE_GL_ACCESOS + " ga " +
                "INNER JOIN " + TABLE_VN_CAT_RUTAS + " vcr ON " +
                "ga." + COL_GLACCESOS_ID_AGENTESPARAMETROS + " = vcr." + COL_VNCATRUTAS_ID_AGENTESPARAMETROS + " " +
                "WHERE vcr." + COL_VNCATRUTAS_ID_CAT_RUTAS + " = '" + numRuta + "';", null);
        cursor.moveToFirst();
        String cve_agente = "";
        while (!cursor.isAfterLast()) {
            cve_agente = cursor.getString(cursor.getColumnIndex(COL_GLACCESOS_CVEUSUARIO));
            cursor.moveToNext();
        }
        cursor.close();

        String Query = "";

        values.put(COL_GLRUTASELECCIONADA_NUMRUTA, numRuta);
        values.put(COL_GLRUTASELECCIONADA_CVEAGENTE, cve_agente);
        SQLiteDatabase db = this.getWritableDatabase();

        // Medida de depuración: antes de registrar el usuario se borran los accesos que tengan mas de dos meses de la bitacora
        Query = "DELETE FROM  " + TABLE_GL_RUTA_SELECCIONADA + " ;";
        db.execSQL(Query);
        db.insert(TABLE_GL_RUTA_SELECCIONADA, null, values);
        db.close();
    }

    // Inserta un registro individual en la tabla movil  in_cat_productos. Recibe un objeto de tipo JSON con estructura del catalogo de productos
    public void insertaInCatProductos(JSONObject jsonObj) {
        ContentValues values = new ContentValues();
        try {
            values.put(COL_INCATPRODUCTOS_CVECATPRODUCTO, jsonObj.getString(COL_INCATPRODUCTOS_CVECATPRODUCTO));
            values.put(COL_INCATPRODUCTOS_ID_COMPANIA, jsonObj.getString(COL_INCATPRODUCTOS_ID_COMPANIA));
            values.put(COL_INCATPRODUCTOS_CVEPRODUCTO, jsonObj.getString(COL_INCATPRODUCTOS_CVEPRODUCTO));
            values.put(COL_INCATPRODUCTOS_NOMPRODUCTO, jsonObj.getString(COL_INCATPRODUCTOS_NOMPRODUCTO));//COL_INCATPRODUCTOS_CVE_CLASIFICACION
            values.put(COL_INCATPRODUCTOS_CVE_CLASIFICACION, jsonObj.getString(COL_INCATPRODUCTOS_CVE_CLASIFICACION));
            values.put(COL_INCATPRODUCTOS_CVEUNIDADMEDIDA, jsonObj.getString(COL_INCATPRODUCTOS_CVEUNIDADMEDIDA));
            values.put(COL_INCATPRODUCTOS_PRECIOUNITARIO, jsonObj.getString(COL_INCATPRODUCTOS_PRECIOUNITARIO));
            values.put(COL_INCATPRODUCTOS_PIEZASPORPAQUETE, jsonObj.getString(COL_INCATPRODUCTOS_PIEZASPORPAQUETE));
            values.put(COL_INCATPRODUCTOS_ESTATUS, jsonObj.getString(COL_INCATPRODUCTOS_ESTATUS));
            values.put(COL_INCATPRODUCTOS_COMENTARIOS, jsonObj.getString(COL_INCATPRODUCTOS_COMENTARIOS));
            values.put(COL_INCATPRODUCTOS_IMAGEN, jsonObj.getString(COL_INCATPRODUCTOS_IMAGEN));
            values.put(COL_INCATPRODUCTOS_IEPS, jsonObj.getString(COL_INCATPRODUCTOS_IEPS));
            values.put(COL_INCATPRODUCTOS_ID_ORGANIZADOR, jsonObj.getString(COL_INCATPRODUCTOS_ID_ORGANIZADOR));
            values.put(COL_INCATPRODUCTOS_OFERTAS_INTELIGENTES, jsonObj.getString(COL_INCATPRODUCTOS_OFERTAS_INTELIGENTES));
            values.put(COL_INCATPRODUCTOS_BONIFICACIONES_INTELIGENTES, jsonObj.getString(COL_INCATPRODUCTOS_BONIFICACIONES_INTELIGENTES));

        } catch (JSONException e) {
            e.printStackTrace();
        } // fin del catch

        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        if (db.insert(TABLE_IN_CAT_PRODUCTOS, null, values) != -1) {
            db.setTransactionSuccessful();
        }
        db.endTransaction();
        db.close();

    } // FIN DE insertaInCatProductos

    public void insertaVntCatRancho(JSONObject jsonObj) {
        ContentValues values = new ContentValues();
        try {
            values.put(COL_VNTCATRANCHO_ID_RANCHO, jsonObj.getString(COL_VNTCATRANCHO_ID_RANCHO));
            values.put(COL_VNTCATRANCHO_ID_RANCHO_MOVIL, jsonObj.getString(COL_VNTCATRANCHO_ID_RANCHO_MOVIL));
            values.put(COL_VNTCATRANCHO_ID_CAT_RUTAS, jsonObj.getString(COL_VNTCATRANCHO_ID_CAT_RUTAS));
            values.put(COL_VNTCATRANCHO_ESTADO, jsonObj.getString(COL_VNTCATRANCHO_ESTADO));//COL_INCATPRODUCTOS_CVE_CLASIFICACION
            values.put(COL_VNTCATRANCHO_MUNICIPIO, jsonObj.getString(COL_VNTCATRANCHO_MUNICIPIO));
            values.put(COL_VNTCATRANCHO_POBLACION, jsonObj.getString(COL_VNTCATRANCHO_POBLACION));
            values.put(COL_VNTCATRANCHO_CODIGO_POSTAL, jsonObj.getString(COL_VNTCATRANCHO_CODIGO_POSTAL));
            values.put(COL_VNTCATRANCHO_NOMBRE_RANCHO, jsonObj.getString(COL_VNTCATRANCHO_NOMBRE_RANCHO));
            values.put(COL_VNTCATRANCHO_CORREO_ELECTRONICO, jsonObj.getString(COL_VNTCATRANCHO_CORREO_ELECTRONICO));
            values.put(COL_VNTCATRANCHO_REFERENCIA, jsonObj.getString(COL_VNTCATRANCHO_REFERENCIA));
            values.put(COL_VNTCATRANCHO_LATITUDE, jsonObj.getString(COL_VNTCATRANCHO_LATITUDE));
            values.put(COL_VNTCATRANCHO_LONGITUDE, jsonObj.getString(COL_VNTCATRANCHO_LONGITUDE));
            values.put(COL_VNTCATRANCHO_SINCRONIZADO, jsonObj.getString(COL_VNTCATRANCHO_SINCRONIZADO));

        } catch (JSONException e) {
            e.printStackTrace();
        } // fin del catch

        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        if (db.insert(TABLE_VNT_CAT_RANCHO, null, values) != -1) {
            db.setTransactionSuccessful();
        }
        db.endTransaction();
        db.close();

    }

    public String getSiguienteRancho(){
        String query = "SELECT CASE WHEN MAX(" + COL_VNTCATRANCHO_ID + ") IS NULL THEN 1 ELSE MAX(" + COL_VNTCATRANCHO_ID + ") + 1 END AS proximo_pago " +
                "FROM " + TABLE_VNT_CAT_RANCHO;
        Cursor cursor = getReadableDatabase().rawQuery(query, null);
        cursor.moveToFirst();
        long proximoPago = 0;
        while (!cursor.isAfterLast()) {
            proximoPago = Long.parseLong(cursor.getString(cursor.getColumnIndex("proximo_pago")));
            cursor.moveToNext();
        }
        cursor.close();

        return "" + proximoPago;
    }

    public String getSiguienteVisita(){
        String query = "SELECT CASE WHEN MAX(" + COL_VNTRANCHOVISITA_ID + ") IS NULL THEN 1 ELSE MAX(" + COL_VNTRANCHOVISITA_ID + ") + 1 END AS proximo_pago " +
                "FROM " + TABLE_VNT_RANCHO_VISITA;
        Cursor cursor = getReadableDatabase().rawQuery(query, null);
        cursor.moveToFirst();
        long proximoPago = 0;
        while (!cursor.isAfterLast()) {
            proximoPago = Long.parseLong(cursor.getString(cursor.getColumnIndex("proximo_pago")));
            cursor.moveToNext();
        }
        cursor.close();

        return "" + proximoPago;
    }

    public String getSiguienteMuestra(){
        String query = "SELECT CASE WHEN MAX(" + COL_VNTRANCHOMUESTRAS_ID + ") IS NULL THEN 1 ELSE MAX(" + COL_VNTRANCHOMUESTRAS_ID + ") + 1 END AS proximo_pago " +
                "FROM " + TABLE_VNT_RANCHO_MUESTRAS;
        Cursor cursor = getReadableDatabase().rawQuery(query, null);
        cursor.moveToFirst();
        long proximoPago = 0;
        while (!cursor.isAfterLast()) {
            proximoPago = Long.parseLong(cursor.getString(cursor.getColumnIndex("proximo_pago")));
            cursor.moveToNext();
        }
        cursor.close();

        return "" + proximoPago;
    }

    public String getSiguienteFoto(){
        String query = "SELECT CASE WHEN MAX(" + COL_VNTFOTOSVISITA_ID + ") IS NULL THEN 1 ELSE MAX(" + COL_VNTFOTOSVISITA_ID + ") + 1 END AS proximo_pago " +
                "FROM " + TABLE_VNT_FOTOS_VISITA;
        Cursor cursor = getReadableDatabase().rawQuery(query, null);
        cursor.moveToFirst();
        long proximoPago = 0;
        while (!cursor.isAfterLast()) {
            proximoPago = Long.parseLong(cursor.getString(cursor.getColumnIndex("proximo_pago")));
            cursor.moveToNext();
        }
        cursor.close();

        return "" + proximoPago;
    }


    public String getIdAgentesParametros(String agente) {
        String Agente = agente;

        //Cursor cursor = getReadableDatabase().rawQuery("select num_ruta from vn_cat_rutas where cve_compania = '019' and cve_agente = '" + Agente + "';", null);
        Cursor cursor = getReadableDatabase().rawQuery("SELECT ga." + COL_GLACCESOS_ID_AGENTESPARAMETROS + " FROM " + TABLE_VN_CAT_RUTAS + " vcr " +
                "INNER JOIN " + TABLE_GL_ACCESOS + " ga ON " +
                "vcr." + COL_VNCATRUTAS_ID_AGENTESPARAMETROS + " = ga." + COL_GLACCESOS_ID_AGENTESPARAMETROS + " " +
                "WHERE vcr." + COL_VNCATRUTAS_ID_COMPANIA + " = '19' AND ga." + COL_GLACCESOS_CVEUSUARIO + " = '" + Agente + "'", null);
        cursor.moveToFirst();
        String num_ruta = "";
        while (!cursor.isAfterLast()) {
            num_ruta = cursor.getString(cursor.getColumnIndex(COL_GLACCESOS_ID_AGENTESPARAMETROS));
            cursor.moveToNext();
        }
        cursor.close();
        if (num_ruta.length() <= 0) {
            num_ruta = "0";
        }

        return num_ruta;
    }
    public String agenteSeleccionado() {
        String cve_agente = "";
        String Query = "SELECT " + COL_GLRUTASELECCIONADA_CVEAGENTE + " FROM " + TABLE_GL_RUTA_SELECCIONADA + ";";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);

        while (cursor.moveToNext()) {
            cve_agente = cursor.getString(cursor.getColumnIndex(COL_GLRUTASELECCIONADA_CVEAGENTE));
        }

        return cve_agente;
    }

    public boolean insertaCatRancho(ContentValues pedido_encabezado) {
        boolean respuesta = true;
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        if (db.insert(TABLE_VNT_CAT_RANCHO, null, pedido_encabezado) != -1) {
            db.setTransactionSuccessful();
            db.endTransaction();
        } else {
            respuesta = false;
        }
        db.close();

        return respuesta;
    }

    public boolean realizaInsert(ContentValues values, String tabla) {
        boolean respuesta = true;
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        if (db.insert(tabla, null, values) != -1) {
            db.setTransactionSuccessful();
            db.endTransaction();
        } else {
            respuesta = false;
        }
        db.close();

        return respuesta;
    }

    public String transmiteRanshos() {

        String query = "SELECT " + COL_VNTCATRANCHO_ID_RANCHO_MOVIL + ","+ COL_VNTCATRANCHO_ID_CAT_RUTAS + ", " + COL_VNTCATRANCHO_ESTADO + ", " + COL_VNTCATRANCHO_MUNICIPIO + ", " + COL_VNTCATRANCHO_POBLACION +
                ", " + COL_VNTCATRANCHO_CODIGO_POSTAL + ", " + COL_VNTCATRANCHO_NOMBRE_RANCHO +
                ", " + COL_VNTCATRANCHO_TELEFONO_CONTACTO + ", " + COL_VNTCATRANCHO_CORREO_ELECTRONICO + ", " + COL_VNTCATRANCHO_ID + ", " +
                COL_VNTCATRANCHO_REFERENCIA + ", " + COL_VNTCATRANCHO_LATITUDE + ", " + COL_VNTCATRANCHO_LATITUDE +
                " FROM " + TABLE_VNT_CAT_RANCHO + " WHERE " + COL_VNTCATRANCHO_SINCRONIZADO + " = '0'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        JSONObject json_pedidos = new JSONObject();
        JSONObject pedido = new JSONObject();
        while (!cursor.isAfterLast()) {
            ///PERROTE
            JSONObject x = new JSONObject();
            try {
                x.put(COL_VNTCATRANCHO_ID_RANCHO_MOVIL, cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_ID_RANCHO_MOVIL)));
                x.put(COL_VNTCATRANCHO_ID_CAT_RUTAS, cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_ID_CAT_RUTAS)));
                x.put(COL_VNTCATRANCHO_ESTADO, cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_ESTADO)));
                x.put(COL_VNTCATRANCHO_MUNICIPIO, cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_MUNICIPIO)));
                x.put(COL_VNTCATRANCHO_POBLACION, cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_POBLACION)));
                x.put(COL_VNTCATRANCHO_CODIGO_POSTAL, cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_CODIGO_POSTAL)));
                x.put(COL_VNTCATRANCHO_NOMBRE_RANCHO, cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_NOMBRE_RANCHO)));
                x.put(COL_VNTCATRANCHO_TELEFONO_CONTACTO, cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_TELEFONO_CONTACTO)));
                x.put(COL_VNTCATRANCHO_CORREO_ELECTRONICO, cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_CORREO_ELECTRONICO)));
                x.put(COL_VNTCATRANCHO_REFERENCIA, cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_REFERENCIA)));
                x.put(COL_VNTCATRANCHO_LATITUDE, cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_LATITUDE)));
                x.put(COL_VNTCATRANCHO_LATITUDE, cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_LATITUDE)));

                String id_ransho = cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_ID));
                pedido.put("rancho" + id_ransho, x);
            } catch (JSONException e) {
                Log.e("@@transmiteRanshos()", "ERROR: " + e);
            }
            cursor.moveToNext();
        }
        cursor.close();
        try {
            json_pedidos.put("Ranchos", pedido);
        } catch (JSONException e) {
            Log.e("@@ERR", "ERROR PERRO, funcion de MyDBHandler ==> transmiteRanshos()" + e);
        }
        String respuesta = json_pedidos.toString();
        Log.e("@@JSON RANSHOS WE ==> ", respuesta);
        return "[" + respuesta + "]";
    } // Fin de transmite visitas

    public String dameNombreRancho(String id_rancho_movil){
        String nombre_ransho = "";
        String Query = "SELECT " + COL_VNTCATRANCHO_NOMBRE_RANCHO + " FROM " + TABLE_VNT_CAT_RANCHO + " WHERE " + COL_VNTCATRANCHO_ID_RANCHO_MOVIL + " = '" + id_rancho_movil + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);

        while (cursor.moveToNext()) {
            nombre_ransho = cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_NOMBRE_RANCHO));
        }

        return nombre_ransho;
    }

    public String dameNombreRanchoFinal(String id_rancho){
        String nombre_ransho = "";
        String Query = "SELECT " + COL_VNTCATRANCHO_NOMBRE_RANCHO + " FROM " + TABLE_VNT_CAT_RANCHO + " WHERE " + COL_VNTCATRANCHO_ID_RANCHO + " = '" + id_rancho + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);

        while (cursor.moveToNext()) {
            nombre_ransho = cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_NOMBRE_RANCHO));
        }

        return nombre_ransho;
    }

    public void ranchoSincronizado(String id_rancho, String id_rancho_movil) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("UPDATE  " + TABLE_VNT_CAT_RANCHO + " SET " + COL_VNTCATRANCHO_ID_RANCHO + " = '" + id_rancho + "', " + COL_VNTCATRANCHO_SINCRONIZADO + " = '1'" +
                " WHERE " + COL_VNTCATRANCHO_ID_RANCHO_MOVIL + " = '" + id_rancho_movil + "'");
        db.close();
    }

    public void visitaSincronizada(String id_rancho, String id_rancho_movil) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("UPDATE  " + TABLE_VNT_RANCHO_VISITA + " SET " + COL_VNTRANCHOVISITA_ID_RANCHO + " = '" + id_rancho + "'" +
                " WHERE " + COL_VNTRANCHOVISITA_ID_RANCHO_MOVIL + " = '" + id_rancho_movil + "'");
        db.close();
    }

    public void visitaSincronizadaFinal(String id_visita, String id_visita_movil) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("UPDATE  " + TABLE_VNT_RANCHO_VISITA + " SET " + COL_VNTRANCHOVISITA_ID_VISITA + " = '" + id_visita + "', " + COL_VNTRANCHOVISITA_SINCRONIZADO + " = '1'" +
                " WHERE " + COL_VNTRANCHOVISITA_ID_VISITA_MOVIL + " = '" + id_visita_movil + "'");
        db.close();
    }

    public void muestraSincronizadaFromVisitas(String id_visita, String id_visita_movil) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("UPDATE  " + TABLE_VNT_RANCHO_MUESTRAS + " SET " + COL_VNTRANCHOMUESTRAS_ID_VISITA + " = '" + id_visita + "'" +
                " WHERE " + COL_VNTRANCHOMUESTRAS_ID_VISITA_MOVIL + " = '" + id_visita_movil + "'");
        db.close();
    }

    public void fotoSincronizadaFromVisitas(String id_visita, String id_visita_movil) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("UPDATE  " + TABLE_VNT_FOTOS_VISITA + " SET " + COL_VNTFOTOSVISITA_ID_VISITA + " = '" + id_visita + "'" +
                " WHERE " + COL_VNTFOTOSVISITA_ID_VISITA_MOVIL + " = '" + id_visita_movil + "'");
        db.close();
    }

    public void muestraSincronizadaFinal(String id_muestra, String id_muestra_movil) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("UPDATE  " + TABLE_VNT_RANCHO_MUESTRAS + " SET " + COL_VNTRANCHOMUESTRAS_ID_MUESTRA + " = '" + id_muestra + "', " + COL_VNTRANCHOMUESTRAS_SINCRONIZADO + " = '1'" +
                " WHERE " + COL_VNTRANCHOMUESTRAS_ID_MUESTRA_MOVIL + " = '" + id_muestra_movil + "'");
        db.close();
    }

    public void fotoSincronizadaFinal(String id_foto, String id_foto_movil) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("UPDATE  " + TABLE_VNT_FOTOS_VISITA + " SET " + COL_VNTFOTOSVISITA_ID_FOTO + " = '" + id_foto + "', " + COL_VNTFOTOSVISITA_SINCRONIZADO + " = '1'" +
                " WHERE " + COL_VNTFOTOSVISITA_ID_FOTO_MOVIL + " = '" + id_foto_movil + "'");
        db.close();
    }

    // Metodo para ver si se quedo algo pendiente de enviar en la parte de pagos, pedidos y visitas
    public boolean checkPendienteEnvio() {

        Boolean info = false;

        String catRancho = "";
        String muestra = "";
        String visita = "";
        String foto = "";

        //Se verifica primero en la parte de pedidos
        Cursor cursor = getReadableDatabase().rawQuery("SELECT " + COL_VNTCATRANCHO_ID_RANCHO_MOVIL + " FROM " + TABLE_VNT_CAT_RANCHO + " WHERE " + COL_VNTCATRANCHO_SINCRONIZADO + " = '0' LIMIT 1", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            catRancho = cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_ID_RANCHO_MOVIL));
            cursor.moveToNext();
        }
        cursor.close();

        //Se verifica en la parte de pagos
        Cursor cursor2 = getReadableDatabase().rawQuery("SELECT " + COL_VNTRANCHOVISITA_ID_VISITA_MOVIL + " FROM " + TABLE_VNT_RANCHO_VISITA + " WHERE " + COL_VNTRANCHOVISITA_SINCRONIZADO + " = '0' AND " +
                COL_VNTRANCHOVISITA_ID_RANCHO + " != '0'  LIMIT 1", null);
        cursor2.moveToFirst();
        while (!cursor2.isAfterLast()) {
            visita = cursor2.getString(cursor2.getColumnIndex(COL_VNTRANCHOVISITA_ID_VISITA_MOVIL));
            cursor2.moveToNext();
        }
        cursor2.close();

        cursor2 = getReadableDatabase().rawQuery("SELECT " + COL_VNTRANCHOMUESTRAS_ID_MUESTRA_MOVIL + " FROM " + TABLE_VNT_RANCHO_MUESTRAS + " WHERE " + COL_VNTRANCHOMUESTRAS_SINCRONIZADO + " = '0' AND " +
                COL_VNTRANCHOMUESTRAS_ID_VISITA + " != '0' LIMIT 1", null);
        cursor2.moveToFirst();
        while (!cursor2.isAfterLast()) {
            muestra = cursor2.getString(cursor2.getColumnIndex(COL_VNTRANCHOMUESTRAS_ID_MUESTRA_MOVIL));
            cursor2.moveToNext();
        }
        cursor2.close();

        cursor2 = getReadableDatabase().rawQuery("SELECT " + COL_VNTFOTOSVISITA_ID_FOTO_MOVIL + " FROM " + TABLE_VNT_FOTOS_VISITA+ " WHERE " + COL_VNTFOTOSVISITA_SINCRONIZADO + " = '0' AND " +
                COL_VNTFOTOSVISITA_ID_VISITA + " != '0' LIMIT 1", null);
        cursor2.moveToFirst();
        while (!cursor2.isAfterLast()) {
            foto = cursor2.getString(cursor2.getColumnIndex(COL_VNTFOTOSVISITA_ID_FOTO_MOVIL));
            cursor2.moveToNext();
        }
        cursor2.close();

        ///Se realiza la validacion si alguno de los tres tiene informacion la variable se va a verdadero
        if (catRancho.length() > 0) {
            info = true;
        }
        if (visita.length() > 0) {
            info = true;
        }
        if(muestra.length() > 0){
            info = true;
        }
        if(foto.length() > 0){
            info = true;
        }
        Log.e("PENDIENTES", "hay predientes de envío");
        Log.e("PENDIENTES", "info vale: "+info);
        return info;
    }

    public String[] damePendientesEnvio(){
        ArrayList<String> pendientes = new ArrayList<String>();
        Cursor cursor = getReadableDatabase().rawQuery("SELECT " + COL_VNTCATRANCHO_ID_RANCHO_MOVIL + " FROM " + TABLE_VNT_CAT_RANCHO + " WHERE " + COL_VNTCATRANCHO_SINCRONIZADO + " = '0' LIMIT 1", null);
        cursor.moveToFirst();
        String catRancho = "";
        while (!cursor.isAfterLast()) {
            catRancho = cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_ID_RANCHO_MOVIL));
            cursor.moveToNext();
        }
        if(catRancho.length() > 0){
            pendientes.add("1");
        }
        cursor.close();

        cursor = getReadableDatabase().rawQuery("SELECT " + COL_VNTRANCHOVISITA_ID_VISITA_MOVIL + " FROM " + TABLE_VNT_RANCHO_VISITA + " WHERE " + COL_VNTRANCHOVISITA_SINCRONIZADO + " = '0' AND " +
                COL_VNTRANCHOVISITA_ID_RANCHO + " != '0'  LIMIT 1", null);
        cursor.moveToFirst();
        String visita = "";
        while (!cursor.isAfterLast()) {
            visita = cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOVISITA_ID_VISITA_MOVIL));
            cursor.moveToNext();
        }
        if(visita.length() > 0){
            pendientes.add("2");
        }
        cursor.close();

        cursor = getReadableDatabase().rawQuery("SELECT " + COL_VNTRANCHOMUESTRAS_ID_MUESTRA_MOVIL + " FROM " + TABLE_VNT_RANCHO_MUESTRAS + " WHERE " + COL_VNTRANCHOMUESTRAS_SINCRONIZADO + " = '0' AND " +
                        COL_VNTRANCHOMUESTRAS_ID_VISITA + " != '0' LIMIT 1", null);
        cursor.moveToFirst();
        String muestra = "";
        while (!cursor.isAfterLast()) {
            muestra = cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOMUESTRAS_ID_MUESTRA_MOVIL));
            cursor.moveToNext();
        }
        if(muestra.length() > 0){
            pendientes.add("3");
        }
        cursor.close();

        cursor = getReadableDatabase().rawQuery("SELECT " + COL_VNTFOTOSVISITA_ID_FOTO_MOVIL + " FROM " + TABLE_VNT_FOTOS_VISITA + " WHERE " + COL_VNTFOTOSVISITA_SINCRONIZADO + " = '0' AND " +
                        COL_VNTFOTOSVISITA_ID_VISITA + " != '0' LIMIT 1", null);
        cursor.moveToFirst();
        String foto = "";
        while (!cursor.isAfterLast()) {
            foto = cursor.getString(cursor.getColumnIndex(COL_VNTFOTOSVISITA_ID_FOTO_MOVIL));
            cursor.moveToNext();
        }
        if(foto.length() > 0){
            pendientes.add("4");
        }
        cursor.close();

        return pendientes.toArray(new String[pendientes.size()]);
    }

    public String[] getRanchos(String sincronizado) {
        String leyenda = (sincronizado.equals("0"))? " (No sincronizado)" : "";
        String columna = (sincronizado.equals("0"))? "'[' || " + COL_VNTCATRANCHO_ID_RANCHO_MOVIL + " || ']'" : COL_VNTCATRANCHO_ID_RANCHO;
        Cursor cursor = getReadableDatabase().rawQuery("SELECT '' || " + columna + " || '- ' || " + COL_VNTCATRANCHO_NOMBRE_RANCHO + " || '" + leyenda + "' AS ransho FROM " + TABLE_VNT_CAT_RANCHO + " WHERE " + COL_VNTCATRANCHO_SINCRONIZADO + " = '" + sincronizado + "' ORDER BY " + COL_VNTCATRANCHO_NOMBRE_RANCHO + ";", null);
        cursor.moveToFirst();
        ArrayList<String> nombreCliente = new ArrayList<String>();
        while (!cursor.isAfterLast()) {
            nombreCliente.add(cursor.getString(cursor.getColumnIndex("ransho")));
            cursor.moveToNext();
        }
        cursor.close();
        return nombreCliente.toArray(new String[nombreCliente.size()]);
    }

    public double precioProducto(String cve_cat_producto) {
        double precio_unitario = 0.0;
        String query = "SELECT " + COL_INCATPRODUCTOS_PRECIOUNITARIO + " FROM " + TABLE_IN_CAT_PRODUCTOS +
                " WHERE " + COL_INCATPRODUCTOS_CVECATPRODUCTO + " = '" + cve_cat_producto + "'";
        Cursor cursor = getReadableDatabase().rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            precio_unitario = cursor.getDouble(cursor.getColumnIndex(COL_INCATPRODUCTOS_PRECIOUNITARIO));
            cursor.moveToNext();
        }
        cursor.close();

        return precio_unitario;
    }

    public String[] getAllInvCatProductos() {
        String query = "SELECT " + COL_INCATPRODUCTOS_CVECATPRODUCTO + ", " + COL_INCATPRODUCTOS_CVEPRODUCTO + ", " + COL_INCATPRODUCTOS_NOMPRODUCTO + " " +
                "FROM " + TABLE_IN_CAT_PRODUCTOS + " WHERE " + COL_INCATPRODUCTOS_CVEUNIDADMEDIDA + " != 'PAQ' " +
                "GROUP BY " + COL_INCATPRODUCTOS_CVECATPRODUCTO;
        Cursor cursor = getReadableDatabase().rawQuery(query, null);
        ArrayList<String> productos = new ArrayList<String>();
        cursor.moveToFirst();
        String productox = "";
        while (!cursor.isAfterLast()) {
            productox = cursor.getString(cursor.getColumnIndex(COL_INCATPRODUCTOS_CVECATPRODUCTO)) + "- (" +
                    cursor.getString(cursor.getColumnIndex(COL_INCATPRODUCTOS_CVEPRODUCTO)) + ") " +
                    cursor.getString(cursor.getColumnIndex(COL_INCATPRODUCTOS_NOMPRODUCTO));
            productos.add(productox);
            cursor.moveToNext();
        }
        cursor.close();
        return productos.toArray(new String[productos.size()]);
    }

    public String pzasPaquetex(String cve) {
        int piezas_por_paquete = 0;
        String cve_cat_producto = cve;
        String msje = "";

        Cursor cursor = getReadableDatabase().rawQuery("SELECT " + COL_INCATPRODUCTOS_PIEZASPORPAQUETE + " FROM " + TABLE_IN_CAT_PRODUCTOS + " WHERE " + COL_INCATPRODUCTOS_CVECATPRODUCTO + " = '" + cve_cat_producto + "';", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            piezas_por_paquete = cursor.getInt(cursor.getColumnIndex(COL_INCATPRODUCTOS_PIEZASPORPAQUETE));
            cursor.moveToNext();
        }
        cursor.close();
        msje = "" + piezas_por_paquete;

        return msje;
    }

    public boolean realizaDelete(String tabla, String campo, String valor) {
        boolean respuesta = true;
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        if (db.delete(tabla, campo + "=" + valor, null) != -1) {
            db.setTransactionSuccessful();
            db.endTransaction();
        } else {
            respuesta = false;
        }
        db.close();

        return respuesta;
    }


    public boolean checkInformacionTabla(String tabla, String columnaBuscar, String columna, String valor) {

        Boolean info = true;
        String dato_cliente = "";


        Cursor cursor = getReadableDatabase().rawQuery("SELECT " + columnaBuscar + " FROM " + tabla + " WHERE " + columna + " = '" + valor + "' LIMIT 1", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            dato_cliente = cursor.getString(cursor.getColumnIndex(columnaBuscar));
            cursor.moveToNext();
        }
        cursor.close();

        if (dato_cliente.length() == 0) {
            info = true;
        }

        return info;
    }

    public String transmiteVisitas() {

        String query = "SELECT " + COL_VNTRANCHOVISITA_ID_RANCHO + ","+ COL_VNTRANCHOVISITA_ID_VISITA_MOVIL + ", " + COL_VNTRANCHOVISITA_FECHA_VISITA + ", " +
                COL_VNTRANCHOVISITA_CULTIVO + ", " + COL_VNTRANCHOVISITA_EXTENSION_SIEMBRA +
                ", " + COL_VNTRANCHOVISITA_PROBLEMA + ", " + COL_VNTRANCHOVISITA_SOLUCION_BIOCHEM +
                ", " + COL_VNTRANCHOVISITA_MUESTRAS + ", " + COL_VNTRANCHOVISITA_PROXIMA_VISITA + ", " + COL_VNTRANCHOVISITA_OBSERVACIONES + ", " +
                COL_VNTRANCHOVISITA_ID + " FROM " + TABLE_VNT_RANCHO_VISITA + " WHERE " + COL_VNTRANCHOVISITA_SINCRONIZADO + " = '0' AND " +
                COL_VNTRANCHOVISITA_ID_RANCHO + " != '0'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        JSONObject json_pedidos = new JSONObject();
        JSONObject pedido = new JSONObject();
        while (!cursor.isAfterLast()) {
            ///PERROTE
            JSONObject x = new JSONObject();
            try {
                x.put(COL_VNTRANCHOVISITA_ID_RANCHO, cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOVISITA_ID_RANCHO)));
                x.put(COL_VNTRANCHOVISITA_ID_VISITA_MOVIL, cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOVISITA_ID_VISITA_MOVIL)));
                x.put(COL_VNTRANCHOVISITA_FECHA_VISITA, cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOVISITA_FECHA_VISITA)));
                x.put(COL_VNTRANCHOVISITA_CULTIVO, cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOVISITA_CULTIVO)));
                x.put(COL_VNTRANCHOVISITA_EXTENSION_SIEMBRA, cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOVISITA_EXTENSION_SIEMBRA)));
                x.put(COL_VNTRANCHOVISITA_PROBLEMA, cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOVISITA_PROBLEMA)));
                x.put(COL_VNTRANCHOVISITA_SOLUCION_BIOCHEM, cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOVISITA_SOLUCION_BIOCHEM)));
                x.put(COL_VNTRANCHOVISITA_MUESTRAS, cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOVISITA_MUESTRAS)));
                x.put(COL_VNTRANCHOVISITA_PROXIMA_VISITA, cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOVISITA_PROXIMA_VISITA)));
                x.put(COL_VNTRANCHOVISITA_OBSERVACIONES, cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOVISITA_OBSERVACIONES)));

                String id_visita = cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOVISITA_ID));
                pedido.put("visita" + id_visita, x);
            } catch (JSONException e) {
                Log.e("@@transmiteVisitas()", "ERROR: " + e);
            }
            cursor.moveToNext();
        }
        cursor.close();
        try {
            json_pedidos.put("Visitas", pedido);
        } catch (JSONException e) {
            Log.e("@@ERR", "ERROR PERRO, funcion de RanchoDB ==> transmiteVisitas()" + e);
        }
        String respuesta = json_pedidos.toString();
        Log.e("@@JSON VISITAS WE ==> ", respuesta);
        return "[" + respuesta + "]";
    } // Fin de transmite visitas


    public String transmiteMuestras() {
        String query = "SELECT " + COL_VNTRANCHOMUESTRAS_ID_VISITA + ","+ COL_VNTRANCHOMUESTRAS_ID_MUESTRA_MOVIL + ", " + COL_VNTRANCHOMUESTRAS_CVE_CAT_PRODUCTO + ", " +
                COL_VNTRANCHOMUESTRAS_CANTIDAD + ", " + COL_VNTRANCHOMUESTRAS_ID +
                " FROM " + TABLE_VNT_RANCHO_MUESTRAS + " WHERE " + COL_VNTRANCHOMUESTRAS_SINCRONIZADO + " = '0' AND " +
                COL_VNTRANCHOMUESTRAS_ID_VISITA + " != '0'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        JSONObject json_pedidos = new JSONObject();
        JSONObject pedido = new JSONObject();
        while (!cursor.isAfterLast()) {
            ///PERROTE
            JSONObject x = new JSONObject();
            try {
                x.put(COL_VNTRANCHOMUESTRAS_ID_VISITA, cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOMUESTRAS_ID_VISITA)));
                x.put(COL_VNTRANCHOMUESTRAS_ID_MUESTRA_MOVIL, cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOMUESTRAS_ID_MUESTRA_MOVIL)));
                x.put(COL_VNTRANCHOMUESTRAS_CVE_CAT_PRODUCTO, cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOMUESTRAS_CVE_CAT_PRODUCTO)));
                x.put(COL_VNTRANCHOMUESTRAS_CANTIDAD, cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOMUESTRAS_CANTIDAD)));

                String id_muestra = cursor.getString(cursor.getColumnIndex(COL_VNTRANCHOMUESTRAS_ID));
                pedido.put("muestra" + id_muestra, x);
            } catch (JSONException e) {
                Log.e("@@transmiteMuestras()", "ERROR: " + e);
            }
            cursor.moveToNext();
        }
        cursor.close();
        try {
            json_pedidos.put("Muestras", pedido);
        } catch (JSONException e) {
            Log.e("@@ERR", "ERROR PERRO, funcion de RanchoDB ==> transmiteMuestras()" + e);
        }
        String respuesta = json_pedidos.toString();
        Log.e("@@JSON MUESTRAS WE ==> ", respuesta);
        return "[" + respuesta + "]";
    } // Fin de transmite visitas

    public String transmiteFotos() {

        String query = "SELECT " + COL_VNTFOTOSVISITA_ID_VISITA + ","+ COL_VNTFOTOSVISITA_ID_FOTO_MOVIL + ", " + COL_VNTFOTOSVISITA_FOTO + ", " +
                COL_VNTFOTOSVISITA_ID +
                " FROM " + TABLE_VNT_FOTOS_VISITA + " WHERE " + COL_VNTFOTOSVISITA_SINCRONIZADO + " = '0' AND " +
                COL_VNTFOTOSVISITA_ID_VISITA + " != '0'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        JSONObject json_pedidos = new JSONObject();
        JSONObject pedido = new JSONObject();
        while (!cursor.isAfterLast()) {
            ///PERROTE
            JSONObject x = new JSONObject();
            try {
                x.put(COL_VNTFOTOSVISITA_ID_VISITA, cursor.getString(cursor.getColumnIndex(COL_VNTFOTOSVISITA_ID_VISITA)));
                x.put(COL_VNTFOTOSVISITA_ID_FOTO_MOVIL, cursor.getString(cursor.getColumnIndex(COL_VNTFOTOSVISITA_ID_FOTO_MOVIL)));
                x.put(COL_VNTFOTOSVISITA_FOTO, cursor.getString(cursor.getColumnIndex(COL_VNTFOTOSVISITA_FOTO)));

                String id_foto = cursor.getString(cursor.getColumnIndex(COL_VNTFOTOSVISITA_ID));
                pedido.put("foto" + id_foto, x);
            } catch (JSONException e) {
                Log.e("@@transmiteFotos()", "ERROR: " + e);
            }
            cursor.moveToNext();
        }
        cursor.close();
        try {
            json_pedidos.put("Fotos", pedido);
        } catch (JSONException e) {
            Log.e("@@ERR", "ERROR PERRO, funcion de RanchoDB ==> transmiteFotos()" + e);
        }
        String respuesta = json_pedidos.toString();
        Log.e("@@JSON FOTOS WE ==> ", respuesta);
        return "[" + respuesta + "]";
    } // Fin de transmite visitas

    public String dameRanchoFromVisita(String id_visita){
        String nombre_ransho = "";
        String Query = "SELECT vcr." + COL_VNTCATRANCHO_NOMBRE_RANCHO + " FROM " + TABLE_VNT_CAT_RANCHO + " vcr " +
                " INNER JOIN " + TABLE_VNT_RANCHO_VISITA + " vrv ON vcr." + COL_VNTCATRANCHO_ID_RANCHO + " = " +
                "vrv." + COL_VNTRANCHOVISITA_ID_RANCHO +
                " WHERE vrv." + COL_VNTRANCHOVISITA_ID_VISITA + " = '" + id_visita + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);

        while (cursor.moveToNext()) {
            nombre_ransho = cursor.getString(cursor.getColumnIndex(COL_VNTCATRANCHO_NOMBRE_RANCHO));
        }

        return nombre_ransho;
    }

}
