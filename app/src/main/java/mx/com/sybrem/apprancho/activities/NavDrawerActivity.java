package mx.com.sybrem.apprancho.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.com.sybrem.apprancho.R;
import mx.com.sybrem.apprancho.clases.ExpandableListAdapter;
import mx.com.sybrem.apprancho.clases.RanchoDB;
import mx.com.sybrem.apprancho.clases.general.CheckNetwork;
import mx.com.sybrem.apprancho.clases.general.DialogsBiochem;
import mx.com.sybrem.apprancho.clases.general.GlobalsBioshem;

public class NavDrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, DialogsBiochem.CustomAccion{

    String respue = "";

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "ranchoTablet.db";

    AlertDialog.Builder pd2;
    AlertDialog dialog;
    View view;
    ProgressDialog pd;
    ProgressDialog pdSender; // Mensaje de progreso en sincronización para JSonTaskSender.
    ProgressDialog pdSendPayment; // Mensaje de progreso en sincronizacion.
    ProgressDialog pdSendVisitas; // Mensaje de progreso en sincronizacion Visitas.
    ProgressDialog pdSendProspectos;// Mensaje de progreso en sincronizacion Prospectos.
    String url_debug = "", pedidos_debug = "";
    String bufferts = "";
    int sincronizada = 0;
    ImageView imgPrincipal;

    View view_Group;
    private DrawerLayout mDrawerLayout;
    ExpandableListAdapter mMenuAdapter;
    ExpandableListView expandableList;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    //Icons, use as you want
    public static int[] icon = {R.drawable.ic_prod_nuevo, R.drawable.ic_listado,
            R.drawable.ic_reporte, R.drawable.ic_cobranza, R.drawable.ic_salir};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_drawer);

        final RanchoDB dbHandler = new RanchoDB(this, null, null, DATABASE_VERSION);
        dbHandler.checkDBStatus(); // Tiene como fin forzar la creacion de la base de datos.

        //Se pasan los datos del usuario que se logeo
        String usuarioLogeado = "";
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        usuarioLogeado = (String) extras.get("usuario");

        //Actuaizamos en la bitacora el usuario logeado
        dbHandler.registraBitacora(usuarioLogeado);

        final String usuario = dbHandler.ultimoUsuarioRegistrado();

        String TipoUsuario = dbHandler.tipoUsuario(usuario);
        String ruta = "";

        if (TipoUsuario.equals("A")) {
            ruta = dbHandler.num_ruta(usuario);
        } else {
            ruta = dbHandler.rutaSeleccionada();
        }
        Log.e("RUTX", "RUTA = "+ruta);

        imgPrincipal = (ImageView)findViewById(R.id.principal);
        String url_imgPrincipal = "http://sybrem.com.mx/adsnet/syncmovil/img_ofertas/oferta1.png";
        Glide.with(NavDrawerActivity.this)
                .load(url_imgPrincipal)
                .into(imgPrincipal);

        String logueao_we = dbHandler.dameLogueado(ruta);
        this.setTitle("Agente: " + logueao_we);


        //Se verifica si el usuario ya tiene informacion para poder operar si no es el caso sincroniza
        if (CheckNetwork.isInternetAvailable(NavDrawerActivity.this)) //returns true if internet available
        {
            //Se verifica si el agente ya tiene informacion para poder operar
            boolean infoAgente = dbHandler.checkInformacionAgente();
            Log.e("##@@ info_clientes", "es => " + infoAgente);

            //Si es true quiere decir que hay que sincronizar para obtener la informacion
            if (infoAgente == true) {
                dbHandler.resetCatalogs(); // Elimina la información de los catálogos existentes
                // Descarga de la base de datos los catalogos.
                new ObtieneDatos(dbHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarx);
        setSupportActionBar(toolbar);

        //Se verifica si hay algo pendiente por sincronizar si es el caso mostramos icono de notificacion y opcion en el menu_backup
        Boolean pendiente = false;
        pendiente = dbHandler.checkPendienteEnvio();

        ImageView notificacion = (ImageView) findViewById(R.id.ImgNotificacion);
        Glide.with(NavDrawerActivity.this)
                .load(R.drawable.notify)
                .into(notificacion);

        if (pendiente == true) {
            notificacion.setVisibility(View.VISIBLE);
        } else {
            notificacion.setVisibility(View.INVISIBLE);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        expandableList = (ExpandableListView) findViewById(R.id.menue);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }
        prepareListData();
        mMenuAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expandableList.setAdapter(mMenuAdapter);
        expandableList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long id) {
                //Log.d("DEBUG", "submenu item clicked");
                view.setSelected(true);
                Intent explicit_intent;
                String nov_id = "";

                String usuarioLog = dbHandler.ultimoUsuarioRegistrado();
                String TipoUsuario = dbHandler.tipoUsuario(usuarioLog);
                String ruta = "";

                if (TipoUsuario.toString().equals("A")) {
                    ruta = dbHandler.num_ruta(usuarioLog);
                } else {
                    ruta = dbHandler.rutaSeleccionada();
                }

                switch (groupPosition) {
                    case 0://PROCESOS
                        switch (childPosition) {
                            case 0:
                                finish();
                                explicit_intent = new Intent(NavDrawerActivity.this, RegistraRancho.class);
                                startActivity(explicit_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                                break;
                            case 1:
                                finish();
                                explicit_intent = new Intent(NavDrawerActivity.this, RegistraVisita.class);
                                startActivity(explicit_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                                break;
                        }
                        break;
                    case 1://OTROS
                        switch (childPosition) {
                            case 0:
                                explicit_intent = new Intent(NavDrawerActivity.this, VideosBiochem.class);
                                explicit_intent.putExtra("descargad", "0");
                                startActivity(explicit_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                                finish();
                                break;
                            case 1:
                                finish();
                                explicit_intent = new Intent(NavDrawerActivity.this, Login.class);
                                startActivity(explicit_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                                break;
                        }
                }

                if (view_Group != null) {
                    view_Group.setBackgroundColor(Color.parseColor("#ffffff"));
                }
                view_Group = view;
                view_Group.setBackgroundColor(Color.parseColor("#DDDDDD"));
                mDrawerLayout.closeDrawers();
                return false;

            }
        });
        expandableList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                return false;
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            expandableList.setIndicatorBounds(expandableList.getRight() - 80, expandableList.getWidth());
        } else {
            expandableList.setIndicatorBoundsRelative(expandableList.getRight() - 80, expandableList.getWidth());
        }
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        final RanchoDB dbHandler = new RanchoDB(this, null, null, DATABASE_VERSION);

        // Inflate the menu_backup; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav_drawer, menu);

        //Se pasan los datos del usuario que se logeo
        String usuarioLogeado = "";
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        usuarioLogeado = (String) extras.get("usuario");
        String tipo_usuario = dbHandler.tipoUsuario(usuarioLogeado);

        if (!tipo_usuario.equals("A")) {
            super.onPrepareOptionsMenu(menu);
            menu.findItem(R.id.cambiar_agente).setVisible(true);
        }
        if (tipo_usuario.equals("A")) {
            super.onPrepareOptionsMenu(menu);
            menu.findItem(R.id.cambiar_agente).setVisible(false);
        }

        //Se verifica si hay algo pendiente por sincronizar si es el caso mostramos icono de notificacion y opcion en el menu_backup
        Boolean pendiente = false;
        pendiente = dbHandler.checkPendienteEnvio();

        if (pendiente == true) {
            super.onPrepareOptionsMenu(menu);
            menu.findItem(R.id.pendientes).setVisible(true);
        } else {
            super.onPrepareOptionsMenu(menu);
            menu.findItem(R.id.pendientes).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        final RanchoDB dbHandler = new RanchoDB(this, null, null, DATABASE_VERSION);
        int id = item.getItemId();
        String usuario = dbHandler.ultimoUsuarioRegistrado();

        if (id == R.id.cambiar_agente) {
            Intent explicit_intent;
            explicit_intent = new Intent(NavDrawerActivity.this, EligeRuta.class);
            explicit_intent.putExtra("usuario", usuario);
            startActivity(explicit_intent);
        } else if (id == R.id.pendientes) {

            if (CheckNetwork.isInternetAvailable(NavDrawerActivity.this)) //returns true if internet available
            {
                // Primero nos preocupamos por el envío de los pedidos que no se han transmitido
                //new JsonTaskSender(dbHandler).execute();
                sincronizaMesta("");
            } else {
                DialogsBiochem.muestraDialogoError(NavDrawerActivity.this, "No cuenta con una conexión a internet, conéctese a internet e intentelo de nuevo.");
            } // Fin del if que verifica la conexión a internet
        }

        return super.onOptionsItemSelected(item);
    }

    //Formamos el menu principal de la app, con los items y los items hijos
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        listDataHeader.add("Procesos");
        listDataHeader.add("Otros");

        List<String> heading2 = new ArrayList<String>();
        heading2.add("Registrar rancho");
        heading2.add("Registrar visita a rancho");
        //heading2.add("Envío de reporte");

        List<String> heading3 = new ArrayList<String>();
        heading3.add("Videos Maiceros");
        heading3.add("Salir");

        listDataChild.put(listDataHeader.get(0), heading2);
        listDataChild.put(listDataHeader.get(1), heading3);
    }

    //En caso de presionar la tecla de regresar
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

        }
    }

    @Override
    public void customAccion() {
        finActivity();
    }

    public void finActivity(){
        RanchoDB dbHandler = new RanchoDB(NavDrawerActivity.this, DATABASE_NAME, null, 1);
        dbHandler.checkDBStatus();
        String usuario = dbHandler.ultimoUsuarioRegistrado();
        finish();
        Intent explicit_intent;
        explicit_intent = new Intent(NavDrawerActivity.this, NavDrawerActivity.class);
        explicit_intent.putExtra("usuario", usuario);
        explicit_intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        explicit_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(explicit_intent);
        overridePendingTransition(R.anim.lista_deabajo_parriba, R.anim.fab_close);
        return;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class ObtieneDatos extends AsyncTask<String, String, String>
    {
        // Bloque para poder usar los metodos de la clase MyDBHandler en la clase JsonTask
        public RanchoDB dbHandler;

        public ObtieneDatos(RanchoDB dbHandler)
        {
            this.dbHandler = dbHandler;
        }
        // Termina bloque de inclusion de los metodos de MyDBHandler (Se uso esta practica debido a que ya esta usado extends en la clase

        protected void onPreExecute()
        {
            super.onPreExecute();
            AlertDialog.Builder pd = new AlertDialog.Builder(NavDrawerActivity.this);
            pd.setCancelable(false);
            view = getLayoutInflater().inflate(R.layout.dialog_transparente, null);

            TextView tvClasif = (TextView) view.findViewById(R.id.txt);
            tvClasif.setText("Obteniendo datos de servidor, espere...");
            ImageView imagen = (ImageView) view.findViewById(R.id.imgGif);
            Glide.with(NavDrawerActivity.this)
                    .load(R.drawable.tractorzaso)
                    .into(imagen);
            pd.setView(view);
            dialog = pd.create();
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimacionArribaAbajo;
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }

        protected String doInBackground(String... params)
        {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND + Process.THREAD_PRIORITY_MORE_FAVORABLE);
            String usuarioLog = dbHandler.ultimoUsuarioRegistrado();
            String TipoUsuario = dbHandler.tipoUsuario(usuarioLog);
            String ruta = "";

            if(TipoUsuario.equals("A")) {
                ruta = dbHandler.num_ruta(usuarioLog);
            }
            else{
                ruta = dbHandler.rutaSeleccionada();
            }

            String urlCatalogoProductos = "http://sybrem.com.mx/adsnet/syncmovil/java/catalogos.php?gol&tabla=productos&ruta=0&gol2&rancho";
            String urlCatalogoRanchos = "http://sybrem.com.mx/adsnet/syncmovil/java/appRancho/catalogos.php?tabla=ranchos&ruta="+ruta;

            /*************************************************************
             * Bloque para el catalogo de Productos:                      *
             * ***********************************************************/
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            String JsonUrlCatalogoProductos = "";

            try {
                URL url = new URL(urlCatalogoProductos);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                JsonUrlCatalogoProductos = buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            // Una vez obtenida la cadena JSon del segundo catalogo se lee linea por lina para insertar en la tabla
            try {
                // Convierte el string con la informaciÑn de los clientes en un array JSON
                JSONArray jsonArr_productos = new JSONArray(JsonUrlCatalogoProductos);

                for (int i = 0; i < jsonArr_productos.length(); i++) {
                    Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND + Process.THREAD_PRIORITY_MORE_FAVORABLE);
                    JSONObject jsonObjCatalogoProductos = jsonArr_productos.getJSONObject(i);
                    dbHandler.insertaInCatProductos(jsonObjCatalogoProductos);
                }
            } // Fin del Try
            catch (JSONException e) {
                e.printStackTrace();
            } // fin del catch

            /*************************************************************
             * Bloque para el catalogo de RANCHOS                        *
             * ***********************************************************/
            connection = null;
            reader = null;
            String JsonUrlCatalogoRanchos = "";

            try {
                URL url = new URL(urlCatalogoRanchos);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                JsonUrlCatalogoRanchos = buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            // Una vez obtenida la cadena JSon del segundo catalogo se lee linea por lina para insertar en la tabla
            try {
                // Convierte el string con la informaciÑn de los clientes en un array JSON
                JSONArray jsonArr_ranchos = new JSONArray(JsonUrlCatalogoRanchos);

                for (int i = 0; i < jsonArr_ranchos.length(); i++) {
                    Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND + Process.THREAD_PRIORITY_MORE_FAVORABLE);
                    JSONObject jsonObjCatalogoRanchos = jsonArr_ranchos.getJSONObject(i);
                    dbHandler.insertaVntCatRancho(jsonObjCatalogoRanchos);
                }
            } // Fin del Try
            catch (JSONException e) {
                e.printStackTrace();
            } // fin del catch


            return null;
        }

        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);

            if (dialog.isShowing())
            {
                dialog.dismiss();
                //reload();
            }

        }
    }// FIN DEL AsyncTask obtieneDatos()

    public void sincronizaMesta(String id_proceso){
        Log.e("SINC", "Entró a \"sincronizaMesta\"");
        RanchoDB dbHandler = new RanchoDB(NavDrawerActivity.this, DATABASE_NAME, null, DATABASE_VERSION);
        String[] array_pendientes = dbHandler.damePendientesEnvio();
        String usuario = dbHandler.ultimoUsuarioRegistrado();
        if(array_pendientes.length > 0){
            Log.e("SINC", "HAY PENDIENTES");
            String id = "";
            if(id_proceso.equals("")){
                id = array_pendientes[0];
            }else{
                Log.e("SINC", "id_proceso: "+ id_proceso);
                int longitud_array = array_pendientes.length; //ped pag vis
                for(int i = 0; i < longitud_array; i++){
                    String aux = array_pendientes[i];
                    if(id_proceso.equals(aux)){
                        int r = longitud_array - 1;
                        if(r == i){

                        }else{
                            id = array_pendientes[i+1];
                        }
                    }
                }
            }
            Log.e("SINC", "id: "+ id);
            switch(id){
                case "1":
                    Log.e("SINC", "se van a enviar los ranshos pendientes");
                    new EnviaRanshos(dbHandler).execute();
                    break;
                case "2":
                    new EnviaVisitas(dbHandler).execute();
                    break;
                case "3":
                    new EnviaMuestras(dbHandler).execute();
                    break;
                case "4" :
                    new EnviaFotos(dbHandler).execute();
                    break;
                default:
                    finActivity();
                    break;
            }
        }else{
            Log.e("SINC", "VALIENDO VERGA");
            finish();
            Intent explicit_intent;
            explicit_intent = new Intent(NavDrawerActivity.this, NavDrawerActivity.class);
            explicit_intent.putExtra("usuario", usuario);
            explicit_intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            explicit_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(explicit_intent);
            NavDrawerActivity.this.invalidateOptionsMenu();
        }
    }

    public class EnviaRanshos extends AsyncTask<String, String, String> {
        // Bloque para poder usar los metodos de la clase MyDBHandler en la clase JsonTask
        private RanchoDB dbHandler;

        public EnviaRanshos(RanchoDB dbHandler) {
            this.dbHandler = dbHandler;
        }
        // Termina bloque de inclusion de los metodos de MyDBHandler (Se uso esta practica debido a que ya esta usado extends en la clase

        protected void onPreExecute() {
            super.onPreExecute();

            pd2 = new AlertDialog.Builder(NavDrawerActivity.this);
            pd2.setCancelable(false);
            //LayoutInflater inflater = (LayoutInflater) CapturaPedidosFin.this.getSystemService( CapturaPedidosFin.this.LAYOUT_INFLATER_SERVICE );
            View view = getLayoutInflater().inflate(R.layout.dialog_transparente, null);

            TextView tvClasif = (TextView) view.findViewById(R.id.txt);
            tvClasif.setText("Enviando datos al servidor, espere...");
            ImageView imgEncabezado = (ImageView)view.findViewById(R.id.imgGif);
            Glide.with(NavDrawerActivity.this)
                    .load(R.drawable.broilanimated)
                    .into(imgEncabezado);

            pd2.setView(view);
            dialog = pd2.create();
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }

        protected String doInBackground(String... params) {

            /*************************************************************
             * Bloque para envio de los pedidos :                        *
             * ***********************************************************/
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            String respuesta_servidor = "";
            String urlSendPedidito = ((GlobalsBioshem)NavDrawerActivity.this.getApplication()).getUrlSendRanchos() + URLEncoder.encode(dbHandler.transmiteRanshos());// + InfoEnviar.enviaInfo();

            try {

                URL url = new URL(urlSendPedidito);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                respuesta_servidor = buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return respuesta_servidor;
        } // FIn del doInBackground

        @Override
        protected void onPostExecute(String respuesta_servidor) {
            super.onPostExecute(respuesta_servidor);

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            String codigo_respuesta = "";
            String mensaje_respuesta = "";
            String errs = "", success = "";
            try {
                JSONArray respuesta = new JSONArray(respuesta_servidor);
                for(int i = 0; i < respuesta.length(); i++){
                    JSONObject obj = respuesta.getJSONObject(i);
                    codigo_respuesta = obj.getString("codigo_respuesta");
                    mensaje_respuesta = obj.getString("mensaje_respuesta");
                    errs = obj.getString("errores");
                    success = obj.getString("success");
                }
                if(codigo_respuesta.equals("201")){
                    JSONArray errores_ranchos = new JSONArray(errs);
                    String msje_malos = "";
                    for(int i = 0; i < errores_ranchos.length(); i++){
                        JSONObject wrong = errores_ranchos.getJSONObject(i);
                        String id_rancho_movil = wrong.getString("id_rancho_movil");
                        String ransho = dbHandler.dameNombreRancho(id_rancho_movil);
                        msje_malos += "*El rancho " + ransho + " no se pudo sincronizar con el servidor, el error fue: "+wrong.getString("err")+".\n\n";
                    }
                    JSONArray success_ranchos = new JSONArray(success);
                    if(success_ranchos.length() > 0){
                        for(int i = 0; i < success_ranchos.length(); i++){
                            JSONObject good = success_ranchos.getJSONObject(i);
                            String id_rancho = good.getString("id_rancho");
                            String id_rancho_movil = good.getString("id_rancho_movil");
                            dbHandler.ranchoSincronizado(id_rancho, id_rancho_movil);
                            boolean existeVisita = dbHandler.checkInformacionTabla(dbHandler.TABLE_VNT_RANCHO_VISITA, dbHandler.COL_VNTRANCHOVISITA_ID_VISITA_MOVIL,
                                    dbHandler.COL_VNTRANCHOVISITA_ID_RANCHO_MOVIL, id_rancho_movil);
                            if(existeVisita == true){
                                dbHandler.visitaSincronizada(id_rancho, id_rancho_movil);
                            }
                        }
                    }
                    mensaje_respuesta += "\n"+msje_malos;
                    DialogsBiochem.muestraDialogoWarningCustom(NavDrawerActivity.this, mensaje_respuesta, new DialogsBiochem.CustomAccion() {
                        @Override
                        public void customAccion() {
                            sincronizaMesta("1");
                        }
                    }, 1);
                }
                if(codigo_respuesta.equals("200")){
                    JSONArray success_ranchos = new JSONArray(success);
                    if(success_ranchos.length() > 0){
                        for(int i = 0; i < success_ranchos.length(); i++){
                            JSONObject good = success_ranchos.getJSONObject(i);
                            String id_rancho = good.getString("id_rancho");
                            String id_rancho_movil = good.getString("id_rancho_movil");
                            dbHandler.ranchoSincronizado(id_rancho, id_rancho_movil);
                            boolean existeVisita = dbHandler.checkInformacionTabla(dbHandler.TABLE_VNT_RANCHO_VISITA, dbHandler.COL_VNTRANCHOVISITA_ID_VISITA_MOVIL,
                                    dbHandler.COL_VNTRANCHOVISITA_ID_RANCHO_MOVIL, id_rancho_movil);
                            if(existeVisita == true){
                                dbHandler.visitaSincronizada(id_rancho, id_rancho_movil);
                            }
                        }
                    }
                    DialogsBiochem.muestraDialogoSuccessCustom(NavDrawerActivity.this, mensaje_respuesta, new DialogsBiochem.CustomAccion() {
                        @Override
                        public void customAccion() {
                            sincronizaMesta("");
                        }
                    });
                }else{
                    DialogsBiochem.muestraDialogoErrorCustom(NavDrawerActivity.this, "CODIGO DE ERROR: " + codigo_respuesta + ".\n" + mensaje_respuesta, new DialogsBiochem.CustomAccion() {
                        @Override
                        public void customAccion() {
                            sincronizaMesta("1");
                        }
                    });
                }
            } catch (JSONException e) {
                DialogsBiochem.muestraDialogoErrorCustom(NavDrawerActivity.this, "Ocurrio un error al convertir la respuesta del servidor:"+respuesta_servidor+"\n" + e, NavDrawerActivity.this::customAccion);
            }
        } // Fin del onPostExecute
    } // Fin de la clase EnviaRanshos

    public class EnviaVisitas extends AsyncTask<String, String, String> {
        // Bloque para poder usar los metodos de la clase MyDBHandler en la clase JsonTask
        private RanchoDB dbHandler;

        public EnviaVisitas(RanchoDB dbHandler) {
            this.dbHandler = dbHandler;
        }
        // Termina bloque de inclusion de los metodos de MyDBHandler (Se uso esta practica debido a que ya esta usado extends en la clase

        protected void onPreExecute() {
            super.onPreExecute();

            pd2 = new AlertDialog.Builder(NavDrawerActivity.this);
            pd2.setCancelable(false);
            //LayoutInflater inflater = (LayoutInflater) CapturaPedidosFin.this.getSystemService( CapturaPedidosFin.this.LAYOUT_INFLATER_SERVICE );
            View view = getLayoutInflater().inflate(R.layout.dialog_transparente, null);

            TextView tvClasif = (TextView) view.findViewById(R.id.txt);
            tvClasif.setText("Enviando datos al servidor, espere...");
            ImageView imgEncabezado = (ImageView)view.findViewById(R.id.imgGif);
            Glide.with(NavDrawerActivity.this)
                    .load(R.drawable.broilanimated)
                    .into(imgEncabezado);

            pd2.setView(view);
            dialog = pd2.create();
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }

        protected String doInBackground(String... params) {

            /*************************************************************
             * Bloque para envio de los pedidos :                        *
             * ***********************************************************/
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            String respuesta_servidor = "";
            String urlSendPedidito = ((GlobalsBioshem)NavDrawerActivity.this.getApplication()).getUrlSendVisitas() + URLEncoder.encode(dbHandler.transmiteVisitas());// + InfoEnviar.enviaInfo();

            try {

                URL url = new URL(urlSendPedidito);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                respuesta_servidor = buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return respuesta_servidor;
        } // FIn del doInBackground

        @Override
        protected void onPostExecute(String respuesta_servidor) {
            super.onPostExecute(respuesta_servidor);

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            String codigo_respuesta = "";
            String mensaje_respuesta = "";
            String errs = "", success = "";
            try {
                JSONArray respuesta = new JSONArray(respuesta_servidor);
                for(int i = 0; i < respuesta.length(); i++){
                    JSONObject obj = respuesta.getJSONObject(i);
                    codigo_respuesta = obj.getString("codigo_respuesta");
                    mensaje_respuesta = obj.getString("mensaje_respuesta");
                    errs = obj.getString("errores");
                    success = obj.getString("success");
                }
                if(codigo_respuesta.equals("201")){
                    JSONArray errores_visitas = new JSONArray(errs);
                    String msje_malos = "";
                    for(int i = 0; i < errores_visitas.length(); i++){
                        JSONObject wrong = errores_visitas.getJSONObject(i);
                        String id_visita_movil = wrong.getString("id_visita_movil");
                        String id_rancho = wrong.getString("id_rancho");
                        String ransho = dbHandler.dameNombreRanchoFinal(id_rancho);
                        msje_malos += "*La visita al rancho " + ransho + " no se pudo sincronizar con el servidor, el error fue: "+wrong.getString("err")+".\n\n";
                    }
                    JSONArray success_visitas = new JSONArray(success);
                    if(success_visitas.length() > 0){
                        for(int i = 0; i < success_visitas.length(); i++){
                            JSONObject good = success_visitas.getJSONObject(i);
                            String id_visita = good.getString("id_visita");
                            String id_visita_movil = good.getString("id_visita_movil");
                            dbHandler.visitaSincronizadaFinal(id_visita, id_visita_movil);

                            boolean existeMuestra = dbHandler.checkInformacionTabla(dbHandler.TABLE_VNT_RANCHO_MUESTRAS, dbHandler.COL_VNTRANCHOMUESTRAS_ID,
                                    dbHandler.COL_VNTRANCHOMUESTRAS_ID_VISITA_MOVIL, id_visita_movil);
                            if(existeMuestra == true){
                                dbHandler.muestraSincronizadaFromVisitas(id_visita, id_visita_movil);
                            }

                            boolean existeFoto = dbHandler.checkInformacionTabla(dbHandler.TABLE_VNT_FOTOS_VISITA, dbHandler.COL_VNTFOTOSVISITA_ID,
                                    dbHandler.COL_VNTFOTOSVISITA_ID_VISITA_MOVIL, id_visita_movil);
                            if(existeFoto == true){
                                dbHandler.fotoSincronizadaFromVisitas(id_visita, id_visita_movil);
                            }
                        }
                    }
                    mensaje_respuesta += "\n"+msje_malos;
                    DialogsBiochem.muestraDialogoWarningCustom(NavDrawerActivity.this, mensaje_respuesta, new DialogsBiochem.CustomAccion() {
                        @Override
                        public void customAccion() {
                            sincronizaMesta("2");
                        }
                    }, 1);
                }
                if(codigo_respuesta.equals("200")){
                    JSONArray success_ranchos = new JSONArray(success);
                    if(success_ranchos.length() > 0){
                        for(int i = 0; i < success_ranchos.length(); i++){
                            JSONObject good = success_ranchos.getJSONObject(i);
                            String id_visita = good.getString("id_visita");
                            String id_visita_movil = good.getString("id_visita_movil");
                            dbHandler.visitaSincronizadaFinal(id_visita, id_visita_movil);

                            boolean existeVisita = dbHandler.checkInformacionTabla(dbHandler.TABLE_VNT_RANCHO_MUESTRAS, dbHandler.COL_VNTRANCHOMUESTRAS_ID,
                                    dbHandler.COL_VNTRANCHOMUESTRAS_ID_VISITA_MOVIL, id_visita_movil);
                            if(existeVisita == true){
                                dbHandler.muestraSincronizadaFromVisitas(id_visita, id_visita_movil);
                            }
                            boolean existeFoto = dbHandler.checkInformacionTabla(dbHandler.TABLE_VNT_FOTOS_VISITA, dbHandler.COL_VNTFOTOSVISITA_ID,
                                    dbHandler.COL_VNTFOTOSVISITA_ID_VISITA_MOVIL, id_visita_movil);
                            if(existeFoto == true){
                                dbHandler.fotoSincronizadaFromVisitas(id_visita, id_visita_movil);
                            }
                        }
                    }
                    DialogsBiochem.muestraDialogoSuccessCustom(NavDrawerActivity.this, mensaje_respuesta, new DialogsBiochem.CustomAccion() {
                        @Override
                        public void customAccion() {
                            sincronizaMesta("");
                        }
                    });
                }else{
                    DialogsBiochem.muestraDialogoErrorCustom(NavDrawerActivity.this, "CODIGO DE ERROR: " + codigo_respuesta + ".\n" + mensaje_respuesta, NavDrawerActivity.this::customAccion);
                }
            } catch (JSONException e) {
                DialogsBiochem.muestraDialogoErrorCustom(NavDrawerActivity.this, "Ocurrio un error al convertir la respuesta del servidor:"+respuesta_servidor+"\n" + e, NavDrawerActivity.this::customAccion);
            }
        } // Fin del onPostExecute
    }

    public class EnviaMuestras extends AsyncTask<String, String, String> {
        // Bloque para poder usar los metodos de la clase MyDBHandler en la clase JsonTask
        private RanchoDB dbHandler;

        public EnviaMuestras(RanchoDB dbHandler) {
            this.dbHandler = dbHandler;
        }
        // Termina bloque de inclusion de los metodos de MyDBHandler (Se uso esta practica debido a que ya esta usado extends en la clase

        protected void onPreExecute() {
            super.onPreExecute();

            pd2 = new AlertDialog.Builder(NavDrawerActivity.this);
            pd2.setCancelable(false);
            //LayoutInflater inflater = (LayoutInflater) CapturaPedidosFin.this.getSystemService( CapturaPedidosFin.this.LAYOUT_INFLATER_SERVICE );
            View view = getLayoutInflater().inflate(R.layout.dialog_transparente, null);

            TextView tvClasif = (TextView) view.findViewById(R.id.txt);
            tvClasif.setText("Enviando datos al servidor, espere...");
            ImageView imgEncabezado = (ImageView)view.findViewById(R.id.imgGif);
            Glide.with(NavDrawerActivity.this)
                    .load(R.drawable.broilanimated)
                    .into(imgEncabezado);

            pd2.setView(view);
            dialog = pd2.create();
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }

        protected String doInBackground(String... params) {

            /*************************************************************
             * Bloque para envio de los pedidos :                        *
             * ***********************************************************/
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            String respuesta_servidor = "";
            String urlSendPedidito = ((GlobalsBioshem)NavDrawerActivity.this.getApplication()).getUrlSendMuestras() + URLEncoder.encode(dbHandler.transmiteMuestras());// + InfoEnviar.enviaInfo();

            try {

                URL url = new URL(urlSendPedidito);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                respuesta_servidor = buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return respuesta_servidor;
        } // FIn del doInBackground

        @Override
        protected void onPostExecute(String respuesta_servidor) {
            super.onPostExecute(respuesta_servidor);

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            String codigo_respuesta = "";
            String mensaje_respuesta = "";
            String errs = "", success = "";
            try {
                JSONArray respuesta = new JSONArray(respuesta_servidor);
                for(int i = 0; i < respuesta.length(); i++){
                    JSONObject obj = respuesta.getJSONObject(i);
                    codigo_respuesta = obj.getString("codigo_respuesta");
                    mensaje_respuesta = obj.getString("mensaje_respuesta");
                    errs = obj.getString("errores");
                    success = obj.getString("success");
                }
                if(codigo_respuesta.equals("201")){
                    JSONArray errores_muestras = new JSONArray(errs);
                    String msje_malos = "";
                    for(int i = 0; i < errores_muestras.length(); i++){
                        JSONObject wrong = errores_muestras.getJSONObject(i);
                        String id_muestra_movil = wrong.getString("id_muestra_movil");
                        String id_visita = wrong.getString("id_visita");
                        String ransho = dbHandler.dameRanchoFromVisita(id_visita);
                        msje_malos += "*Una muestra para el rancho " + ransho + " no se pudo sincronizar con el servidor, el error fue: "+wrong.getString("err")+".\n\n";
                    }
                    JSONArray success_muestras = new JSONArray(success);
                    if(success_muestras.length() > 0){
                        for(int i = 0; i < success_muestras.length(); i++){
                            JSONObject good = success_muestras.getJSONObject(i);
                            String id_muestra = good.getString("id_muestra");
                            String id_muestra_movil = good.getString("id_muestra_movil");
                            dbHandler.muestraSincronizadaFinal(id_muestra, id_muestra_movil);
                        }
                    }
                    mensaje_respuesta += "\n"+msje_malos;
                    DialogsBiochem.muestraDialogoWarningCustom(NavDrawerActivity.this, mensaje_respuesta, new DialogsBiochem.CustomAccion() {
                        @Override
                        public void customAccion() {
                            sincronizaMesta("3");
                        }
                    }, 1);
                }
                if(codigo_respuesta.equals("200")){
                    JSONArray success_ranchos = new JSONArray(success);
                    if(success_ranchos.length() > 0){
                        for(int i = 0; i < success_ranchos.length(); i++){
                            JSONObject good = success_ranchos.getJSONObject(i);
                            String id_muestra = good.getString("id_muestra");
                            String id_muestra_movil = good.getString("id_muestra_movil");
                            dbHandler.muestraSincronizadaFinal(id_muestra, id_muestra_movil);
                        }
                    }
                    DialogsBiochem.muestraDialogoSuccessCustom(NavDrawerActivity.this, mensaje_respuesta, new DialogsBiochem.CustomAccion() {
                        @Override
                        public void customAccion() {
                            sincronizaMesta("");
                        }
                    });
                }else{
                    DialogsBiochem.muestraDialogoErrorCustom(NavDrawerActivity.this, "CODIGO DE ERROR: " + codigo_respuesta + ".\n" + mensaje_respuesta, NavDrawerActivity.this::customAccion);
                }
            } catch (JSONException e) {
                DialogsBiochem.muestraDialogoErrorCustom(NavDrawerActivity.this, "Ocurrio un error al convertir la respuesta del servidor:"+respuesta_servidor+"\n" + e, NavDrawerActivity.this::customAccion);
            }
        } // Fin del onPostExecute
    }// Fin de envia muestras

    public class EnviaFotos extends AsyncTask<String, String, String> {
        // Bloque para poder usar los metodos de la clase MyDBHandler en la clase JsonTask
        private RanchoDB dbHandler;

        public EnviaFotos(RanchoDB dbHandler) {
            this.dbHandler = dbHandler;
        }
        // Termina bloque de inclusion de los metodos de MyDBHandler (Se uso esta practica debido a que ya esta usado extends en la clase

        protected void onPreExecute() {
            super.onPreExecute();

            pd2 = new AlertDialog.Builder(NavDrawerActivity.this);
            pd2.setCancelable(false);
            //LayoutInflater inflater = (LayoutInflater) CapturaPedidosFin.this.getSystemService( CapturaPedidosFin.this.LAYOUT_INFLATER_SERVICE );
            View view = getLayoutInflater().inflate(R.layout.dialog_transparente, null);

            TextView tvClasif = (TextView) view.findViewById(R.id.txt);
            tvClasif.setText("Enviando datos al servidor, espere...");
            ImageView imgEncabezado = (ImageView)view.findViewById(R.id.imgGif);
            Glide.with(NavDrawerActivity.this)
                    .load(R.drawable.broilanimated)
                    .into(imgEncabezado);

            pd2.setView(view);
            dialog = pd2.create();
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }

        protected String doInBackground(String... params) {

            /*************************************************************
             * Bloque para envio de los pedidos :                        *
             * ***********************************************************/
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            String respuesta_servidor = "";
            String urlSendPedidito = ((GlobalsBioshem)NavDrawerActivity.this.getApplication()).getUrlSendFotos() + URLEncoder.encode(dbHandler.transmiteFotos());// + InfoEnviar.enviaInfo();

            try {

                URL url = new URL(urlSendPedidito);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                respuesta_servidor = buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return respuesta_servidor;
        } // FIn del doInBackground

        @Override
        protected void onPostExecute(String respuesta_servidor) {
            super.onPostExecute(respuesta_servidor);

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            String codigo_respuesta = "";
            String mensaje_respuesta = "";
            String errs = "", success = "";
            try {
                JSONArray respuesta = new JSONArray(respuesta_servidor);
                for(int i = 0; i < respuesta.length(); i++){
                    JSONObject obj = respuesta.getJSONObject(i);
                    codigo_respuesta = obj.getString("codigo_respuesta");
                    mensaje_respuesta = obj.getString("mensaje_respuesta");
                    errs = obj.getString("errores");
                    success = obj.getString("success");
                }
                if(codigo_respuesta.equals("201")){
                    JSONArray errores_fotos = new JSONArray(errs);
                    String msje_malos = "";
                    for(int i = 0; i < errores_fotos.length(); i++){
                        JSONObject wrong = errores_fotos.getJSONObject(i);
                        String id_foto_movil = wrong.getString("id_foto_movil");
                        String id_visita = wrong.getString("id_visita");
                        String ransho = dbHandler.dameRanchoFromVisita(id_visita);
                        msje_malos += "*Una foto de la visita al rancho " + ransho + " no se pudo sincronizar con el servidor, el error fue: "+wrong.getString("err")+".\n\n";
                    }
                    JSONArray success_fotos = new JSONArray(success);
                    if(success_fotos.length() > 0){
                        for(int i = 0; i < success_fotos.length(); i++){
                            JSONObject good = success_fotos.getJSONObject(i);
                            String id_foto = good.getString("id_foto");
                            String id_foto_movil = good.getString("id_foto_movil");
                            dbHandler.fotoSincronizadaFinal(id_foto, id_foto_movil);
                        }
                    }
                    mensaje_respuesta += "\n"+msje_malos;
                    DialogsBiochem.muestraDialogoWarningCustom(NavDrawerActivity.this, mensaje_respuesta, new DialogsBiochem.CustomAccion() {
                        @Override
                        public void customAccion() {
                            sincronizaMesta("4");
                        }
                    }, 1);
                }
                if(codigo_respuesta.equals("200")){
                    JSONArray success_ranchos = new JSONArray(success);
                    if(success_ranchos.length() > 0){
                        for(int i = 0; i < success_ranchos.length(); i++){
                            JSONObject good = success_ranchos.getJSONObject(i);
                            String id_foto = good.getString("id_foto");
                            String id_foto_movil = good.getString("id_foto_movil");
                            dbHandler.fotoSincronizadaFinal(id_foto, id_foto_movil);
                        }
                    }
                    DialogsBiochem.muestraDialogoSuccessCustom(NavDrawerActivity.this, mensaje_respuesta, new DialogsBiochem.CustomAccion() {
                        @Override
                        public void customAccion() {
                            sincronizaMesta("");
                        }
                    });
                }else{
                    DialogsBiochem.muestraDialogoErrorCustom(NavDrawerActivity.this, "CODIGO DE ERROR: " + codigo_respuesta + ".\n" + mensaje_respuesta, NavDrawerActivity.this::customAccion);
                }
            } catch (JSONException e) {
                DialogsBiochem.muestraDialogoErrorCustom(NavDrawerActivity.this, "Ocurrio un error al convertir la respuesta del servidor:"+respuesta_servidor+"\n" + e, NavDrawerActivity.this::customAccion);
            }
        } // Fin del onPostExecute
    }
}
