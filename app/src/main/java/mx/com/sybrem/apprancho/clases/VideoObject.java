package mx.com.sybrem.apprancho.clases;

public class VideoObject {

    private String videoName;

    public VideoObject(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoName() {
        return videoName;
    }
}
