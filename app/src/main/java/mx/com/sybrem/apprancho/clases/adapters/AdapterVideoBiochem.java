package mx.com.sybrem.apprancho.clases.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.sybrem.apprancho.R;
import mx.com.sybrem.apprancho.clases.VideoObject;

public class AdapterVideoBiochem extends BaseAdapter {
    protected Activity activity;
    protected ArrayList<VideoObject> lista;
    protected ArrayList<VideoObject> lista_prov;
    private int lastPosition = -1;

    public AdapterVideoBiochem(Activity activity, ArrayList<VideoObject> lista) {
        this.activity = activity;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;

        if (v == null) {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.item_video, null);
            holder = new ViewHolder();
            holder.nameVideo = (TextView)v.findViewById(R.id.txtVideo);
            v.setTag(holder);
        }else{
            holder = (ViewHolder) v.getTag();
        }
        VideoObject vid = (VideoObject) lista.get(position);

        holder.nameVideo.setText(vid.getVideoName());

        if (getCount() > 3) {
            Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), (position > lastPosition) ? R.anim.lista_deabajo_parriba : R.anim.lista_dearriba_pabajo);
            v.startAnimation(animation);
            lastPosition = position;
        }

        return v;
    }

    class ViewHolder {
        TextView nameVideo;
    }
}
