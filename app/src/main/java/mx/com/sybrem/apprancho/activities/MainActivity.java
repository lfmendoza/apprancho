package mx.com.sybrem.apprancho.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import mx.com.sybrem.apprancho.R;
import mx.com.sybrem.apprancho.BuildConfig;
import mx.com.sybrem.apprancho.clases.general.DialogsBiochem;
import mx.com.sybrem.apprancho.clases.general.ToastBiochem;

public class MainActivity extends AppCompatActivity {

    private static final int SOLICITUD_PERMISO_WRITE_CALL_LOG = 0;
    String permisoUbicacion1 = Manifest.permission.ACCESS_FINE_LOCATION;
    String permisoMemoria = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    String permisoCamaga = Manifest.permission.CAMERA;

    String[] Permisos = new String[]{permisoMemoria, permisoUbicacion1, permisoCamaga};

    int sumaBanderas = 0;
    String banderaPermiso = "";

    TextView version_app;
    String currentVersionName = "";
    LinearLayout img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        currentVersionName = BuildConfig.VERSION_NAME;

        img = (LinearLayout) findViewById(R.id.inicio);
        img.setClickable(true);
        version_app = (TextView) findViewById(R.id.versionAppcita);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/classico.ttf");
        version_app.setTypeface(face);
        version_app.setText("v" + currentVersionName);

        //Si el dispositivo cuenta con la versión 6 de Android o superior, advierte que hay que otorgar los permisos
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            SharedPreferences pref = getSharedPreferences("permisosBiochem", Context.MODE_PRIVATE);
            String permisos = pref.getString("permisos", "0");
            if (permisos.equals("0")) {

                DialogsBiochem.muestraDialogoWarningCustom(MainActivity.this, "Su dispositivo cuenta con una versión de Android mayor a 6.0 (Android " + Build.VERSION.RELEASE + "), toque la imagen para conceder cada uno de los permisos" +
                        " necesarios, toque la pantalla hasta que sea redirigido a la pantalla de login.", new DialogsBiochem.CustomAccion() {
                    @Override
                    public void customAccion() {

                    }
                }, 1);
            }
        }

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    sumaBanderas = 0;
                    for (int i = 0; i < Permisos.length; i++) {
                        sumaBanderas += entrarApp(Permisos[i]);

                    }
                    SharedPreferences pref = getSharedPreferences("permisosBiochem", Context.MODE_PRIVATE);
                    String permisos = pref.getString("permisos", "0");
                    if (sumaBanderas < 3) {
                        ToastBiochem.toastSimple(MainActivity.this, "Conceda todos los permisos para continuar", 0);
                    } else {
                        if (permisos.equals("0")) {
                            SharedPreferences preferencias = getSharedPreferences("permisosBiochem", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferencias.edit();
                            editor.putString("permisos", "" + sumaBanderas);
                            editor.commit();
                            login();
                            return;
                        }else{
                            login();
                            return;
                        }
                    }
                } else {
                    login();
                    return;
                }
            }
        });
    }

    public void login() {
        Intent explicit_intent;
        explicit_intent = new Intent(MainActivity.this, Login.class);
        explicit_intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        explicit_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(explicit_intent);
    }

    public int entrarApp(String permiso) {
        int bandera_ubicacion = 0;
        if (ContextCompat.checkSelfPermission(this, permiso) == PackageManager.PERMISSION_GRANTED) {
            bandera_ubicacion = 1;
        } else {
            banderaPermiso = permiso;
            solicitarPermiso(permiso, "Necesita otorgar los permisos necesarios a la aplicación para poder acceder", SOLICITUD_PERMISO_WRITE_CALL_LOG, this);
        }
        return bandera_ubicacion;
    }

    public static void solicitarPermiso(final String permiso, String justificacion,
                                        final int requestCode, final Activity actividad) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(actividad,
                permiso)) {
            new AlertDialog.Builder(actividad)
                    .setTitle("Solicitud de permiso")
                    .setMessage(justificacion)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            ActivityCompat.requestPermissions(actividad,
                                    new String[]{permiso}, requestCode);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(actividad,
                    new String[]{permiso}, requestCode);
        }
    }
}
