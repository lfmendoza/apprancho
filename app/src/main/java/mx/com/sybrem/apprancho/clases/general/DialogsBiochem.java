package mx.com.sybrem.apprancho.clases.general;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import mx.com.sybrem.apprancho.R;

public class DialogsBiochem {

    public static void muestraDialogoError(Activity activity, String msj) {

        AlertDialog.Builder pd2 = new AlertDialog.Builder(activity);
        pd2.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_error, null);

        TextView tvClasif = (TextView) view.findViewById(R.id.txt);
        tvClasif.setText(msj);
        Button btnCerrar = (Button) view.findViewById(R.id.btnDialogError);

        pd2.setView(view);
        final AlertDialog dialog = pd2.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                return;
            }
        });
    }

    public static void muestraDialogoSuccess(Activity activity, String msj) {
        AlertDialog.Builder pd2 = new AlertDialog.Builder(activity);
        pd2.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_success, null);

        TextView tvClasif = (TextView) view.findViewById(R.id.txt);
        tvClasif.setText(msj);
        Button btnCerrar = (Button) view.findViewById(R.id.btnDialogSuccess);

        pd2.setView(view);
        final AlertDialog dialog = pd2.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                return;
            }
        });
    }

    public static void muestraDialogoSuccessCustom(Activity activity, String msje, final CustomAccion custom_accion) {
        AlertDialog.Builder pd2 = new AlertDialog.Builder(activity);
        pd2.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_success, null);

        TextView tvClasif = (TextView) view.findViewById(R.id.txt);
        tvClasif.setText(msje);
        Button btnCerrar = (Button) view.findViewById(R.id.btnDialogSuccess);

        pd2.setView(view);
        final AlertDialog dialog = pd2.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                custom_accion.customAccion();
                return;
            }
        });
    }

    public static void muestraDialogoErrorCustom(Activity activity, String msj, final CustomAccion custom_action) {

        AlertDialog.Builder pd2 = new AlertDialog.Builder(activity);
        pd2.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_error, null);

        TextView tvClasif = (TextView) view.findViewById(R.id.txt);
        tvClasif.setText(msj);
        Button btnCerrar = (Button) view.findViewById(R.id.btnDialogError);

        pd2.setView(view);
        final AlertDialog dialog = pd2.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                custom_action.customAccion();
                return;
            }
        });
    }


    public static void muestraDialogoWarningCustom(Activity activity, String msj, final CustomAccion custom_accion, int botones) {
        AlertDialog.Builder pd2 = new AlertDialog.Builder(activity);
        pd2.setCancelable(false);
        int layout_dialog = (botones == 1) ? R.layout.dialog_warning_1_boton : R.layout.dialog_warning_2_botones;
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(layout_dialog, null);

        TextView tvClasif = (TextView) view.findViewById(R.id.txt);
        tvClasif.setText(msj);
        Button btnCerrar = (Button) view.findViewById(R.id.btnAcepta);
        Button btnRechaza = null;
        if(botones == 2){
            btnRechaza = (Button) view.findViewById(R.id.btnRechaza);
        }

        pd2.setView(view);
        final AlertDialog dialog = pd2.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAsFloating;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                custom_accion.customAccion();
                return;
            }
        });
        if (botones == 2) {
            btnRechaza.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    return;
                }
            });
        }
    }

    public interface CustomAccion {
        void customAccion();
    }
}
