package mx.com.sybrem.apprancho.clases.general;

import android.app.Activity;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.github.johnpersano.supertoasts.library.utils.PaletteUtils;

public class ToastBiochem {
    public static void toastSimple(Activity activity, String mensaje, int bandera) {
        SuperActivityToast.create(activity, new Style(), Style.TYPE_BUTTON)
                .setText(mensaje)
                .setTextSize(Style.TEXTSIZE_LARGE)
                .setDuration(Style.DURATION_LONG)
                .setFrame(Style.FRAME_LOLLIPOP)
                .setColor(PaletteUtils.getSolidColor((bandera == 1) ? PaletteUtils.MATERIAL_GREEN : PaletteUtils.MATERIAL_RED))
                .setAnimations(Style.ANIMATIONS_POP).show();
    }
}
