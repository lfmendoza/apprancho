package mx.com.sybrem.apprancho.clases;

public class FotoVisita {
    int idImagen;
    String base64Img;

    public FotoVisita(int idImagen, String base64Img) {
        this.idImagen = idImagen;
        this.base64Img = base64Img;
    }

    public int getIdImagen() {
        return idImagen;
    }

    public String getBase64Img() {
        return base64Img;
    }

    @Override
    public boolean equals(Object anObject) {
        if (!(anObject instanceof FotoVisita)) {
            return false;
        }
        FotoVisita otherMember = (FotoVisita)anObject;
        return otherMember.getBase64Img().equals(getBase64Img());
    }
}
