package mx.com.sybrem.apprancho.clases.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.sybrem.apprancho.R;
import mx.com.sybrem.apprancho.clases.Agente;

public class AdapterAgente extends BaseAdapter implements Filterable {
    protected Activity activity;
    protected ArrayList<Agente> itemAgente;
    protected ArrayList<Agente> itemAgente_prov;
    private int lastPosition = -1;

    public AdapterAgente(Activity activity, ArrayList<Agente> itemAgente) {
        this.activity = activity;
        this.itemAgente = itemAgente;
    }

    @Override
    public int getCount() {
        return itemAgente.size();
    }

    @Override
    public Object getItem(int i) {
        return itemAgente.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View v = convertView;
        if(convertView==null){
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.item_agente_select, null);
        }
        Agente agente = itemAgente.get(i);
        TextView nombreAgente = (TextView) v.findViewById(R.id.txt);
        nombreAgente.setText(agente.getNombre_agente());

        if (getCount() > 3) {
            Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), (i > lastPosition) ? R.anim.lista_deabajo_parriba : R.anim.lista_dearriba_pabajo);
            v.startAnimation(animation);
            lastPosition = i;
        }
        return v;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<Agente> results = new ArrayList<Agente>();
                if (itemAgente_prov == null)
                    itemAgente_prov = itemAgente;
                if (constraint != null) {
                    if (itemAgente_prov != null && itemAgente_prov.size() > 0) {
                        for (final Agente p : itemAgente_prov) {
                            if (p.getNombre_agente().toLowerCase()
                                    .contains(constraint.toString()))
                                results.add(p);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                itemAgente = (ArrayList<Agente>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
