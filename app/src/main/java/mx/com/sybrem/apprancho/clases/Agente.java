package mx.com.sybrem.apprancho.clases;

public class Agente {

    private String num_ruta;
    private String nombre_agente;

    public Agente(String num_ruta, String nombre_agente) {
        this.num_ruta = num_ruta;
        this.nombre_agente = nombre_agente;
    }

    public String getNum_ruta() {
        return num_ruta;
    }

    public String getNombre_agente() {
        return nombre_agente;
    }
}
