package mx.com.sybrem.apprancho.clases.general;

import android.app.Application;

public class GlobalsBioshem extends Application {

    //PARA SABER SI SE HACEN PRUEBAS O SE LANZA A PRODUCCION
    // 0 PRUEBAS
    // 1 PRODUCCION
    private int enviroment = 0;

    //URLS DE ENVIO
    private String urlSendMuestras = "http://sybrem.com.mx/adsnet/syncmovil/java/appRancho/";
    private String urlSendRanchos = "http://sybrem.com.mx/adsnet/syncmovil/java/appRancho/";
    private String urlSendVisitas = "http://sybrem.com.mx/adsnet/syncmovil/java/appRancho/";
    private String urlSendFotos = "http://sybrem.com.mx/adsnet/syncmovil/java/appRancho/";

    public int getEnviroment() {
        return enviroment;
    }

    public String getUrlSendMuestras() {
        if(enviroment == 1)
            return urlSendMuestras + "recibeMuestras.php?"+ InfoEnviar.enviaInfo() +"&jason=";
        else
            return urlSendMuestras + "pruebas/recibeMuestras.php?" + InfoEnviar.enviaInfo() + "&jason=";
    }

    public String getUrlSendRanchos() {
        if(enviroment == 1)
            return urlSendRanchos + "recibeRanchos.php?"+ InfoEnviar.enviaInfo() +"&jason=";
        else
            return urlSendRanchos + "pruebas/recibeRanchos.php?" + InfoEnviar.enviaInfo()+ "&jason=";
    }

    public String getUrlSendVisitas() {
        if(enviroment == 1)
            return urlSendVisitas + "recibeVisitas.php?"+ InfoEnviar.enviaInfo() +"&jason=";
        else
            return urlSendVisitas + "pruebas/recibeVisitas.php?" + InfoEnviar.enviaInfo() + "&jason=";
    }

    public String getUrlSendFotos() {
        if(enviroment == 1)
            return urlSendFotos + "recibeFotos.php?"+ InfoEnviar.enviaInfo() +"&jason=";
        else
            return urlSendFotos + "pruebas/recibeFotos.php?" + InfoEnviar.enviaInfo() + "&jason=";
    }
}